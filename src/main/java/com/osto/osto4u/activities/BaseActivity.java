package com.osto.osto4u.activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.osto.osto4u.fragments.BalanceFragment;
import com.osto.osto4u.fragments.ChangePasswordFragment;
import com.osto.osto4u.fragments.HelpAndSupportFragment;
import com.osto.osto4u.fragments.HomeFragment;
import com.osto.osto4u.fragments.MyInterestFragment;
import com.osto.osto4u.fragments.PrivacyPolicyFragment;
import com.osto.osto4u.fragments.ReferFriendsFragment;
import com.osto.osto4u.fragments.SavedItemsFragment;
import com.osto.osto4u.fragments.TermsAndConditionsFragment;
import com.osto.osto4u.helpers.Config;
import com.osto.osto4u.helpers.FragmentDrawer;
import com.osto.osto4u.helpers.NotificationUtils;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class BaseActivity extends ActionBarActivity implements FragmentDrawer.FragmentDrawerListener {

    private static String TAG = BaseActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    Fragment fragment;
    SessionManager sessionManager;
    DrawerLayout mDrawerLayout;


    private BroadcastReceiver mRegistrationBroadcastReceiver;

    private void displayFirebaseRegId() {
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", null);

        Log.e(TAG, "Firebase reg id: " + regId);

    }


    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        // register GCM registration complete receiver
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.REGISTRATION_COMPLETE));

        // register new push message receiver
        // by doing this, the activity will be notified each time a new message arrives
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Config.PUSH_NOTIFICATION));

        // clear the notification area when the app is opened
        NotificationUtils.clearNotifications(getApplicationContext());
        //generateFBHashKey();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                // checking for type intent filter
                if (intent.getAction().equals(Config.REGISTRATION_COMPLETE)) {
                    // gcm successfully registered
                    // now subscribe to `global` topic to receive app wide notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(Config.TOPIC_GLOBAL);

                    displayFirebaseRegId();

                } else if (intent.getAction().equals(Config.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();
                }
            }
        };

        displayFirebaseRegId();
        Log.e("LoggedIn from", new SessionManager(this).getUserDetails().get("loginFrom") + " ");

        sessionManager = new SessionManager(getApplicationContext());

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        drawerFragment = (FragmentDrawer)
                getFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, mToolbar);
        drawerFragment.setDrawerListener(this);

        // display the first navigation drawer view on app launch
//        displayView(0);

        switchFragment(new HomeFragment());

    }


    @Override
    public void onDrawerItemSelected(View view, int position) {
        if (new SessionManager(getApplicationContext()).getUserDetails().get("loginFrom").toString().equalsIgnoreCase("app")) {
            displayView(position);
        } else {
            displayViewNoCP(position);
        }
    }

    private void displayViewNoCP(int position) {
        switch (position) {
            case 0:
                switchFragment(new HomeFragment());
                break;
            case 1:
                switchFragment(new SavedItemsFragment());
                break;

            case 2:
                switchFragment(new MyInterestFragment());
                break;
//            case 3:
//                switchFragment(new ClubsFragment());
//                break;
//            case 4:
//                switchFragment(new EventsFragment());
//                break;
//            case 3:
//                switchFragment(new BirthdaysFragment());
//                break;
            case 3:
                switchFragment(new BalanceFragment());
                break;
            case 4:
                switchFragment(new ReferFriendsFragment());
                break;
            case 5:
                switchFragment(new TermsAndConditionsFragment());
                break;
            case 6:
                switchFragment(new PrivacyPolicyFragment());
                break;
            case 7:
                switchFragment(new HelpAndSupportFragment());
                break;
            case 8:
                sessionManager.logoutUser();
                finish();
                break;

            default:
                break;
        }
    }


    private void displayView(int position) {
        switch (position) {
            case 0:
                switchFragment(new HomeFragment());
                break;
            case 1:
                switchFragment(new SavedItemsFragment());
                break;

            case 2:
                switchFragment(new MyInterestFragment());
                break;
//            case 3:
//                switchFragment(new ClubsFragment());
//                break;
//            case 4:
//                switchFragment(new EventsFragment());
//                break;
//            case 3:
//                switchFragment(new BirthdaysFragment());
//                break;
            case 3:
                switchFragment(new BalanceFragment());
                break;
            case 4:
                switchFragment(new ChangePasswordFragment());
                break;
            case 5:
                switchFragment(new ReferFriendsFragment());
                break;
            case 6:
                switchFragment(new TermsAndConditionsFragment());
                break;
            case 7:
                switchFragment(new PrivacyPolicyFragment());
                break;
            case 8:
                switchFragment(new HelpAndSupportFragment());
                break;
            case 9:
                sessionManager.logoutUser();
                finish();
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            if (!((getFragmentManager().findFragmentById(R.id.container_body)) instanceof HomeFragment)) {
                switchFragment(new HomeFragment());
            } else {
                super.onBackPressed();
            }
        }
    }

    public void switchFragment(Fragment mFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, mFragment);
        fragmentTransaction.commit();
    }


    //Generate FB HashKey
    public void generateFBHashKey()
    {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.osto.osto4u",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT) + "   Value");
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.d("KeyHash:", e.getMessage() + "   1");


        } catch (NoSuchAlgorithmException e) {
            Log.d("KeyHash:", e.getMessage() + "   2");

        }
    }
}