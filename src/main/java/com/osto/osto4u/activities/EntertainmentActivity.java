package com.osto.osto4u.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.InterestResponse;
import com.osto.osto4u.models.InterestsBody;
import com.osto.osto4u.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class EntertainmentActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    LinearLayout shortMoviesLayout, musicsLayout, dubsmashLayout, danceLayouts, imagesLayout, standupComedyLayout;

    CheckBox shortMoviesCB, musicCB, danceCB, dubsmashCB, imagesCB, standupComedyCB;
    ProgressBar mProgressBar;
    ArrayList<String> selectedValues;

    Button nextButton;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainment);
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());

        shortMoviesLayout = (LinearLayout) findViewById(R.id.short_movies_ent);
        musicsLayout = (LinearLayout) findViewById(R.id.music_ent);
        dubsmashLayout = (LinearLayout) findViewById(R.id.dubsmash_ent);
        danceLayouts = (LinearLayout) findViewById(R.id.dance_ent);
        imagesLayout = (LinearLayout) findViewById(R.id.images_ent);
        standupComedyLayout = (LinearLayout) findViewById(R.id.standup_comedy_ent);

        mProgressBar = (ProgressBar)findViewById(R.id.progressbar_entertainment);

        nextButton = (Button) findViewById(R.id.update_interests_ent);
        shortMoviesCB = (CheckBox) findViewById(R.id.short_movies_checkbox_ent);
        musicCB = (CheckBox) findViewById(R.id.music_checkbox_ent);
        dubsmashCB = (CheckBox) findViewById(R.id.dubsmash_checkbox_ent);
        imagesCB = (CheckBox) findViewById(R.id.images_checkbox_ent);
        danceCB = (CheckBox) findViewById(R.id.dance_checkbox_ent);
        standupComedyCB = (CheckBox) findViewById(R.id.standup_comedy_checkbox_ent);

        hideProgressBar();

        shortMoviesLayout.setOnClickListener(this);
        musicsLayout.setOnClickListener(this);
        dubsmashLayout.setOnClickListener(this);
        danceLayouts.setOnClickListener(this);
        imagesLayout.setOnClickListener(this);
        standupComedyLayout.setOnClickListener(this);
        nextButton.setOnClickListener(this);
        selectedValues = new ArrayList<>();

        shortMoviesCB.setOnCheckedChangeListener(this);
        musicCB.setOnCheckedChangeListener(this);
        dubsmashCB.setOnCheckedChangeListener(this);
        imagesCB.setOnCheckedChangeListener(this);
        danceCB.setOnCheckedChangeListener(this);
        standupComedyCB.setOnCheckedChangeListener(this);

    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.update_interests_ent:

                if (selectedValues.size() == 0) {
                    Toast.makeText(this, "Select atleast one Interest", Toast.LENGTH_SHORT).show();

                } else {
                    submitInterestsApiCall();
                }
                break;
            case R.id.short_movies_ent:
                if (shortMoviesCB.isChecked()) {
                    shortMoviesCB.setChecked(false);
                } else {
                    shortMoviesCB.setChecked(true);

                }

                break;
            case R.id.dubsmash_ent:
                if (dubsmashCB.isChecked()) {
                    dubsmashCB.setChecked(false);
                } else {
                    dubsmashCB.setChecked(true);

                }

                break;
            case R.id.standup_comedy_ent:

                if (standupComedyCB.isChecked()) {
                    standupComedyCB.setChecked(false);
                } else {
                    standupComedyCB.setChecked(true);

                }

                break;
            case R.id.images_ent:
                if (imagesCB.isChecked()) {
                    imagesCB.setChecked(false);
                } else {
                    imagesCB.setChecked(true);

                }

                break;

            case R.id.dance_ent:
                if (danceCB.isChecked()) {
                    danceCB.setChecked(false);
                } else {
                    danceCB.setChecked(true);

                }

                break;
            case R.id.music_ent:
                if (musicCB.isChecked()) {
                    musicCB.setChecked(false);
                } else {
                    musicCB.setChecked(true);

                }
//                }
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


        switch (buttonView.getId()) {
            case R.id.images_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("Images");
                } else {
                    selectedValues.remove("Images");
                }
                break;
            case R.id.dubsmash_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("Dubsmash");
                } else {
                    selectedValues.remove("Dubsmash");
                }
                break;
            case R.id.short_movies_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("ShortMovies");
                } else {
                    selectedValues.remove("ShortMovies");
                }
                break;
            case R.id.dance_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("Dance");
                } else {
                    selectedValues.remove("Dance");
                }
                break;
            case R.id.standup_comedy_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("StandupComedy");
                } else {
                    selectedValues.add("StandupComedy");
                }
                break;
            case R.id.music_checkbox_ent:
                if (isChecked) {
                    selectedValues.add("Music");
                } else {
                    selectedValues.remove("Music");
                }
                break;
        }
    }

    public void submitInterestsApiCall() {
//        intersts = new String[selectedValues.size()];
        showProgressBar();
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        String interests = selectedValues.get(0);

        for (int i = 1; i < selectedValues.size(); i++) {
            interests = interests.concat(",") + selectedValues.get(i);
        }
        Log.e("Entertainment Selected", interests + "");

        InterestsBody body = new InterestsBody();
//        body.setUserId("1002");
        body.setUserId(String.valueOf(new SessionManager(getApplicationContext()).getUserDetails().get("userid")));
        body.setIntrest(interests);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<InterestResponse> call = apiInterface.updateInterest(body);
        call.enqueue(new Callback<InterestResponse>() {
            @Override
            public void onResponse(Call<InterestResponse> call, Response<InterestResponse> response) {

                InterestResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        Log.e("Entertainment ", responseBody.getSuccess() + "");

                        if (responseBody.getCode().equalsIgnoreCase("200")) {
                            Log.e("Entertainment In If ", responseBody.getSuccess() + "");


//                            sessionManager.createLoginSession(emailTV.getText().toString());
//                            startActivity(new Intent(getApplicationContext(), BaseActivity.class));
//                            errorMessageDisplay.setVisibility(View.GONE);
                            OSTOApplication.getInstance().getRequestQueue().getCache().remove("http://osto.herokuapp.com:80/home");

                            startActivity(new Intent(getApplicationContext(), BaseActivity.class));
                            finish();
                        } else {
                            Log.e("Entertainment In Else ", responseBody.getSuccess() + "");
                        }
                    }

                } else {
                    Log.e("Entertainment Fail", response.message() + "");
                }
            }

            @Override
            public void onFailure(Call<InterestResponse> call, Throwable t) {
                Log.e("OnFailure ", t.getMessage() + "");
//                mProgressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });

    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
}
