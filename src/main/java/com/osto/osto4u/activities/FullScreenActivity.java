package com.osto.osto4u.activities;


import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.osto.osto4u.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.View.GONE;

public class FullScreenActivity extends YouTubeBaseActivity {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    private YouTubePlayerView myouTubePlayerView;
    private YouTubePlayer.OnInitializedListener monInitializedListener;
    String type, path;
    ImageView mImageView, goBack;
    VideoView mVideoView;
    ProgressBar mProgressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());

        setContentView(R.layout.fragment_full_screen);
        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_activity_fullscreen);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        mToolBarLayout.setVisibility(GONE);
        toolBarTitle.setVisibility(GONE);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        goBack.setVisibility(View.VISIBLE);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_full_screen);
        mProgressBar.setVisibility(View.VISIBLE);

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        myouTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtube_player_view);
        monInitializedListener = new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                mProgressBar.setVisibility(GONE);
                String[] url = path.split("=");
                youTubePlayer.loadVideo(url[1]);
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
            }
        };



        Bundle bundle = getIntent().getExtras();
        type = (String) bundle.get("category");
        path = bundle.getString("path");

        Log.e("URL",path);

        mImageView = (ImageView) findViewById(R.id.imageview_fullscreen);
        mVideoView = (VideoView) findViewById(R.id.videoview_fullscreen);



        if (type.equalsIgnoreCase("images")) {
            mVideoView.setVisibility(GONE);
            myouTubePlayerView.setVisibility(GONE);
            mImageView.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(GONE);
            Glide.with(this).load(path).placeholder(R.drawable.home_placeholder)
                    .into(mImageView);
        } else if (type.equalsIgnoreCase("video")) {
            mVideoView.setVisibility(View.VISIBLE);
            mImageView.setVisibility(GONE);
            myouTubePlayerView.setVisibility(GONE);

            final Uri uri = Uri.parse(path);
            mVideoView.setVideoURI(uri);

            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mProgressBar.setVisibility(GONE);
                    mVideoView.start();
                }
            });
            MediaController mediaController = new MediaController(this);
            mVideoView.setMediaController(mediaController);
        }
        else
            {
                mVideoView.setVisibility(GONE);
                mImageView.setVisibility(GONE);
                mProgressBar.setVisibility(GONE);
                myouTubePlayerView.initialize("AIzaSyDCizPkKclVPdKV_YJSxGl3_w6idwv0hjo", monInitializedListener);

            }
    }
}

