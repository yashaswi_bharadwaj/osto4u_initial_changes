package com.osto.osto4u.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.LoginPostBody;
import com.osto.osto4u.models.LoginResponseModel;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.ActivityScreenTransition;
import com.osto.osto4u.utils.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class LoginActivity extends MainActivity implements View.OnClickListener {
    private final String TAG = AppConstants.TAG + LoginActivity.class.getSimpleName();

    SharedPreferences mSharedPref;
    EditText emailTV, passwordTV;
    Button loginBtn;
    RelativeLayout facebookLogin, gmailLogin;
    TextView loginRedirect, errorMessageDisplay, forgotPassword;
    SessionManager sessionManager;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        Log.d(TAG, "attaching Base context");
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        sessionManager = new SessionManager(getApplicationContext());

        emailTV = (EditText) findViewById(R.id.email_login);
        passwordTV = (EditText) findViewById(R.id.password_login);
        loginBtn = (Button) findViewById(R.id.login_btn);
        facebookLogin = (RelativeLayout) findViewById(R.id.facebook_login);
        gmailLogin = (RelativeLayout) findViewById(R.id.gmail_login);
        loginRedirect = (TextView) findViewById(R.id.signup_redirect);
        errorMessageDisplay = (TextView) findViewById(R.id.login_error_message);
        forgotPassword = (TextView) findViewById(R.id.forgot_password_login);

        errorMessageDisplay.setVisibility(View.GONE);
        loginRedirect.setOnClickListener(this);
        loginBtn.setOnClickListener(this);
        facebookLogin.setOnClickListener(this);
        gmailLogin.setOnClickListener(this);
        forgotPassword.setOnClickListener(this);

        mSharedPref = getApplicationContext().getSharedPreferences("userIn", MODE_PRIVATE);


        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_login);
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        passwordTV.setTransformationMethod(new PasswordTransformationMethod());

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ActivityScreenTransition.animateScreen(LoginActivity.this,
                ActivityScreenTransition.ANIM_TYPE.EXIT);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.login_btn:
                 signIn();
                break;
            case R.id.gmail_login:
                //  gmailSignInBtn.performClick();
                googleSignIn("login");
                break;
            case R.id.facebook_login:
//                new SignUpActivity().loginButton.performClick();
                fBLogin("login");
                break;

            case R.id.signup_redirect:
                startActivity(new Intent(getApplicationContext(), SignUpActivity.class));
                ActivityScreenTransition.animateScreen(LoginActivity.this,
                        ActivityScreenTransition.ANIM_TYPE.ENTER);

                break;
            case R.id.forgot_password_login:
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
                ActivityScreenTransition.animateScreen(LoginActivity.this,
                        ActivityScreenTransition.ANIM_TYPE.ENTER);
        }
    }

    public void signIn() {
        Log.d(TAG, "signing In");

        if (inValidateFields()) {
            mProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            LoginPostBody body = new LoginPostBody();
            body.setEmail(emailTV.getText().toString());
            body.setPassword(passwordTV.getText().toString());

            ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
            Call<LoginResponseModel> call = apiInterface.loginToApp(body);
            call.enqueue(new Callback<LoginResponseModel>() {
                @Override
                public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                    LoginResponseModel responseBody;

                    if (response.isSuccessful()) {
                        responseBody = response.body();
                        if (responseBody != null) {
                            if (responseBody.getSuccess().equalsIgnoreCase("login sucessfull")) {
                                mProgressBar.setVisibility(View.GONE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                sessionManager.createLoginSession(emailTV.getText().toString(),
                                        "",
                                        responseBody.getUserId(),"app");

                                startActivity(new Intent(getApplicationContext(), BaseActivity.class));
                                errorMessageDisplay.setVisibility(View.GONE);
                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(),responseBody.getSuccess(),Toast.LENGTH_SHORT).show();
//                                errorMessageDisplay.setText(responseBody.getSuccess());
//                                errorMessageDisplay.setVisibility(View.VISIBLE);
                                mProgressBar.setVisibility(View.GONE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            }
                        }

                    } else {
                        Log.e("LOGIN ACTIVITY", response.errorBody() + "");
                    }
                }

                @Override
                public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                    Log.e("LOGIN ACTIVITY", t.getMessage() + "");
                    mProgressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });
        } else {
            Toast.makeText(this, "Enter details to login", Toast.LENGTH_SHORT).show();
        }
    }

    public boolean inValidateFields() {
        if (emailTV.getText().toString().contentEquals("") || passwordTV.getText().toString().contentEquals("")) {
            return false;
        } else
            return true;
    }



}
