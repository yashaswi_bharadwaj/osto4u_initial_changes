package com.osto.osto4u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.VerifyOTPBody;
import com.osto.osto4u.models.VerifyOTPResponse;
import com.osto.osto4u.models.VerifyReferralPostBody;
import com.osto.osto4u.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyOTPActivity extends AppCompatActivity {

    RelativeLayout mToolBarLayout,referralVerifyLayout;
    LinearLayout verifyOTPLayout;
    TextView toolBarTitle;
    ImageView goBack;
    Button verifyOTPBtn,verifyReferalCodeBtn,skipBtn;
    EditText phoneVerify, otpEntered,referralCode;

    ProgressBar mProgressBar;
    String userEmail,userName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());


        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_activity_verify_otp);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        toolBarTitle.setText("Verify OTP");


        referralCode = (EditText)findViewById(R.id.referal_code_et);
        verifyOTPLayout = (LinearLayout) findViewById(R.id.before_verify_otp);
        verifyOTPLayout.setVisibility(View.VISIBLE);

        referralVerifyLayout = (RelativeLayout)findViewById(R.id.after_verify_otp);
        referralVerifyLayout.setVisibility(View.GONE);

        verifyReferalCodeBtn = (Button) findViewById(R.id.verify_referral);
        skipBtn= (Button) findViewById(R.id.skip_referral);


        skipBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getApplicationContext(), EntertainmentActivity.class));
                finish();
            }
        });

        verifyReferalCodeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(referralCode.getText().toString().contentEquals(""))
                {
                 Toast.makeText(getApplicationContext(),"Enter referral code",Toast.LENGTH_SHORT).show();
                }
                else {
                    mProgressBar.setVisibility(View.VISIBLE);
                    verifyReferralCodeApiCall(referralCode.getText().toString());
                }
            }
        });

//        phoneVerify = (EditText) findViewById(R.id.mobile_num_verification);
        otpEntered = (EditText) findViewById(R.id.otp_verification);
        verifyOTPBtn = (Button) findViewById(R.id.verify_btn);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_verifyotp);
        mProgressBar.setVisibility(View.GONE);

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        verifyOTPBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!otpEntered.getText().toString().contentEquals("")) {
                    mProgressBar.setVisibility(View.VISIBLE);
                    verifyOTPApiCall(otpEntered.getText().toString());

                } else {
                    otpEntered.setError("Please Enter OTP");
                }

            }
        });


    }

    private void verifyReferralCodeApiCall(String code) {

        VerifyReferralPostBody referralPostBody= new VerifyReferralPostBody();
        referralPostBody.setRefferalcode(code);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.verifyReferralCode(referralPostBody);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    mProgressBar.setVisibility(View.GONE);
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getCode()==200)
                        {
                            Toast.makeText(getApplicationContext(),responseBody.getSuccess(),Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), EntertainmentActivity.class));
                            finish();
                        }
                        else if(responseBody.getCode() ==201)
                        {
                            Toast.makeText(getApplicationContext(),responseBody.getResults(),Toast.LENGTH_SHORT).show();
                        }
                        else
                        {

                            Toast.makeText(getApplicationContext(),"Something Went Wrong",Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e("Birthdays Null", response.message() + "");
                    }
                } else {
                    Log.e("Birthdays api failed", response.message() + "");
                    mProgressBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("OnFailure Bday ", t.getMessage() + "");
                mProgressBar.setVisibility(View.GONE);
            }
        });


    }

    public void verifyOTPApiCall(String otp) {
       final int userId  = getIntent().getIntExtra("userid",0);
       userEmail = getIntent().getStringExtra("usermail");
        userName = getIntent().getStringExtra("username");

        VerifyOTPBody body = new VerifyOTPBody();
        body.setUserid(String.valueOf(userId));
        body.setOtp(otp);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<VerifyOTPResponse> call = apiInterface.verifyOTP(body);
        call.enqueue(new Callback<VerifyOTPResponse>() {
            @Override
            public void onResponse(Call<VerifyOTPResponse> call, Response<VerifyOTPResponse> response) {

                if (response.isSuccessful()) {
                    VerifyOTPResponse verifyOTPResponse = response.body();

                    Log.e("VerifyOTPS Success", verifyOTPResponse + "");

                    if (verifyOTPResponse != null) {

//                        startActivity(new Intent(getApplicationContext(), EntertainmentActivity.class));
//                        finish();
                        Log.e("VerifyOTP User Id", new SessionManager(getApplicationContext()).getUserDetails().get("userid") + "");
                        Log.e("VerifyOTP Code", verifyOTPResponse.getCode() + "");
                        Log.e("VerifyOTPS Success", verifyOTPResponse.getSuccess() + "");

                        if (verifyOTPResponse.getCode() == 200) {

                            new SessionManager(getApplicationContext()).createLoginSession(userEmail,userName,userId,
                                    "app");

                            referralVerifyLayout.setVisibility(View.VISIBLE);
                            verifyOTPLayout.setVisibility(View.GONE);

//
//                            startActivity(new Intent(getApplicationContext(), EntertainmentActivity.class));
//                            finish();
                        }
                    else
                        {
                            Toast.makeText(getApplicationContext(), "Please Enter Correct OTP", Toast.LENGTH_SHORT).show();
                        }
                    }


                } else {
                    Log.e("VerifyOTP ACTIVITY", response.message()+ "");
//                        startActivity(new Intent(getApplicationContext(),EntertainmentActivity.class));

                }
//                startActivity(new Intent(getApplicationContext(), EntertainmentActivity.class));
                mProgressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
//                finish();

            }

            @Override
            public void onFailure(Call<VerifyOTPResponse> call, Throwable t) {
                Log.e("VerifyOTP Failed", t.getMessage() + "");
                mProgressBar.setVisibility(View.GONE);
                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


            }
        });

    }
}
