package com.osto.osto4u.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.LoginTypeModel;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener {
    private final String TAG = AppConstants.TAG + MainActivity.class.getSimpleName();

    LoginButton loginButton;
    CallbackManager callbackManager;
    EditText emailET, passwordET, mobileET, userNameEt;
    RelativeLayout facebookSignUp, gmailSignUp;
    public SignInButton gmailSignInBtn;
    private static final String EMAIL = "email";
    private static final String FIRST_NAME = "first_name";
    private static final String LAST_NAME = "last_name";
    private static final String GENDER = "gender";
    private static final String BIRTHDAY = "birthday";
    public String from = "login";
    ProgressBar mProgressBar;
    private static final int RC_SIGN_IN = 1001;
    public String loginType;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
        Log.d(TAG, "attaching Base context");
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        Log.d(TAG, "onCreate");

        GoogleSignInOptions signInOptions = new GoogleSignInOptions
                .Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail()
                .build();


        OSTOApplication.getInstance().mApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, signInOptions)
                .build();

        gmailSignInBtn = (SignInButton) findViewById(R.id.sign_in_button_gmail);
        gmailSignInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "Gmail Login OnClick");
            }
        });


        loginButton = (LoginButton) findViewById(R.id.facebook_login_button);
        loginButton.setReadPermissions(Arrays.asList(EMAIL));
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Facebook login button");
                //    fBLogin();
            }
        });
    }


//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
////        callbackManager.onActivityResult(requestCode, resultCode, data);
//        super.onActivityResult(requestCode, resultCode, data);

//        if(requestCode==RC_SIGN_IN)
//        {
//
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            if(result.isSuccess())
//            {
//                final GoogleApiClient client =OSTOApplication.getInstance().mApiClient;
//                result.getSignInAccount();
//
//                GoogleSignInAccount acct = result.getSignInAccount();
//
//                Log.e("MAIN ACTIVITY Gmail", "display name: " + acct.getDisplayName());
//            }
//        }
//        else
//        {
//            callbackManager.onActivityResult(requestCode, resultCode, data);
//
//        }
//    }


    public void fBLogin(String from) {
        this.from = from;
        Log.d(TAG, "fbLogin from = " + from);

        //showProgressBar();
        boolean loggedIn = AccessToken.getCurrentAccessToken() == null;
        LoginManager.getInstance().logInWithReadPermissions(this,
                Arrays.asList("public_profile", EMAIL));

        callbackManager = CallbackManager.Factory.create();

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "onSuccess; " + loginResult.getAccessToken().getUserId());

                getUserProfile(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException e) {
                Log.d(TAG, "onError");
            }
        });
    }

    private void getUserProfile(final AccessToken accessToken) {
        Log.d(TAG, "getUserProfile");

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
                        try {
                            //You can fetch user info like this…
                            //object.getJSONObject(“picture”).
                            Log.e(TAG, "NAME = " + object.getString("name"));
                            Log.e(TAG, "User Id FB = " + accessToken.getUserId());

                            if (from.contentEquals("login")) {
                                if (object.has("email")) {
                                    if (!object.getString("email").contentEquals("")) {
                                        loginType("email", object.getString("email"),
                                                object.getString("name"), "facebook");
                                    }
                                } else {
                                    loginType("username", "", object.getString("name"),
                                            "facebook");
                                }
                            } else {
                                updateUI(object.getString("name"), object.getString("email"));

                            }

                            //object.getString(“name”);
                            //object.getString(“email”));
                            //object.getString(“id”));

                        } catch (JSONException e) {
                            Log.e("NAME Exception", "" + e.getMessage());
                            hideProgressBar();
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,picture.width(200)");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == RC_SIGN_IN) {
            showProgressBar();
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void updateUI(String name, String email) {

        hideProgressBar();

        if (email.contentEquals("")) {
            emailET.requestFocus();
        } else {
            mobileET.requestFocus();
        }
        emailET.setText(email);
        userNameEt.setText(name);
        passwordET.setVisibility(View.GONE);
        facebookSignUp.setClickable(false);
    }

    public void updateGmailUI(String name, String email) {
        hideProgressBar();

        emailET.setText(email);
        userNameEt.setText(name);
        passwordET.setVisibility(View.GONE);
        mobileET.requestFocus();
        gmailSignUp.setClickable(false);
    }


    public void googleSignIn(String from) {
        if (OSTOApplication.getInstance().mApiClient != null) {
            OSTOApplication.getInstance().mApiClient.disconnect();
        }

        this.from = from;

//        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(OSTOApplication.getInstance().mApiClient);
//        if (opr.isDone()) {
//            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
//            // and the GoogleSignInResult will be available instantly.
//            Log.d("Login Gmail", "Got cached sign-in");
//            GoogleSignInResult result = opr.get();
//
//            handleSignInResult(result);
//        } else {
//            // If the user has not previously signed in on this device or the sign-in has expired,
//            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
//            // single sign-on will occur in this branch.
////            showProgressDialog();
//            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
//                @Override
//                public void onResult(GoogleSignInResult googleSignInResult) {
////                    hideProgressDialog();
//                    handleSignInResult(googleSignInResult);
//                }
//            });
//        }


        final Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(OSTOApplication.getInstance().mApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void googleSignOut() {
        Auth.GoogleSignInApi.signOut(OSTOApplication.getInstance().mApiClient).setResultCallback(new ResultCallback<Status>() {
            @Override
            public void onResult(@NonNull Status status) {

            }
        });
    }

    private void handleSignInResult(GoogleSignInResult result) {

        Log.d("GSign IN", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            if (acct != null) {

                Log.e("", "display name: " + acct.getDisplayName());
                String personName = acct.getDisplayName();
//                String personPhotoUrl = acct.getPhotoUrl().toString();
                String email = acct.getEmail();

                if (from.contentEquals("login")) {
                    loginType("email", email, "", "email");
                } else {
                    updateGmailUI(personName, email);
                }

                Log.e("GSign IN", "Name: " + personName + ", email: " + email);
            }


        } else {
            // Signed out, show unauthenticated UI.
//            updateUI(false);
        }
    }


    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d("Login Gmail", "onConnectionFailed:" + connectionResult);
    }

    public void loginType(String type, final String email, final String userName, final String loginFrom) {
        LoginTypeModel body = new LoginTypeModel();

        Log.d(TAG, "loginType; type = " + type + ", email = " + email + ", userName = " +
                userName + ", loginForm = " + loginFrom);

        body.setEmail(email);
        body.setLogintype(type);
        body.setUsername(userName);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.loginFrom(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                if (response.isSuccessful()) {
                    hideProgressBar();
                    GeneralRespModel resp = response.body();
                    if (resp != null) {

                        //    Log.e(" User Id", new SessionManager(getApplicationContext()).getUserDetails().get("USERID") + "");
                        if (resp.getSuccess().contentEquals("login sucessfull")) {

                            new SessionManager(getApplicationContext()).createLoginSession(email, userName,
                                    resp.getUserid(), loginFrom);
                            startActivity(new Intent(getApplicationContext(), BaseActivity.class));


                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), resp.getSuccess(), Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    Log.e("VerifyOTP ACTIVITY", response.message() + "");
                    hideProgressBar();
                }

            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Login Failed", t.getMessage() + "");
                hideProgressBar();
            }
        });

    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
}
