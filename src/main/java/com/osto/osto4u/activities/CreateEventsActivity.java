package com.osto.osto4u.activities;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.CreateEventsBody;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class CreateEventsActivity extends AppCompatActivity {
    EditText newEventName, newEventDesc, newEventVenue, newEventDateTime;

    Button createButton, clearButton;
    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    ImageView goBack;
    CreateEventsBody eventsBody;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_event);
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());


        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_create_events);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        goBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("Create Events");

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        newEventName = (EditText) findViewById(R.id.create_event_name);
        newEventDesc = (EditText) findViewById(R.id.create_event_description);
        newEventVenue = (EditText) findViewById(R.id.create_event_venue);
        newEventDateTime = (EditText) findViewById(R.id.create_event_date_time);

        clearButton = (Button) findViewById(R.id.create_clubs_clear_btn);
        createButton = (Button) findViewById(R.id.create_clubs_create_btn);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (invalidateFields()) {
                    apiCall(
                            new SessionManager(getApplicationContext()).getUserDetails().get("userid").toString(),
                            newEventName.getText().toString(),
                            newEventDesc.getText().toString(),
                            newEventVenue.getText().toString(),
                            newEventDateTime.getText().toString()
                    );
                }

            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newEventName.setText("");
                newEventDesc.setText("");
                newEventVenue.setText("");
                newEventDateTime.setText("");
            }
        });
    }

    public void apiCall(String userId, String eventName, String decription, String venue, String timestamp) {

        eventsBody = new CreateEventsBody();
        eventsBody.setEventname(eventName);
        eventsBody.setUserid("1031");
        eventsBody.setTimestamp(timestamp);
        eventsBody.setVenue(venue);
        eventsBody.setDescription(decription);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.createEvents(eventsBody);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getResponseCode() == 200) {
                            Log.e("Birthdays", responseBody.getSuccess() + "");
                            Toast.makeText(getApplicationContext(), "Event Created Successfully", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e("Birthdays Null", response.message() + "");
                    }
                } else {
                    Log.e("Birthdays api failed", response.message() + "");
                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("OnFailure Bday ", t.getMessage() + "");
            }
        });
    }

    public boolean invalidateFields() {
        if (newEventName.getText().toString().contentEquals("")) {
            newEventName.setError("Enter Event Name");
            return false;
        }
        if (newEventDesc.getText().toString().contentEquals("")) {
            newEventDesc.setError("Enter Description");
            return false;
        }

        if (newEventVenue.getText().toString().contentEquals("")) {
            newEventVenue.setError("Enter valid Mobile Number");
            return false;
        }

        if (newEventDateTime.getText().toString().contentEquals("")) {
            newEventDateTime.setError("Password length must be more than 6 characters");
            return false;
        }
        return true;
    }
}
