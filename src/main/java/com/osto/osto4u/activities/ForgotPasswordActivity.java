package com.osto.osto4u.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.ForgotPasswordBody;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.ActivityScreenTransition;
import com.osto.osto4u.utils.AppConstants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ForgotPasswordActivity extends AppCompatActivity {
    private final String TAG = AppConstants.TAG + ForgotPasswordActivity.class.getSimpleName();

    Button recoverButton;
    EditText forgotPasswordEmail;
    ProgressBar mProgressBar;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_forgot_pwd);
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        recoverButton = (Button) findViewById(R.id.forgot_pwd_recover);
        forgotPasswordEmail = (EditText) findViewById(R.id.forgot_pwd_registered_mail);

        recoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                onBackPressed();

//                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
//                finish();
                apiCall();
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ActivityScreenTransition.animateScreen(ForgotPasswordActivity.this,
                ActivityScreenTransition.ANIM_TYPE.EXIT);
    }

    public void apiCall() {

        if (!forgotPasswordEmail.getText().toString().contentEquals("")) {
            mProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

            ForgotPasswordBody object = new ForgotPasswordBody();
            object.setEmail(forgotPasswordEmail.getText().toString());
            ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
            Call<GeneralRespModel> call = apiInterface.forgetPassword(object);
            call.enqueue(new Callback<GeneralRespModel>() {
                @Override
                public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                    if (response.isSuccessful()) {
                        GeneralRespModel forgotPwdResp = response.body();

                        if (forgotPwdResp != null) {
                            Log.e(TAG, " response Body " + forgotPwdResp.getCode());

                            if (forgotPwdResp.getCode() == 200) {

                                Toast.makeText(getApplicationContext(), forgotPwdResp.getSuccess(), Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(getApplicationContext(), LoginActivity.class));

                                finish();
                            } else {
                                Toast.makeText(getApplicationContext(), forgotPwdResp.getSuccess(), Toast.LENGTH_SHORT).show();
                            }
                        }
                        mProgressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);


                    } else {
                        Log.e(TAG, " response Failed " + response.message());
//                        startActivity(new Intent(getApplicationContext(),EntertainmentActivity.class));
                        mProgressBar.setVisibility(View.GONE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    }
//                        Toast.makeText(getApplicationContext(),"Recover Password from registered mail",Toast.LENGTH_SHORT).show();


                }

                @Override
                public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                    Log.e(TAG, " API FAILURE " + t.getMessage());
                    mProgressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });


        } else {
            forgotPasswordEmail.setError("Enter email");

        }
    }
}
