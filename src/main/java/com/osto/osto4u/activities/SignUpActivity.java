package com.osto.osto4u.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.RegisterPostBody;
import com.osto.osto4u.models.RegisterResponse;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.ActivityScreenTransition;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


public class SignUpActivity extends MainActivity implements View.OnClickListener {


    //    EditText emailET, passwordET, mobileET, userNameEt;
    Button signUpBtn;

    TextView loginRedirect;
    String signupFrom="";
    TextView termsConditions;

    CheckBox mTermsAndConditions;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        FacebookSdk.sdkInitialize(getApplicationContext());
        initialize();

    }

    public void initialize() {
        callbackManager = CallbackManager.Factory.create();
        emailET = (EditText) findViewById(R.id.email_signup);
        userNameEt = (EditText) findViewById(R.id.username_signup);

        mobileET = (EditText) findViewById(R.id.mobile_signup);
        passwordET = (EditText) findViewById(R.id.password_signup);
        signUpBtn = (Button) findViewById(R.id.signup_btn);
        facebookSignUp = (RelativeLayout) findViewById(R.id.facebook_signup);
        gmailSignUp = (RelativeLayout) findViewById(R.id.gmail_signup);
        loginRedirect = (TextView) findViewById(R.id.login_redirect);

        mTermsAndConditions = (CheckBox) findViewById(R.id.cb_terms_conditions);

        termsConditions = (TextView)findViewById(R.id.terms_signup);
        loginRedirect.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
        gmailSignUp.setOnClickListener(this);
        facebookSignUp.setOnClickListener(this);
        mProgressBar = (ProgressBar) findViewById(R.id.progressbar_signup);
        mProgressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

        passwordET.setTransformationMethod(new PasswordTransformationMethod());

        termsConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TCActivity.class));
                ActivityScreenTransition.animateScreen(SignUpActivity.this,
                        ActivityScreenTransition.ANIM_TYPE.ENTER);
            }
        });

        mTermsAndConditions.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    signUpBtn.setEnabled(true);
                } else {
                    signUpBtn.setEnabled(false);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ActivityScreenTransition.animateScreen(SignUpActivity.this,
                ActivityScreenTransition.ANIM_TYPE.EXIT);
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.signup_btn:
                //startActivity(new Intent(getApplicationContext(),EntertainmentActivity.class));
                SignUpNewUser();
                break;
            case R.id.gmail_signup:
                Log.e("Values", "Gmail SIGNUP Method");
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    Log.e("Values", "FB LoggedOut");
                }
                signupFrom = "gmail";
                googleSignIn("signup");
                break;

            case R.id.facebook_signup:
                if (OSTOApplication.getInstance().getmApiClient().isConnected()) {
//                    mApiClient.disconnect();
                    googleSignOut();
                    Log.e("Values", "Gmail LoggedOut");

                }
                signupFrom = "facebook";
                fBLogin("signup");


                break;

            case R.id.login_redirect:
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                }
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (AccessToken.getCurrentAccessToken() != null) {
            LoginManager.getInstance().logOut();
        }

    }

    public boolean invalidateFields() {

        if (userNameEt.getText().toString().contentEquals("")) {
            userNameEt.setError("Enter UserName");
            return false;
        }

        if (!Patterns.EMAIL_ADDRESS.matcher(emailET.getText()).matches()) {
            emailET.setError("Enter valid email Address");
            return false;
        }
        if (mobileET.getText().length() != 10) {
            mobileET.setError("Enter valid Mobile Number");
            return false;
        }
        if (signupFrom.contentEquals("")) {

            if (passwordET.getText().length() < 6) {
                passwordET.setError("Password length must be more than 6 characters");
                return false;
            }
        }


        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

    }


    public void SignUpNewUser() {
        if (invalidateFields()) {
            mProgressBar.setVisibility(View.VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            RegisterPostBody newUser = new RegisterPostBody();
            newUser.setEmail(emailET.getText().toString());
            newUser.setPhone(mobileET.getText().toString());
            newUser.setPassword(passwordET.getText().toString());
            newUser.setUsername(userNameEt.getText().toString());

            SharedPreferences prefs = getSharedPreferences("fb_tokenid", MODE_PRIVATE);
            String firebaseTokenId = prefs.getString("key", "");
            newUser.setDeviceid(firebaseTokenId);


            Log.e("Firebase Token", firebaseTokenId+" ");

            ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
            Call<RegisterResponse> call = apiInterface.registerNewUser(newUser);
            call.enqueue(new Callback<RegisterResponse>() {
                @Override
                public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {

                    if (response.isSuccessful()) {
                        RegisterResponse registerResponse = response.body();

                        if (registerResponse != null) {
//                            Log.e("USERID", registerResponse.getUserid() + "");
                            if (registerResponse.getCode().contentEquals("200")) {
//                                new SessionManager(getApplicationContext())
//                                        .createLoginSession(emailET.getText().toString()
//                                                ,mobileET.getText().toString()
//                                                , registerResponse.getUserid());
                                startActivity(new Intent(getApplicationContext(), VerifyOTPActivity.class)
                                        .putExtra("userid", registerResponse.getUserid())
                                        .putExtra("username", userNameEt.getText().toString())
                                        .putExtra("usermail", emailET.getText().toString()));
                                finish();
                            } else {
                                if(signupFrom.contentEquals("gmail"))
                                {
                                    gmailSignUp.setClickable(false);
                                }
                                if(signupFrom.contentEquals("facebook"))
                                {
                                    facebookSignUp.setClickable(false);
                                }
                                Toast.makeText(getApplicationContext(), registerResponse.getSuccess(), Toast.LENGTH_SHORT).show();
                            }
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "Error in Sign up, Try Again", Toast.LENGTH_SHORT).show();
                        Log.e("SIGN UP ACTIVITY", response.message() + "");
                    }

                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                    mProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onFailure(Call<RegisterResponse> call, Throwable t) {
                    Log.e("SIGN UP ACTIVITY", t.getMessage() + "");
                    mProgressBar.setVisibility(View.GONE);
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                }
            });

        }
    }
}
