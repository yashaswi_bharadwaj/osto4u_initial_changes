package com.osto.osto4u.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.adapters.MyExpandableListAdapter;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.ActivityScreenTransition;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TCActivity extends AppCompatActivity {


    ExpandableListView expandableListView;
    MyExpandableListAdapter mExpandableListAdapter;
    List<Integer> expandableListTitle;
    HashMap<Integer, Integer> expandableListDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tc);
        expandableListView = (ExpandableListView)findViewById(R.id.expandable_list_view_t_c);

        expandableListTitle = new ArrayList<>();
        expandableListDetail = new HashMap<>();

        setExpandableListValues();

        mExpandableListAdapter= new MyExpandableListAdapter(this, expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(mExpandableListAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        ActivityScreenTransition.animateScreen(TCActivity.this,
                ActivityScreenTransition.ANIM_TYPE.EXIT);
    }

    public void setExpandableListValues() {
        expandableListTitle.add(R.string.eligibility);
        expandableListTitle.add(R.string.other_t_c);
        expandableListTitle.add(R.string.use_of_osto4u_platform);
        expandableListTitle.add(R.string.do_s);
        expandableListTitle.add(R.string.don_ts);
        expandableListTitle.add(R.string.notice);
        expandableListTitle.add(R.string.payment_criteria);
        expandableListTitle.add(R.string.termination_policy);
        expandableListTitle.add(R.string.modification_of_this_agreement);
        expandableListTitle.add(R.string.disclaimer);

        expandableListDetail.put(R.string.eligibility,R.string.eligibility_answer);
        expandableListDetail.put(R.string.other_t_c,R.string.other_t_c_answer);
        expandableListDetail.put(R.string.use_of_osto4u_platform,R.string.use_of_osto4u_platform_answer);
        expandableListDetail.put(R.string.do_s,R.string.do_s_answer);
        expandableListDetail.put(R.string.don_ts,R.string.don_ts_answer);
        expandableListDetail.put(R.string.notice,R.string.notice_answer);
        expandableListDetail.put(R.string.payment_criteria,R.string.payment_criteria_answer);
        expandableListDetail.put(R.string.termination_policy,R.string.termination_policy_answer);
        expandableListDetail.put(R.string.modification_of_this_agreement,R.string.modification_of_this_agreement_answer);
        expandableListDetail.put(R.string.disclaimer,R.string.disclaimer_answer);
    }
}
