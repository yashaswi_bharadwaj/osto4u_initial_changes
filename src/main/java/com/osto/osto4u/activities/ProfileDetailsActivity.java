package com.osto.osto4u.activities;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.ProfileDetailsPostBody;
import com.osto.osto4u.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class ProfileDetailsActivity extends AppCompatActivity {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    TextView userName,userEmail,userPhone;
    RadioGroup radioGroup;
    EditText dateOfBirth,aboutYou;
    Button saveBtn;
    String gender="";
    ImageView goBack;

    String savedUserName="",savedMobile="",savedDescription="",savedEmail="";


    Calendar myCalendar = Calendar.getInstance();
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_details);

        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_create_profile_details);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        toolBarTitle.setText("My Profile");
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        aboutYou = (EditText) findViewById(R.id.about_user_details);
        dateOfBirth = (EditText)findViewById(R.id.date_of_birth);
        radioGroup = (RadioGroup)findViewById(R.id.radioGroup);
        userName = (TextView)findViewById(R.id.user_name_user_details);
        userEmail= (TextView)findViewById(R.id.email_user_details);
        userPhone= (TextView)findViewById(R.id.mobile_user_details);
        saveBtn = (Button)findViewById(R.id.save_user_details);
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateProfile(aboutYou.getText().toString(),dateOfBirth.getText().toString(),
                        "",gender);
            }
        });
        savedUserName = getIntent().getStringExtra("username");
        savedMobile= getIntent().getStringExtra("mobile");
        savedDescription = getIntent().getStringExtra("description");
        savedEmail = getIntent().getStringExtra("email");

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if(checkedId == R.id.male)
                {
                    gender= "male";
                }
                else if( checkedId == R.id.female)
                {
                    gender= "female";
                }
                else
                {
                    gender= "others";
                }
            }
        });

        userDetails();



//        dateOfBirth.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                // TODO Auto-generated method stub
//                new DatePickerDialog(getApplicationContext(), date, myCalendar
//                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
//                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
//            }
//        });

    }

    public void updateProfile(String desc,String dob,String dppath,String gender)

    {

        ProfileDetailsPostBody body = new ProfileDetailsPostBody();
        body.setUserid(String.valueOf(new SessionManager(this).getUserDetails().get("userid")));
        body.setDescription(desc);
        body.setDob(dob);
        body.setDppath(dppath);
        body.setGender(gender);


        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateProfile(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                if (response.isSuccessful()) {
                    GeneralRespModel registerResponse = response.body();

                    if (registerResponse != null) {
//                            Log.e("USERID", registerResponse.getUserid() + "");
                        if (registerResponse.getCode()==200) {
                            if(registerResponse.getSuccess().equalsIgnoreCase("Profile updated successfully"))
                            Toast.makeText(getApplicationContext(), registerResponse.getSuccess(), Toast.LENGTH_SHORT).show();
                            onBackPressed();

                        }
                    }

                } else {
                    Toast.makeText(getApplicationContext(), "Error in Sign up, Try Again", Toast.LENGTH_SHORT).show();
                    Log.e("SIGN UP ACTIVITY", response.message() + "");
                    onBackPressed();

                }

            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("SIGN UP ACTIVITY", t.getMessage() + "");
                onBackPressed();

            }
        });

    }

    private void userDetails() {
        userName.setText(savedUserName);
        userEmail.setText(savedEmail);
        userPhone.setText(savedMobile);
        aboutYou.setText(savedDescription);


    }

    DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            dateOfBirth.setText(dayOfMonth+"/"+monthOfYear+"/"+year);
            updateLabel();
        }

    };
    private void updateLabel() {
        String myFormat = "MM/dd/yy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        dateOfBirth.setText(sdf.format(myCalendar.getTime()));
    }

}
