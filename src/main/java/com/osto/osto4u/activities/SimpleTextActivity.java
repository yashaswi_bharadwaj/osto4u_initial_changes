package com.osto.osto4u.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.R;

public class SimpleTextActivity extends AppCompatActivity {

    TextView answerTV, questionTV, toolBarTitle;
    RelativeLayout mToolBarLayout;
    ImageView goBack;
    Bundle bundleExtra;
    Boolean questionToBeDisplayed;
    int questionNumber, answerNumber, toolbarHeader,explanation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple_text);
        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_activity);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        answerTV = (TextView) findViewById(R.id.answer_textview);
        questionTV = (TextView) findViewById(R.id.question_textview);
        bundleExtra = getIntent().getBundleExtra("bundle");
        questionToBeDisplayed = bundleExtra.getBoolean("questionToDisplay", false);


        if (questionToBeDisplayed) {
            questionNumber = bundleExtra.getInt("question");
            answerNumber = bundleExtra.getInt("answer");

            toolBarTitle.setText("FAQ");
            questionTV.setVisibility(View.VISIBLE);
            questionTV.setText(questionNumber);
            answerTV.setText(answerNumber);
        } else {

            toolbarHeader = bundleExtra.getInt("toolBarHeader");
            explanation = bundleExtra.getInt("explanation");

            toolBarTitle.setText(toolbarHeader);
            answerTV.setText(explanation);
            questionTV.setVisibility(View.GONE);
            findViewById(R.id.question_cardview).setVisibility(View.GONE);
            findViewById(R.id.answer_title).setVisibility(View.GONE);

        }


        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
