package com.osto.osto4u.activities;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.osto.osto4u.R;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class CreateClubsActivity extends AppCompatActivity {
    EditText newClubName, newClubDescription, category;
    Button createButton, clearButton;
    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    ImageView goBack;
    Spinner categorySpinner;
    String[] spinnerItems = new String[]{"Dance", "Dubsmash", "Images","Music","Short Movies","Standup Comedy",};


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_create_clubs);

//        Crashlytics.setString("Crashed Screen", this.getClass().getName());

        newClubName = (EditText) findViewById(R.id.create_club_name);
        newClubDescription = (EditText) findViewById(R.id.create_club_description);
        clearButton = (Button) findViewById(R.id.create_clubs_clear_btn);
        createButton = (Button)findViewById(R.id.create_clubs_create_btn);
        categorySpinner = (Spinner) findViewById(R.id.custom_spinner_create_clubs);

        mToolBarLayout = (RelativeLayout) findViewById(R.id.toolbar_activity_create_clubs);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        goBack.setVisibility(View.VISIBLE);
        toolBarTitle.setText("Create Clubs");

        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              onBackPressed();
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newClubName.setText("");
                newClubDescription.setText("");
            }
        });

        setSpinnerValues();

    }

    public void setSpinnerValues()
    {
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(getApplicationContext(),R.layout.spinner_values_textview,R.id.spinner_item_textview,spinnerItems);
        categorySpinner.setAdapter(mAdapter);
    }
}
