package com.osto.osto4u.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.R;
import com.osto.osto4u.utils.ActivityScreenTransition;
import com.osto.osto4u.utils.AppConstants;

public class WelcomeScreen extends AppCompatActivity {
    private final String TAG = AppConstants.TAG + WelcomeScreen.class.getSimpleName();

    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flash_screen);

        Log.d(TAG, "onCreate");

        sessionManager = new SessionManager(getApplicationContext());

        Thread welcomeThread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(AppConstants.SPLASH_SCREEN_DURATION);
                    startNextActivity(sessionManager.isLoggedIn());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        welcomeThread.start();
    }

    private void startNextActivity(boolean isLoggedIn) {
        Intent nextActivityIntent;

        if (isLoggedIn) {
            nextActivityIntent = new Intent(WelcomeScreen.this, BaseActivity.class);
        } else {
            nextActivityIntent = new Intent(WelcomeScreen.this, LoginActivity.class);
        }

        nextActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK
                | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(nextActivityIntent);
        finish();
        ActivityScreenTransition.animateScreen(WelcomeScreen.this,
                ActivityScreenTransition.ANIM_TYPE.ENTER);
    }
}
