package com.osto.osto4u.interfaces;

import com.osto.osto4u.fragments.GetReferalResponse;
import com.osto.osto4u.models.BirthdaysResponse;
import com.osto.osto4u.models.ChangePasswordModel;
import com.osto.osto4u.models.CommentsPostBody;
import com.osto.osto4u.models.CommentsResultModel;
import com.osto.osto4u.models.CreateEventsBody;
import com.osto.osto4u.models.CreatePostBody;
import com.osto.osto4u.models.FollowUserPostBody;
import com.osto.osto4u.models.ForgotPasswordBody;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.HomeResponse;
import com.osto.osto4u.models.InterestResponse;
import com.osto.osto4u.models.InterestsBody;
import com.osto.osto4u.models.LoginPostBody;
import com.osto.osto4u.models.LoginResponseModel;
import com.osto.osto4u.models.LoginTypeModel;
import com.osto.osto4u.models.MyInterestRespModel;
import com.osto.osto4u.models.MyProfileResp;
import com.osto.osto4u.models.NotificationResponse;
import com.osto.osto4u.models.ProfileDetailsPostBody;
import com.osto.osto4u.models.RatingPostModel;
import com.osto.osto4u.models.RegisterPostBody;
import com.osto.osto4u.models.RegisterResponse;
import com.osto.osto4u.models.SaveItemPostBody;
import com.osto.osto4u.models.SavePostResponse;
import com.osto.osto4u.models.SearchUserModelResponse;
import com.osto.osto4u.models.SharePostBody;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.models.VerifyOTPBody;
import com.osto.osto4u.models.VerifyOTPResponse;
import com.osto.osto4u.models.VerifyReferralPostBody;
import com.osto.osto4u.models.ViewCountPostBody;
import com.osto.osto4u.models.WriteCommentsPostBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by VEDAVYASA GUTTAL on 3/18/2018.
 */

public interface ApiInterface {

    @POST("/register")
    Call<RegisterResponse> registerNewUser(@Body RegisterPostBody postBody);

    @POST("/login")
    Call<LoginResponseModel> loginToApp(@Body LoginPostBody postBody);

    @POST("/logintype")
    Call<GeneralRespModel> loginFrom(@Body LoginTypeModel postBody);

    @POST("/verifyotp")
    Call<VerifyOTPResponse> verifyOTP(@Body VerifyOTPBody postBody);

    @POST("/intrest")
    Call<InterestResponse> updateInterest(@Body InterestsBody postBody);

    @POST("/getintrest")
    Call<MyInterestRespModel> getUserInterests(@Body UseridModel postBody);


    @POST("/updateintrest")
    Call<InterestResponse> updateInterestFromMyInterest(@Body InterestsBody postBody);

    @POST("/forgot")
    Call<GeneralRespModel> forgetPassword(@Body ForgotPasswordBody object);

    @POST("/home")
    Call<HomeResponse> getHomeValues(@Body UseridModel object);

    @POST("/followershome")
    Call<HomeResponse> getFollowingHome(@Body UseridModel object);



    @POST("/birthday")
    Call<BirthdaysResponse> getBirthdays(@Body UseridModel object);

    @POST("/createevents")
    Call<GeneralRespModel> createEvents(@Body CreateEventsBody object);

    @POST("/profile")
    Call<MyProfileResp> getProfile(@Body UseridModel object);


    @POST("/uploadpost")
    Call<GeneralRespModel> createPost(@Body CreatePostBody object);

    @POST("/recoverpassword")
    Call<GeneralRespModel> changePassword(@Body ChangePasswordModel object);


    @POST("/getcommenets")
    Call<CommentsResultModel> getComments(@Body CommentsPostBody object);

    @POST("/comments")
    Call<GeneralRespModel> writeComments(@Body WriteCommentsPostBody object);

    @POST("/share")
    Call<GeneralRespModel> sharePost(@Body SharePostBody object);

    @POST("/updaterating")
    Call<GeneralRespModel> updateRating(@Body RatingPostModel object);

    @POST("/saveitem")
    Call<SavePostResponse> savePost(@Body SaveItemPostBody object);

    @POST("/getrefferal")
    Call<GetReferalResponse> getReferral(@Body UseridModel object);


    @POST("/generaterefferalcode")
    Call<GetReferalResponse> generateReferral(@Body UseridModel object);


    @POST("/verifyrefferralcode")
    Call<GeneralRespModel> verifyReferralCode(@Body VerifyReferralPostBody object);


    @POST("/viewcount")
    Call<GeneralRespModel> updateViewCount(@Body ViewCountPostBody object);


    @POST("/updatenotification")
    Call<GeneralRespModel> updateNotification(@Body UseridModel object);


    @POST("/getnotification")
    Call<NotificationResponse> getNotification(@Body UseridModel object);

    @POST("/followers")
    Call<GeneralRespModel> followUpdate(@Body FollowUserPostBody object);


    @POST("/search")
    Call<SearchUserModelResponse> seacrhUser(@Body UseridModel object);

    @POST("/updateprofile")
    Call<GeneralRespModel> updateProfile(@Body ProfileDetailsPostBody object);




}
