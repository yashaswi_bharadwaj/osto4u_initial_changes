package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.ClubsModel;
import com.osto.osto4u.R;

import java.util.List;

public class ClubsRVAdapter extends RecyclerView.Adapter<ClubsRVAdapter.MyViewHolder> {

    Context mContext;
    List<ClubsModel> clubsItems;
    String from;

    public ClubsRVAdapter(Context mContext, List<ClubsModel> clubsItems, String from) {
        this.clubsItems = clubsItems;
        this.mContext = mContext;
        this.from = from;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_club_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ClubsModel model = clubsItems.get(position);

        holder.clubName.setText(model.getClubName());
        holder.ownerName.setText(model.getOwnerName());
        holder.clubDescription.setText(model.getSearchByClubDesc());
        holder.membersCount.setText("Members " + model.getMembersCount());
        holder.clubProfilePic.setImageResource(model.getClubProfilePic());
        holder.clubDescPic.setImageResource(model.getClubDescPic());
        holder.clubMembersLV.setAdapter(new CustomImageLoadAdapter(mContext, model.getMembersProfPics()));
        if (from.contentEquals("clubsFrag")) {
            holder.joinClubBtn.setVisibility(View.GONE);

//            holder.joinClubBtn.setVisibility(View.VISIBLE);

            holder.joinClubBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                }
            });
        } else
        {
            holder.joinClubBtn.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return clubsItems.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView clubName,ownerName,clubDescription,membersCount;
        ImageView clubProfilePic,clubDescPic;
        RecyclerView clubMembersLV;
        Button joinClubBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            clubName = (TextView) itemView.findViewById(R.id.search_club_name);
            ownerName = (TextView) itemView.findViewById(R.id.search_owner_name);
            membersCount = (TextView) itemView.findViewById(R.id.members_count);
            clubDescription = (TextView) itemView.findViewById(R.id.search_club_desc);
            clubProfilePic =(ImageView)itemView.findViewById(R.id.search_club_prof_pic);
            clubDescPic =(ImageView)itemView.findViewById(R.id.search_club_cover_pic);
            clubMembersLV = (RecyclerView) itemView.findViewById(R.id.members_prof_rv);
            joinClubBtn = (Button)itemView.findViewById(R.id.join_club_button);

            clubMembersLV.setLayoutManager(new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false));
        }
    }
}
