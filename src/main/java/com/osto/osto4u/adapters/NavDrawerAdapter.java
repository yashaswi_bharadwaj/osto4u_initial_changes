package com.osto.osto4u.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.osto.osto4u.R;

public class NavDrawerAdapter extends RecyclerView.Adapter<NavDrawerAdapter.MyViewHolder> {
    private LayoutInflater inflater;
    private Context context;
    String[] items;
    Integer[] icons;
    String loginFrom;

    public NavDrawerAdapter(Context context, String[] items, Integer[] icons) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.items = items;
        this.icons = icons;
        this.loginFrom = loginFrom;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.drawer_items, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.drawerItemIcon.setImageResource(icons[position]);
        holder.title.setText(items[position]);
        if (items[position].equalsIgnoreCase("Saved Items") || items[position].equalsIgnoreCase("Events")) {
            holder.goArrow.setVisibility(View.VISIBLE);
        } else {
            holder.goArrow.setVisibility(View.GONE);
        }
//        if (!new SessionManager(context).getUserDetails().get("loginFrom").toString().contentEquals("app")) {
//            if (items[position].equalsIgnoreCase("Change Password")) {
//            } else {
//
//            }
//        }


    }

    @Override
    public int getItemCount() {
        return icons.length;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView drawerItemIcon, goArrow;
        LinearLayout drawerItemsLayout;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            drawerItemIcon = (ImageView) itemView.findViewById(R.id.drawer_item_icon);
            goArrow = (ImageView) itemView.findViewById(R.id.go_arrow);
            drawerItemsLayout = (LinearLayout) itemView.findViewById(R.id.drawer_items_layout);
        }
    }
}