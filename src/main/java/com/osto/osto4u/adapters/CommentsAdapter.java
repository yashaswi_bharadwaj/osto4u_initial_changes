package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.CommentResults;
import com.osto.osto4u.R;

import java.util.List;

public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.MyViewHolder> {

    Context mContext;
    List<CommentResults> comments;
    OnClickListener listener;

    public interface OnClickListener {
        public void onClick(int question, int answer);
    };

    public CommentsAdapter(Context mContext, List<CommentResults> comments, OnClickListener listener) {
        this.comments = comments;
        this.mContext = mContext;
        this.listener=listener;
    }

    public CommentsAdapter(Context mContext, List<CommentResults> comments) {
        this.comments= comments;
        this.mContext = mContext;
    }

    @Override
    public CommentsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.comments_items, parent, false);
        return new CommentsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final CommentsAdapter.MyViewHolder holder, int position) {
        final CommentResults model = comments.get(position);
        holder.commenterName.setText(model.getUsername());
        holder.comments.setText(model.getComments());

//        holder.comments.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                listener.onClick(model.getQuestion(),model.getAnswer());
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return comments.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView commenterName,comments;

        public MyViewHolder(View itemView) {
            super(itemView);
            commenterName = (TextView) itemView.findViewById(R.id.commenter_name);
            comments= (TextView) itemView.findViewById(R.id.comments_text);

        }
    }
}
