package com.osto.osto4u.adapters;


import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.google.android.youtube.player.YouTubePlayer;
import com.hedgehog.ratingbar.RatingBar;
import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.HomeItemsModel;
import com.osto.osto4u.models.HomeResults;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;

public class HomeItemsRVAdapter1 extends RecyclerView.Adapter<HomeItemsRVAdapter1.MyViewHolder> {

    Context mContext;
    List<HomeItemsModel> homeItems;
    List<HomeResults> homeResults;
    CommentsAdapter commentsAdapter;
    ProgressBar mProgressBar;
    TextView noCommentsMessage;

    onCommentClickListener commentClickListener;
    onRateChangeListener rateChangeListener;
    onFrameClickListener frameClickListener;
    onShareClickListener shareClickListener;
    onDotMenuListener onDotMenuListener;

    public interface onShareClickListener {
        public void onItemClick(HomeResults results);
    }

    public interface onRateChangeListener {
        public void onRateChanged(HomeResults results, float ratingCount);
    }

    public interface onFrameClickListener {
        public void onItemClick(HomeResults results);
    }

    public interface onCommentClickListener {
        public void onItemClick(HomeResults results, View view);
    }

    public interface onDotMenuListener {
        public void onItemClick(HomeResults results, View view);
    }

    private YouTubePlayer.OnInitializedListener monInitializedListener;


    public HomeItemsRVAdapter1(Context mContext, List<HomeItemsModel> homeItems) {
        this.homeItems = homeItems;
        this.mContext = mContext;
    }

    public HomeItemsRVAdapter1(Context mContext, ArrayList<HomeResults> homeResults, String abc) {
        this.homeResults = homeResults;
        this.mContext = mContext;


    }

    public HomeItemsRVAdapter1(Context mContext, List<HomeResults> homeResults, onRateChangeListener onRateChangeListener,
                               onShareClickListener onShareClickListener,
                               onCommentClickListener onCommentClickListener,
                               onFrameClickListener onFrameClickListener,
                               onDotMenuListener onDotMenuListener) {
        this.homeResults = homeResults;
        this.mContext = mContext;
        this.commentClickListener = onCommentClickListener;
        this.shareClickListener = onShareClickListener;
        this.rateChangeListener = onRateChangeListener;
        this.frameClickListener = onFrameClickListener;
        this.onDotMenuListener =onDotMenuListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_screen_items1, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
//        HomeItemsModel model = homeItems.get(position);
//
//        holder.userNameTV.setText(model.getUserName());
//        holder.viewers.setText(String.valueOf(model.getViewersWatched()));
//        holder.contentDesc.setText(model.getContentDesc());
//        holder.userLocationTV.setText(model.getUserLocation());
//        holder.contentImage.setImageResource(model.getContentImage());
        final HomeResults res = homeResults.get(position);
        holder.userNameTV.setText(res.getUsername());
        holder.userLocationTV.setText(res.getLocation());
        holder.contentDesc.setText(res.getDescription());

        holder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareClickListener.onItemClick(res);
            }
        });

        holder.ratingBar.setOnRatingChangeListener(new RatingBar.OnRatingChangeListener() {
            @Override
            public void onRatingChange(float RatingCount) {
                rateChangeListener.onRateChanged(res,RatingCount);
            }
        });
        holder.commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClickListener.onItemClick(res, v);
            }
        });

        holder.contentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameClickListener.onItemClick(res);
            }
        });
        holder.mVideoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameClickListener.onItemClick(res);
            }
        });
        holder.youtubeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameClickListener.onItemClick(res);
            }
        });

        holder.homeDotMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDotMenuListener.onItemClick(res,v);
            }
        });


        Log.e("Inside Type", "Youtube " + res.getPath());

//        monInitializedListener = new YouTubePlayer.OnInitializedListener() {
//            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
//            @Override
//            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
//                if (!wasRestored) {
//                    youTubePlayer.setFullscreen(false);
//                    String[] url = res.getPath().split("=");
//                    youTubePlayer.loadVideo(url[1]);
//                    youTubePlayer.seekToMillis(100);
////                    http://img.youtube.com/vi/bQVoAWSP7k4/0.jpg
//                    youTubePlayer.play();
//
//                    youTubePlayer.setPlaybackEventListener(new YouTubePlayer.PlaybackEventListener() {
//                        @Override
//                        public void onPlaying() {
//                            Log.e("Youtube", "Playing ");
//
//                            holder.playBtn.setVisibility(View.INVISIBLE);
//                        }
//
//                        @Override
//                        public void onPaused() {
//
//                        }
//
//                        @Override
//                        public void onStopped() {
//
//                        }
//
//                        @Override
//                        public void onBuffering(boolean b) {
//
//                            Log.e("Youtube", "Buffering " + b);
//                        }
//
//                        @Override
//                        public void onSeekTo(int i) {
//
//                        }
//                    });
//
//                    holder.playBtn.setVisibility(View.INVISIBLE);
//                }
//
//                FragmentTransaction transaction = (new HomeFragment()).getChildFragmentManager().beginTransaction();
//                transaction.add(R.id.youtube_player_view, holder.youTubePlayerFragment).commit();
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//
//            }
//        };

        holder.ratingBar.setStar(res.getRating());
        holder.ratingBar.setStarEmptyDrawable(mContext.getResources().getDrawable(R.drawable.empty_like));
        holder.ratingBar.setStarFillDrawable(mContext.getResources().getDrawable(R.drawable.filled_like));
        holder.ratingBar.setStarCount(5);

        if (res.getRating() == 0) {
            Log.e("Rating", res.getRating() + "  Pos ==>" + position);

            holder.totalUserRating.setText("(0)");
        } else {
            holder.totalUserRating.setText("(" + res.getRating() + ")");
        }

        holder.ratingBar.setStar(res.getRating());
//        holder.ratingBar.halfStar(true);
        Log.e("user Rating", res.getUserrating() + "  Pos ==>" + position);
        Log.e("Rating", res.getRating() + "  Pos ==>" + position);


        if(res.getUserrating()!=null) {
            if (res.getUserrating().contentEquals("0")) {
                holder.ratingBar.setmClickable(true);
            } else {
                holder.ratingBar.setmClickable(false);
//            Toast.makeText(mContext, "You already rated this post", Toast.LENGTH_SHORT).show();
            }
        }
        else
        {
            holder.ratingBar.setmClickable(true);

        }

//        holder.ratingBar.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {

//            }
//        });

        if(res.getUserrating()!=null)
        holder.ratingBar.setStar(Float.parseFloat(res.getUserrating()));

//        holder.ratingBar.setOnRatingChangeListener(
//                new RatingBar.OnRatingChangeListener() {
//                    @Override
//                    public void onRatingChange(float RatingCount) {
////                        Toast.makeText(mContext, "the Fill star is" + RatingCount, Toast.LENGTH_SHORT).show();
//                        if (res.getRating() == 0) {
////                            mProgressBarVideo.setVisibility(View.VISIBLE);
//                            apiCallForLikes(res.getPostid(), RatingCount + "");
//                        } else {
//                            holder.ratingBar.setStar(Float.parseFloat(res.getUserrating()));
//                            Toast.makeText(mContext, "You already rated this post", Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//                });

        if (res.getCategory().equalsIgnoreCase("shortmovies")
                || res.getCategory().equalsIgnoreCase("dubsmash")
                || res.getCategory().equalsIgnoreCase("dance")
                || res.getCategory().equalsIgnoreCase("standupComedy")) {
            holder.playBtn.setVisibility(View.VISIBLE);

            holder.contentImage.setVisibility(View.INVISIBLE);
            if (res.getPath().contains("youtube")) {
                holder.playBtn.setVisibility(View.VISIBLE);
                final String youtubeEmbedPath = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/"
                        +res.getPath().split("=")[1]+"\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
                Log.e("Youtube ", "  Pos ==>" + youtubeEmbedPath);

                String thumbview ="http://img.youtube.com/vi/"+ res.getPath().split("=")[1]+"/0.jpg";
                holder.thumbnailView.setVisibility(View.GONE);
                Glide.with(mContext).load(thumbview).into(holder.thumbnailView);
                holder.playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        holder.youtubeView.setVisibility(View.VISIBLE);
                        holder.youtubeView.loadData(youtubeEmbedPath,"text/html","utf-8");

                    }
                });
                holder.youtubeView.setVisibility(View.GONE);

                holder.youtubeView.setVisibility(View.VISIBLE);

            } else {

                Log.e("Inside Type", "VideoView " + position);
                holder.youtubeView.setVisibility(View.INVISIBLE);
                holder.mVideoView.setVisibility(View.VISIBLE);
                final Uri uri = Uri.parse(res.getPath());
                holder.mVideoView.setVideoURI(uri);

                holder.mVideoView.seekTo(100);

                holder.playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mp) {

                                holder.mProgressBarVideo.setVisibility(View.GONE);
                                holder.mVideoView.start();
                                holder.playBtn.setVisibility(View.GONE);

                            }
                        });
                        // create an object of media controller
                        MediaController mediaController = new MediaController(mContext);
                        holder.mVideoView.setMediaController(mediaController);
                        holder.playBtn.setVisibility(View.GONE);
                        if (holder.mVideoView.isPlaying()) {
                            holder.playBtn.setVisibility(View.INVISIBLE);
                        } else {
                            holder.playBtn.setVisibility(View.VISIBLE);

                        }
                    }
                });
            }
        } else if (res.getCategory().equalsIgnoreCase("images")) {
            Log.e("Inside Type", "Images " + position);
            holder.playBtn.setVisibility(View.GONE);

            holder.youtubeView.setVisibility(View.INVISIBLE);
            holder.mVideoView.setVisibility(View.INVISIBLE);
            holder.contentImage.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(res.getPath()).placeholder(R.drawable.home_placeholder).into(holder.contentImage);

        } else {
            holder.contentImage.setImageResource(R.drawable.home_placeholder);
            holder.mVideoView.setVisibility(View.INVISIBLE);
            holder.contentImage.setVisibility(View.VISIBLE);
        }


//        holder.shareLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                apiCallForShare(res.getPath(), res.getPostid(), res.getShares());
//                // mProgressBarVideo.setVisibility(View.VISIBLE);
//            }
//        });
//        holder.commentLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                onShowPopup(v, res.getPostid());
//            }
//        });


        //        Glide.with(mContext).load(res.getPath()).into(holder.contentImage);

//        holder.viewers.setText(String.valueOf(model.getViewersWatched()));
//        holder.contentDesc.setText(model.getContentDesc());


    }


    /*    Show Popup    */
    PopupWindow popWindow;

//    public void onShowPopup(View v, final int postid) {
//
//        final EditText myComment;
//        Button submitCommentBtn;
//        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        View inflatedView = layoutInflater.inflate(R.layout.comments_section, null, false);
//        // find the ListView in the popup layout
//        RecyclerView listView = (RecyclerView) inflatedView.findViewById(R.id.comments_listview);
//        listView.setLayoutManager(new LinearLayoutManager(mContext));
//        LinearLayout headerView = (LinearLayout) inflatedView.findViewById(R.id.headerLayout);
//
//        mProgressBar = (ProgressBar) inflatedView.findViewById(R.id.progressbar_comments);
//        noCommentsMessage = (TextView) inflatedView.findViewById(R.id.no_comments_message);
//        noCommentsMessage.setVisibility(View.GONE);
//
//        myComment = (EditText) inflatedView.findViewById(R.id.write_comment);
//        submitCommentBtn = (Button) inflatedView.findViewById(R.id.send_comment);
//        mProgressBar.setVisibility(View.VISIBLE);
//
//        submitCommentBtn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (myComment.getText()
//                        .toString().contentEquals("")) {
//                } else {
//                    sendComment(postid, myComment.getText().toString());
//                }
//            }
//        });
//
//
//        // get device size
//        Display display;
//        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
//        display = windowManager.getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
//        int width = displayMetrics.widthPixels;
//        int height = displayMetrics.heightPixels;
//
//
//        // fill the data to the list items
//        setSimpleList(listView, postid);
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, width, height - 50, true);
//        // set a background drawable with rounders corners
//        popWindow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.rounded_blue_bg));
//
//        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
//        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//
//        popWindow.setAnimationStyle(R.style.PopupAnimation);
//
//        // show the popup at bottom of the screen and set some margin at bottom ie,
//        popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);
//    }


//    public void sendComment(int postid, String comment) {
//
//        WriteCommentsPostBody body = new WriteCommentsPostBody();
//
//
//        body.setPostid(postid + "");
//        body.setComments(comment);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.writeComments(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                List<CommentResults> commentList = new ArrayList<>();
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        Log.e("Comments", "APi Call Success");
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("comments added sucessfully")) {
//                            Log.e("Comments", "Success adding");
//                            Toast.makeText(mContext, "Comment added successfully", Toast.LENGTH_SHORT).show();
//                            popWindow.dismiss();
//                        } else {
//                            Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
//                            Log.e("Comments", "Failed");
//
//                        }
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
//
//            }
//        });
//
//    }


//    public void setSimpleList(RecyclerView mListView, int postid) {
////        QuesAnsModel model;
////        ArrayList<QuesAnsModel> commentList = new ArrayList<>();
////
////
////        model = new QuesAnsModel("Manju", " Comments made by Manju");
////        commentList.add(model);
////        model = new QuesAnsModel("Mukunda", " Comments made by Mukunda");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Manoj", " Comments made by Manoj");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Vinay", " Comments made by Vinay");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Deepak", " Comments made by Deepak");
////        commentList.add(model);
//        Log.e("Comments", "Make APi Call");
//        apiCallForComments(mListView, postid);
//
//
//    }

//    public void apiCallForComments(final RecyclerView mListView, int postid) {
//
//        CommentsPostBody body = new CommentsPostBody();
//        body.setPostid(postid);
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<CommentsResultModel> call = apiInterface.getComments(body);
//        call.enqueue(new Callback<CommentsResultModel>() {
//            @Override
//            public void onResponse(Call<CommentsResultModel> call, Response<CommentsResultModel> response) {
//
//                List<CommentResults> commentList = new ArrayList<>();
//                CommentsResultModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        mProgressBar.setVisibility(View.GONE);
//                        if (responseBody.getResults() != null) {
//                            if (responseBody.getResults().toString().contains("No comment exists")) {
//                                noCommentsMessage.setVisibility(View.VISIBLE);
//                            } else {
//                                commentList = responseBody.getResults();
//                                commentsAdapter = new CommentsAdapter(mContext, commentList);
//                                mListView.setAdapter(commentsAdapter);
//                            }
//                        }
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommentsResultModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
//
//            }
//        });
//
//
//    }


//    public void apiCallForShare(String postpath, int postid, String sharecount) {
//
//        SharePostBody body = new SharePostBody();
//        body.setPostid(postid);
//        body.setSharecount(sharecount);
//        body.setPostpath(postpath);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.sharePost(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        //   mProgressBarVideo.setVisibility(View.GONE);
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("")) {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
////                    mProgressBarVideo.setVisibility(View.GONE);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
////                mProgressBarVideo.setVisibility(View.GONE);
//
//            }
//        });
//
//
//    }


//    public void apiCallForLikes(int postid, String likes) {
//
//        RatingPostModel body = new RatingPostModel();
//        body.setPostid(postid);
//        body.setLikes(likes);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.updateRating(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        //  mProgressBarVideo.setVisibility(View.GONE);
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("Like Count updated successfully")) {
//
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
////                    mProgressBarVideo.setVisibility(View.GONE);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
////                mProgressBarVideo.setVisibility(View.GONE);
//
//            }
//        });
//    }


    @Override
    public int getItemCount() {
        return homeResults.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView userNameTV, userLocationTV, contentDesc, viewers, totalUserRating;
        ImageView contentImage, playBtn,thumbnailView,homeDotMenu;
        VideoView mVideoView;
        RatingBar ratingBar;
//        FrameLayout youtubeFrameLayout;
        LinearLayout commentLayout, shareLayout;
//        YouTubePlayerFragment youTubePlayerFragment;
        ProgressBar mProgressBarVideo;

        WebView youtubeView;



        public MyViewHolder(View itemView) {
            super(itemView);
//            youTubePlayerFragment = YouTubePlayerFragment.newInstance();


            mProgressBarVideo = (ProgressBar) itemView.findViewById(R.id.progressbar_videos_play);
            youtubeView = (WebView) itemView.findViewById(R.id.webview);

            thumbnailView = (ImageView)itemView.findViewById(R.id.home_thumbnail);

            youtubeView.getSettings().setJavaScriptEnabled(true);
            youtubeView.setWebChromeClient(new WebChromeClient() {});


            homeDotMenu = (ImageView)itemView.findViewById(R.id.home_dot_menu);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating_bar_home);
            userLocationTV = (TextView) itemView.findViewById(R.id.home_location_name);
            totalUserRating = (TextView) itemView.findViewById(R.id.total_rating_value);
            userNameTV = (TextView) itemView.findViewById(R.id.home_user_name);
            contentDesc = (TextView) itemView.findViewById(R.id.home_content_desc);
            viewers = (TextView) itemView.findViewById(R.id.home_viewers_number);
            contentImage = (ImageView) itemView.findViewById(R.id.home_user_content_pic);
            playBtn = (ImageView) itemView.findViewById(R.id.play_btn_video);
            mVideoView = (VideoView) itemView.findViewById(R.id.video_view_home);
            commentLayout = (LinearLayout) itemView.findViewById(R.id.comments_section_layout);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.share_section);


        }
    }

    //    private void addLog(final String log) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Log.e("MP3Converter"," " +log);
//            }
//        });
//    }

}
