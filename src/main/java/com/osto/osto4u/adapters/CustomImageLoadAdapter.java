package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.osto.osto4u.R;

import java.util.List;

public class CustomImageLoadAdapter extends RecyclerView.Adapter<CustomImageLoadAdapter.MyViewHolder> {


    Context mContext;
    List<Integer> clubMembersList;


    public CustomImageLoadAdapter(Context mContext, List<Integer> clubMembersList) {
        this.mContext = mContext;
        this.clubMembersList = clubMembersList;
    }

    @Override
    public CustomImageLoadAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_view_layout, parent, false);
        return new CustomImageLoadAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CustomImageLoadAdapter.MyViewHolder holder, int position) {

        holder.mImageView.setImageResource(clubMembersList.get(position));
    }

    @Override
    public int getItemCount() {
        return clubMembersList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageView;

        public MyViewHolder(View view) {
            super(view);
            mImageView = (ImageView) view.findViewById(R.id.image_view);
        }
    }
}


