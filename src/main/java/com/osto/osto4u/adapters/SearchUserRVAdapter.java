package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.FollowUserPostBody;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.SearchUserModelResponse;
import com.osto.osto4u.models.SearchUserResultsSection;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchUserRVAdapter extends RecyclerView.Adapter<SearchUserRVAdapter.MyViewHolder> {

    Context mContext;
    List<SearchUserResultsSection> userItems;
    List<SearchUserResultsSection> arrayItems;

    public SearchUserRVAdapter(Context mContext, List<SearchUserResultsSection> userItems) {
        this.userItems = userItems;
        this.mContext = mContext;

        this.arrayItems = new ArrayList<SearchUserResultsSection>();
        this.arrayItems.addAll(userItems);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_user_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final SearchUserResultsSection model = userItems.get(position);

        holder.userNameTV.setText(model.getUsername());
        if (model.getDppath() != null) {
            Glide.with(mContext).load(model.getDppath()).into(holder.contentImage);
        } else {
            holder.contentImage.setImageResource(R.drawable.prof_pic_placeholder);
        }

        if (model.isIsfollow()) {
            holder.followBtn.setVisibility(View.INVISIBLE);
        } else {
            holder.followBtn.setVisibility(View.VISIBLE);

        }
        holder.followBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.mProgressBar.setVisibility(View.VISIBLE);
                apiCallToFollow(String.valueOf(model.getUserid()), model.getUsername(),holder.followBtn,holder.mProgressBar);
//                getUserList();

            }
        });

    }

    @Override
    public int getItemCount() {
        return userItems.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView userNameTV;
        ImageView contentImage;
        Button followBtn;
        ProgressBar mProgressBar;
        public MyViewHolder(View itemView) {
            super(itemView);

            userNameTV = (TextView) itemView.findViewById(R.id.search_user_name);
            contentImage = (ImageView) itemView.findViewById(R.id.search_user_prof_pic);
            followBtn = (Button) itemView.findViewById(R.id.follow_btn_user);
            mProgressBar= (ProgressBar) itemView.findViewById(R.id.progressbar_search_user);
        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        userItems.clear();
        if (charText.length() == 0) {
            userItems.addAll(userItems);
        } else {
            for (SearchUserResultsSection wp : arrayItems) {
                if (wp.getUsername().toLowerCase(Locale.getDefault()).contains(charText)) {
                    userItems.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public void apiCallToFollow(String followersId, final String username, final Button button, final ProgressBar mProressBar) {
        FollowUserPostBody body = new FollowUserPostBody();
        body.setSourceuserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
        body.setFolloweruserid(followersId);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.followUpdate(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, retrofit2.Response<GeneralRespModel> response) {
                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getSuccess().equalsIgnoreCase("Your follower count updated")) {
                            Log.e("Follow ", " Success API Call");
                            button.setVisibility(View.INVISIBLE);
                            mProressBar.setVisibility(View.GONE);
                            Toast.makeText(mContext, "You are now following " + username, Toast.LENGTH_SHORT).show();
                        }
                    }

                } else {
                    Log.e("Follow", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Follow ", " " + t.getMessage());

            }
        });

    }

    public static final String SEARCH_USER_URL = "http://osto.herokuapp.com/search";

    public void getUserList() {

        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<SearchUserModelResponse> call = apiInterface.seacrhUser(body);
        call.enqueue(new Callback<SearchUserModelResponse>() {
            @Override
            public void onResponse(Call<SearchUserModelResponse> call, retrofit2.Response<SearchUserModelResponse> response) {

                SearchUserModelResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getCode().contentEquals("200")) {
                            Log.e("User Search", responseBody.getResults().toString());
                            notifyDataSetChanged();
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<SearchUserModelResponse> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed " + t.getMessage());

            }
        });

        // Adding request to request queue

    }


}
