package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.EventsModel;
import com.osto.osto4u.R;

import java.util.List;

public class EventsRVAdapter extends RecyclerView.Adapter<EventsRVAdapter.MyViewHolder> {

    Context mContext;
    List<EventsModel> eventsList;
    String from;

    public EventsRVAdapter(Context mContext, List<EventsModel> eventsList, String from) {
        this.eventsList = eventsList;
        this.mContext = mContext;
        this.from = from;

    }

    @Override
    public EventsRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.events_recycler_contents, parent, false);
        return new EventsRVAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(EventsRVAdapter.MyViewHolder holder, int position) {
        EventsModel model = eventsList.get(position);

        holder.eventsName.setText(model.getEventsName());
        holder.eventsDescription.setText(model.getEventsDescription());
        holder.eventsTiming.setText(model.getEventsTiming());
        holder.eventsCoverPic.setImageResource(model.getEventsCoverPic());

        if(from.contentEquals("invitation"))
        {
            holder.acceptLayout.setVisibility(View.VISIBLE);
            holder.eventsFollow.setVisibility(View.GONE);
        }
        else if  (from.contentEquals("events"))
        {
            holder.acceptLayout.setVisibility(View.GONE);
            holder.eventsFollow.setVisibility(View.VISIBLE);

        }
        else
        {
            holder.acceptLayout.setVisibility(View.GONE);
            holder.eventsFollow.setVisibility(View.GONE);

        }

    }

    @Override
    public int getItemCount() {
        return eventsList.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView eventsName,eventsTiming,eventsDescription;
        ImageView eventsContact,eventsShare,eventsMail,eventsFollow,eventsCoverPic;
        Button acceptBtn,declineBtn;
        LinearLayout acceptLayout;

        public MyViewHolder(View itemView) {
            super(itemView);

            eventsName = (TextView) itemView.findViewById(R.id.events_event_name);
            eventsTiming = (TextView) itemView.findViewById(R.id.events_event_timings);
            eventsDescription = (TextView) itemView.findViewById(R.id.events_description);

            eventsContact =(ImageView)itemView.findViewById(R.id.events_contact);
            eventsShare =(ImageView)itemView.findViewById(R.id.events_share);
            eventsMail= (ImageView) itemView.findViewById(R.id.events_mail);
            eventsFollow = (ImageView) itemView.findViewById(R.id.events_pic4);
            eventsCoverPic = (ImageView) itemView.findViewById(R.id.events_cover_pic);
            declineBtn = (Button) itemView.findViewById(R.id.decline_btn_events);
            acceptBtn = (Button) itemView.findViewById(R.id.accept_btn_events);
            acceptLayout = (LinearLayout)itemView.findViewById(R.id.accept_clear_layout_events);

        }
    }
}