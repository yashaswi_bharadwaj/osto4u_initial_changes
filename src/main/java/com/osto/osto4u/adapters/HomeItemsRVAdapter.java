package com.osto.osto4u.adapters;


import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.R;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.HomeItemsModel;
import com.osto.osto4u.models.HomeResults;
import com.osto.osto4u.models.ViewCountPostBody;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeItemsRVAdapter extends RecyclerView.Adapter<HomeItemsRVAdapter.MyViewHolder> {

    Context mContext;
    List<HomeItemsModel> homeItems;
    List<HomeResults> homeResults;
    CommentsAdapter commentsAdapter;
    ProgressBar mProgressBar;
    TextView noCommentsMessage;

    onCommentClickListener commentClickListener;
    onRateChangeListener rateChangeListener;
    onFrameClickListener frameClickListener;
    onShareClickListener shareClickListener;
    onDotMenuListener onDotMenuListener;

    public interface onShareClickListener {
        public void onItemClick(HomeResults results);
    }

    public interface onRateChangeListener {
        public void onRateChanged(RatingBar mRatingBar,HomeResults results, float ratingCount);
    }

    public interface onFrameClickListener {
        public void onItemClick(HomeResults results);
    }

    public interface onCommentClickListener {
        public void onItemClick(HomeResults results, View view);
    }

    public interface onDotMenuListener {
        public void onItemClick(HomeResults results, View view);
    }

    public HomeItemsRVAdapter(Context mContext, List<HomeResults> homeResults,
                              onRateChangeListener onRateChangeListener,
                              onShareClickListener onShareClickListener,
                              onCommentClickListener onCommentClickListener,
                              onFrameClickListener onFrameClickListener,
                              onDotMenuListener onDotMenuListener) {
        this.mContext = mContext;
        this.homeResults = homeResults;
        this.commentClickListener = onCommentClickListener;
        this.shareClickListener = onShareClickListener;
        this.rateChangeListener = onRateChangeListener;
        this.frameClickListener = onFrameClickListener;
        this.onDotMenuListener = onDotMenuListener;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_screen_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final HomeResults model = homeResults.get(position);
        holder.userNameTV.setText(model.getUsername());
        holder.userLocationTV.setText(model.getLocation());
        holder.contentDesc.setText(model.getDescription());

        if(model.getShares().contentEquals(""))
        {
            holder.shareCount.setText("(0)");
        }
        else
        {
            holder.shareCount.setText("(" + model.getShares() + ")");
        }
        holder.viewers.setText(model.getEng());

        holder.shareLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareClickListener.onItemClick(model);
            }
        });


        holder.commentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                commentClickListener.onItemClick(model, v);
            }
        });

        holder.contentImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameClickListener.onItemClick(model);
                apiCallViewCount(model.getPostid());

            }
        });

        holder.enlargeVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                frameClickListener.onItemClick(model);
                apiCallViewCount(model.getPostid());

            }
        });

        holder.homeDotMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDotMenuListener.onItemClick(model, v);
            }
        });

        holder.ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {

                if (fromUser) {
//                    holder.ratingBar.setRating(rating);
                    rateChangeListener.onRateChanged(ratingBar,model, rating);
                    holder.ratingBar.setIsIndicator(true);
                }
            }
        });
        holder.ratingBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!model.getUserrating().contentEquals("0"))
                {
                    Toast.makeText(mContext,"You already rated this post",Toast.LENGTH_SHORT).show();
                }
            }
        });



        if (model.getUserrating().contentEquals("0")) {
            holder.ratingBar.setIsIndicator(false);
        } else {
            holder.ratingBar.setIsIndicator(true);
            holder.ratingBar.setRating(Float.parseFloat(model.getUserrating()));
        }
        holder.totalUserRating.setText("(" + model.getRating() + ")");

        if (model.getCategory().equalsIgnoreCase("shortmovies")
                || model.getCategory().equalsIgnoreCase("dubsmash")
                || model.getCategory().equalsIgnoreCase("dance")
                || model.getCategory().equalsIgnoreCase("standupComedy")) {
            holder.playBtn.setVisibility(View.VISIBLE);

            holder.contentImage.setVisibility(View.GONE);

            holder.enlargeVideo.setVisibility(View.VISIBLE);
            if (model.getPath().contains("youtube")) {
                holder.youtubeView.setVisibility(View.VISIBLE);
                holder.playBtn.setVisibility(View.GONE);
                final String youtubeEmbedPath = "<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/"
                        + model.getPath().split("=")[1] + "\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe>";
                Log.e("Youtube ", "  Pos ==>" + youtubeEmbedPath);

                String thumbview = "http://img.youtube.com/vi/" + model.getPath().split("=")[1] + "/0.jpg";
                holder.thumbnailView.setVisibility(View.GONE);
                Glide.with(mContext).load(thumbview).into(holder.thumbnailView);
                holder.youtubeView.loadData(youtubeEmbedPath, "text/html", "utf-8");
                holder.mVideoView.setVisibility(View.GONE);

            } else {

                Log.e("Inside Type", "VideoView " + position);
                holder.youtubeView.setVisibility(View.GONE);
                final Uri uri = Uri.parse(model.getPath());
                holder.mVideoView.setVideoURI(uri);
                try {
                    holder.contentImage.setImageBitmap(retriveVideoFrameFromVideo(model.getPath()));
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                    holder.contentImage.setImageResource(R.drawable.home_placeholder);
                }

                holder.mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {

                        holder.mProgressBarVideo.setVisibility(View.GONE);

                    }
                });
                holder.playBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        holder.contentImage.setVisibility(View.GONE);
                        holder.mVideoView.setVisibility(View.VISIBLE);
                        holder.mVideoView.start();
                        holder.playBtn.setVisibility(View.GONE);

                        // create an object of media controller
                        MediaController mediaController = new MediaController(mContext);

                        holder.mVideoView.setMediaController(mediaController);
                        if (holder.mVideoView.isPlaying()) {
                            holder.playBtn.setVisibility(View.GONE);
                        } else {
                            holder.playBtn.setVisibility(View.VISIBLE);
                        }
                    }
                });
            }
        } else if (model.getCategory().equalsIgnoreCase("images")) {
            holder.enlargeVideo.setVisibility(View.GONE);
            Log.e("Inside Type", "Images " + position);
            holder.playBtn.setVisibility(View.GONE);

            holder.mProgressBarVideo.setVisibility(View.VISIBLE);
            holder.youtubeView.setVisibility(View.GONE);
            holder.mVideoView.setVisibility(View.GONE);
            holder.contentImage.setVisibility(View.VISIBLE);

            SimpleTarget target = new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap bitmap, GlideAnimation glideAnimation) {
                    holder.mProgressBarVideo.setVisibility(View.GONE);
                    holder.contentImage.setImageBitmap(bitmap);
                }
            };
            Glide.with(mContext).load(model.getPath()).asBitmap().placeholder(R.drawable.home_placeholder).into(target);





        } else {
            holder.contentImage.setImageResource(R.drawable.home_placeholder);
            holder.mVideoView.setVisibility(View.GONE);
            holder.contentImage.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return homeResults.size();
    }


    /*    Show Popup    */
    PopupWindow popWindow;

//    public void onShowPopup(View v, final int postid) {
//
//        final EditText myComment;
//        Button submitCommentBtn;
//        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        // inflate the custom popup layout
//        View inflatedView = layoutInflater.inflate(R.layout.comments_section, null, false);
//        // find the ListView in the popup layout
//        RecyclerView listView = (RecyclerView) inflatedView.findViewById(R.id.comments_listview);
//        listView.setLayoutManager(new LinearLayoutManager(mContext));
//        LinearLayout headerView = (LinearLayout) inflatedView.findViewById(R.id.headerLayout);
//
//        mProgressBar = (ProgressBar) inflatedView.findViewById(R.id.progressbar_comments);
//        noCommentsMessage = (TextView) inflatedView.findViewById(R.id.no_comments_message);
//        noCommentsMessage.setVisibility(View.GONE);
//
//        myComment = (EditText) inflatedView.findViewById(R.id.write_comment);
//        submitCommentBtn = (Button) inflatedView.findViewById(R.id.send_comment);
//        mProgressBar.setVisibility(View.VISIBLE);
//
//        submitCommentBtn.setOnClickListener(new View.OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                if (myComment.getText()
//                        .toString().contentEquals("")) {
//                } else {
//                    sendComment(postid, myComment.getText().toString());
//                }
//            }
//        });
//
//
//        // get device size
//        Display display;
//        WindowManager windowManager = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
//        display = windowManager.getDefaultDisplay();
//        final Point size = new Point();
//        display.getSize(size);
////        mDeviceHeight = size.y;
//        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
//        int width = displayMetrics.widthPixels;
//        int height = displayMetrics.heightPixels;
//
//
//        // fill the data to the list items
//        setSimpleList(listView, postid);
//
//        // set height depends on the device size
//        popWindow = new PopupWindow(inflatedView, width, height - 50, true);
//        // set a background drawable with rounders corners
//        popWindow.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.rounded_blue_bg));
//
//        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
//        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
//
//        popWindow.setAnimationStyle(R.style.PopupAnimation);
//
//        // show the popup at bottom of the screen and set some margin at bottom ie,
//        popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);
//    }


//    public void sendComment(int postid, String comment) {
//
//        WriteCommentsPostBody body = new WriteCommentsPostBody();
//
//
//        body.setPostid(postid + "");
//        body.setComments(comment);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.writeComments(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                List<CommentResults> commentList = new ArrayList<>();
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        Log.e("Comments", "APi Call Success");
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("comments added sucessfully")) {
//                            Log.e("Comments", "Success adding");
//                            Toast.makeText(mContext, "Comment added successfully", Toast.LENGTH_SHORT).show();
//                            popWindow.dismiss();
//                        } else {
//                            Toast.makeText(mContext, "Something went wrong", Toast.LENGTH_SHORT).show();
//                            Log.e("Comments", "Failed");
//
//                        }
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
//
//            }
//        });
//
//    }


//    public void setSimpleList(RecyclerView mListView, int postid) {
////        QuesAnsModel model;
////        ArrayList<QuesAnsModel> commentList = new ArrayList<>();
////
////
////        model = new QuesAnsModel("Manju", " Comments made by Manju");
////        commentList.add(model);
////        model = new QuesAnsModel("Mukunda", " Comments made by Mukunda");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Manoj", " Comments made by Manoj");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Vinay", " Comments made by Vinay");
////        commentList.add(model);
////
////        model = new QuesAnsModel("Deepak", " Comments made by Deepak");
////        commentList.add(model);
//        Log.e("Comments", "Make APi Call");
//        apiCallForComments(mListView, postid);
//
//
//    }

//    public void apiCallForComments(final RecyclerView mListView, int postid) {
//
//        CommentsPostBody body = new CommentsPostBody();
//        body.setPostid(postid);
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<CommentsResultModel> call = apiInterface.getComments(body);
//        call.enqueue(new Callback<CommentsResultModel>() {
//            @Override
//            public void onResponse(Call<CommentsResultModel> call, Response<CommentsResultModel> response) {
//
//                List<CommentResults> commentList = new ArrayList<>();
//                CommentsResultModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        mProgressBar.setVisibility(View.GONE);
//                        if (responseBody.getResults() != null) {
//                            if (responseBody.getResults().toString().contains("No comment exists")) {
//                                noCommentsMessage.setVisibility(View.VISIBLE);
//                            } else {
//                                commentList = responseBody.getResults();
//                                commentsAdapter = new CommentsAdapter(mContext, commentList);
//                                mListView.setAdapter(commentsAdapter);
//                            }
//                        }
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CommentsResultModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
//
//            }
//        });
//
//
//    }


//    public void apiCallForShare(String postpath, int postid, String sharecount) {
//
//        SharePostBody body = new SharePostBody();
//        body.setPostid(postid);
//        body.setSharecount(sharecount);
//        body.setPostpath(postpath);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.sharePost(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        //   mProgressBarVideo.setVisibility(View.GONE);
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("")) {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//
//                        }
//
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
////                    mProgressBarVideo.setVisibility(View.GONE);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
////                mProgressBarVideo.setVisibility(View.GONE);
//
//            }
//        });
//
//
//    }


//    public void apiCallForLikes(int postid, String likes) {
//
//        RatingPostModel body = new RatingPostModel();
//        body.setPostid(postid);
//        body.setLikes(likes);
//        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<GeneralRespModel> call = apiInterface.updateRating(body);
//        call.enqueue(new Callback<GeneralRespModel>() {
//            @Override
//            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
//
//                GeneralRespModel responseBody;
//
//                if (response.isSuccessful()) {
//                    responseBody = response.body();
//                    if (responseBody != null) {
//                        //  mProgressBarVideo.setVisibility(View.GONE);
//
//                        if (responseBody.getSuccess().equalsIgnoreCase("Like Count updated successfully")) {
//
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        } else {
//                            Toast.makeText(mContext, responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
//                        }
//
//                    }
//
//                } else {
//                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
//                    Log.e("Comments", "APi Call Fail");
////                    mProgressBarVideo.setVisibility(View.GONE);
//
//                }
//            }
//
//            @Override
//            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
//                Log.e("Comments", "APi Call Failed");
////                mProgressBarVideo.setVisibility(View.GONE);
//
//            }
//        });
//    }


    public void apiCallViewCount(int postid) {

        ViewCountPostBody body = new ViewCountPostBody();
        body.setPostid(postid);
        body.setUserid(new SessionManager(mContext).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateViewCount(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getCode() == 200) {
                            Log.e("ViewCount", "Updated");
                        } else {
                            Log.e("ViewCount", "Not Updated");
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("ViewCount", "Not Updated " + t.getMessage());
            }
        });
    }


    public class MyViewHolder extends ViewHolder {

        TextView userNameTV, userLocationTV, contentDesc, viewers, totalUserRating, shareCount;
        ImageView contentImage, playBtn, thumbnailView, homeDotMenu, enlargeVideo;
        VideoView mVideoView;
        RatingBar ratingBar;
        //        FrameLayout youtubeFrameLayout;
        RelativeLayout mViewContainer;
        LinearLayout commentLayout, shareLayout;
        //        YouTubePlayerFragment youTubePlayerFragment;
        ProgressBar mProgressBarVideo;

        WebView youtubeView;


        public MyViewHolder(View itemView) {
            super(itemView);
//            youTubePlayerFragment = YouTubePlayerFragment.newInstance();


            enlargeVideo = (ImageView) itemView.findViewById(R.id.enlarge_video);
            mProgressBarVideo = (ProgressBar) itemView.findViewById(R.id.progressbar_videos_play);
            youtubeView = (WebView) itemView.findViewById(R.id.webview);

            thumbnailView = (ImageView) itemView.findViewById(R.id.home_thumbnail);

            youtubeView.getSettings().setJavaScriptEnabled(true);
            youtubeView.setWebChromeClient(new WebChromeClient() {
            });
            mViewContainer = (RelativeLayout) itemView.findViewById(R.id.view_content_image_video);

            homeDotMenu = (ImageView) itemView.findViewById(R.id.home_dot_menu);
            ratingBar = (RatingBar) itemView.findViewById(R.id.rating_bar_home);
            userLocationTV = (TextView) itemView.findViewById(R.id.home_location_name);
            totalUserRating = (TextView) itemView.findViewById(R.id.total_rating_value);
            shareCount = (TextView) itemView.findViewById(R.id.share_count_home_screen);
            userNameTV = (TextView) itemView.findViewById(R.id.home_user_name);
            contentDesc = (TextView) itemView.findViewById(R.id.home_content_desc);
            viewers = (TextView) itemView.findViewById(R.id.home_viewers_number);
            contentImage = (ImageView) itemView.findViewById(R.id.home_user_content_pic);
            playBtn = (ImageView) itemView.findViewById(R.id.play_btn_video);
            mVideoView = (VideoView) itemView.findViewById(R.id.video_view_home);
            commentLayout = (LinearLayout) itemView.findViewById(R.id.comments_section_layout);
            shareLayout = (LinearLayout) itemView.findViewById(R.id.share_section);


        }
    }

    //    private void addLog(final String log) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Log.e("MP3Converter"," " +log);
//            }
//        });
//    }


//    public void swap(ArrayList<HomeResults> datas)
//    {
//        if(datas == null || datas.size()==0)
//            return;
//        if (homeResults != null && homeResults.size()>0)
//            homeResults.clear();
//        homeResults.addAll(datas);
//        notifyDataSetChanged();
//
//    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}
