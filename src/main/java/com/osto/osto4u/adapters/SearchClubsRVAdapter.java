package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.SearchClubsModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SearchClubsRVAdapter extends RecyclerView.Adapter<SearchClubsRVAdapter.MyViewHolder> {

    Context mContext;
    List<SearchClubsModel> userItems;
    List<SearchClubsModel> arrayItems;

    public SearchClubsRVAdapter(Context mContext, List<SearchClubsModel> userItems) {
        this.userItems = userItems;
        this.mContext = mContext;

        this.arrayItems = new ArrayList<SearchClubsModel>();
        this.arrayItems.addAll(userItems);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_user_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SearchClubsModel model = userItems.get(position);

        holder.userNameTV.setText(model.getClubName());
        holder.contentImage.setImageResource(R.drawable.prof_pic_placeholder);
//        holder.contentImage.setImageResource(model.getClubdppath());
    }

    @Override
    public int getItemCount() {
        return userItems.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView userNameTV;
        ImageView contentImage;

        public MyViewHolder(View itemView) {
            super(itemView);

            userNameTV = (TextView) itemView.findViewById(R.id.search_user_name);
            contentImage = (ImageView) itemView.findViewById(R.id.search_user_prof_pic);

        }
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        userItems.clear();
        if (charText.length() == 0) {
            userItems.addAll(userItems);
        }
        else
        {
            for (SearchClubsModel wp : arrayItems)
            {
                if (wp.getClubName().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    userItems.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }
}
