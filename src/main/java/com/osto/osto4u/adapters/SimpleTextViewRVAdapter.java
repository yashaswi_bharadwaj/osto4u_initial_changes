package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.QuesAnsModel;
import com.osto.osto4u.R;

import java.util.List;

public class SimpleTextViewRVAdapter extends RecyclerView.Adapter<SimpleTextViewRVAdapter.MyViewHolder> {

    Context mContext;
    List<QuesAnsModel> quesAns;
    OnClickListener listener;

    public interface OnClickListener {
        public void onClick(int question, int answer);
    };

    public SimpleTextViewRVAdapter(Context mContext, List<QuesAnsModel> quesAns, OnClickListener listener) {
        this.quesAns = quesAns;
        this.mContext = mContext;
        this.listener=listener;
    }

    @Override
    public SimpleTextViewRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.simple_tv, parent, false);
        return new SimpleTextViewRVAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleTextViewRVAdapter.MyViewHolder holder, int position) {
        final QuesAnsModel model = quesAns.get(position);
        holder.title.setText(model.getQuestion());

        holder.title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onClick(model.getQuestion(),model.getAnswer());
            }
        });

    }

    @Override
    public int getItemCount() {
        return quesAns.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView title;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.simple_tv_text);
        }
    }
}
