package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.NotificationResults;
import com.osto.osto4u.R;

import java.util.List;

public class NotificationsRVAdapter extends RecyclerView.Adapter<NotificationsRVAdapter.MyViewHolder> {

    Context mContext;
    List<NotificationResults> notificationResults;
    public NotificationsRVAdapter(Context mContext, List<NotificationResults> notificationResults) {
        this.notificationResults = notificationResults;
        this.mContext = mContext;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_items, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationResults model = notificationResults.get(position);

        holder.notificationMessage.setText(model.getMessage());
        holder.contentImage.setImageResource(R.drawable.prof_pic_placeholder);
    }

    @Override
    public int getItemCount() {
        return notificationResults.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView notificationMessage;
        ImageView contentImage;

        public MyViewHolder(View itemView) {
            super(itemView);

            notificationMessage = (TextView) itemView.findViewById(R.id.notification_message);
            contentImage = (ImageView) itemView.findViewById(R.id.notification_prof_pic);

        }
    }

}
