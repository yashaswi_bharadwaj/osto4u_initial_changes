package com.osto.osto4u.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindorks.placeholderview.ViewHolder;
import com.osto.osto4u.models.FansFollowModel;
import com.osto.osto4u.R;

import java.util.List;

public class FansFollowRVAdapter extends RecyclerView.Adapter<FansFollowRVAdapter.MyViewHolder> {

    Context mContext;
    List<FansFollowModel> fansModel;

    public FansFollowRVAdapter(Context mContext, List<FansFollowModel> fansModel) {
        this.fansModel = fansModel;
        this.mContext = mContext;

    }

    @Override
    public FansFollowRVAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fans_follow_contents, parent, false);
        return new FansFollowRVAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FansFollowRVAdapter.MyViewHolder holder, int position) {
        FansFollowModel model = fansModel.get(position);

        holder.fansName.setText(model.getFanName());
        holder.fansProfPic.setImageResource(model.getProfPicFans());

    }

    @Override
    public int getItemCount() {
        return fansModel.size();
    }

    public class MyViewHolder extends ViewHolder {

        TextView fansName;
        ImageView fansProfPic;
        Button requestBtn;

        public MyViewHolder(View itemView) {
            super(itemView);

            fansName = (TextView) itemView.findViewById(R.id.name_fans_follow);
            fansProfPic= (ImageView) itemView.findViewById(R.id.prof_pic_fans_follow);
            requestBtn= (Button) itemView.findViewById(R.id.request_btn_fans_follow);
        }
    }
}