package com.osto.osto4u.adapters;

/**
 * Created by VEDAVYASA GUTTAL on 4/21/2018.
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaMetadataRetriever;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.osto.osto4u.models.MyProfilePostResults;
import com.osto.osto4u.R;

import java.util.HashMap;
import java.util.List;

public class GridLayoutAdapter extends BaseAdapter {
    private Context mContext;
    List<MyProfilePostResults> myProfResults;
    itemClickListener listener;

    public interface itemClickListener
    {
         void onItemClick(MyProfilePostResults result);
    }
    // Constructor
    public GridLayoutAdapter(Context c, List<MyProfilePostResults> myProfResults, itemClickListener listener) {
        mContext = c;
        this.myProfResults = myProfResults;
        this.listener =listener;
    }

    public int getCount() {
        return myProfResults.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        View gridItemView;
        ImageView gridItem,playBtn;

        if (convertView == null) {
            gridItemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_layout_item, parent, false);
            gridItem = (ImageView) gridItemView.findViewById(R.id.grid_image);
            playBtn = (ImageView) gridItemView.findViewById(R.id.play_btn_grid_item);




            if(myProfResults.get(position).getPath()!=null)
            {
                if(myProfResults.get(position).getPath().endsWith("jpg")
                        ||myProfResults.get(position).getPath().endsWith("JPG")
                        ||myProfResults.get(position).getPath().endsWith("jpeg")
                        ||myProfResults.get(position).getPath().endsWith("JPEG")
                        ||myProfResults.get(position).getPath().endsWith("PNG")
                        ||myProfResults.get(position).getPath().endsWith("png"))

                {
                    playBtn.setVisibility(View.GONE);
                    Glide.with(mContext).load(myProfResults.get(position).getPath()).into(gridItem);
                }
//
//            if(myProfResults.get(position).getCategory().equalsIgnoreCase("images"))
//            {
//                playBtn.setVisibility(View.GONE);
//            }
                else
                {

                    if(myProfResults.get(position).getPath().contains("youtube"))
                    {
                        String thumbview = "http://img.youtube.com/vi/" + myProfResults.get(position).getPath().split("=")[1] + "/0.jpg";
                        Glide.with(mContext).load(thumbview).into(gridItem);

                    }
                    else
                    {
                        gridItem.setImageResource(R.drawable.home_placeholder);

                    }
//                tr
                    // http://img.youtube.com/vi/GDFUdMvacI0/0.jpg
                    // y {
//                    gridItem.setImageBitmap(retriveVideoFrameFromVideo(myProfResults.get(position).getPath()));
//                } catch (Throwable throwable) {

//                    throwable.printStackTrace();
//                }
                    playBtn.setVisibility(View.VISIBLE);
                    gridItem.setImageResource(R.drawable.home_placeholder);
                }
            }

            playBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(myProfResults.get(position));
                }
            });
            gridItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onItemClick(myProfResults.get(position));

                }
            });



        } else {
            gridItemView = (View) convertView;
        }
        return gridItemView;
    }

    public static Bitmap retriveVideoFrameFromVideo(String videoPath) throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        } catch (Exception e) {
            e.printStackTrace();
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)" + e.getMessage());

        } finally {
            if (mediaMetadataRetriever != null) {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }
}