package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.gson.JsonParseException;
import com.osto.osto4u.activities.FullScreenActivity;
import com.osto.osto4u.adapters.CommentsAdapter;
import com.osto.osto4u.adapters.HomeItemsRVAdapter;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.CommentResults;
import com.osto.osto4u.models.CommentsPostBody;
import com.osto.osto4u.models.CommentsResultModel;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.HomeResponse;
import com.osto.osto4u.models.HomeResults;
import com.osto.osto4u.models.NotificationResponse;
import com.osto.osto4u.models.NotificationResults;
import com.osto.osto4u.models.RatingPostModel;
import com.osto.osto4u.models.SaveItemPostBody;
import com.osto.osto4u.models.SavePostResponse;
import com.osto.osto4u.models.SharePostBody;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.models.WriteCommentsPostBody;
import com.osto.osto4u.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;


public class HomeFragment extends Fragment {

    RecyclerView mRecyclerView;
    HomeItemsRVAdapter mAdapter;
    RelativeLayout mToolbar;
    ImageView searchBtn, friendsIV, notoificationIV;
    TextView homeSegment, createPostSegment, followingSegment;
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    public ProgressBar mProgressBar;
    PopupWindow popWindow;
    EditText myComment;
    CommentsAdapter commentsAdapter;
    TextView noCommentsMessage;
    TextView notificatioCountView;
    int notificationCount;
    LinearLayout filterOptions;
    ArrayList<String> catList = new ArrayList<>();
    LinearLayout interestsNotSelected;
    TextView myInterests, noPostsSelected;
    public final String HOME_URL = "http://osto.herokuapp.com:80/home";
    List<HomeResults> displayValues;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());


        View view = inflater.inflate(R.layout.fragment_home, container, false);

        mToolbar = (RelativeLayout) view.findViewById(R.id.layout_toolbar);
        notificatioCountView= (TextView)mToolbar.findViewById(R.id.notification_count_toolbar_home);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_home_frag);

        showProgressBar();
        interestsNotSelected = (LinearLayout) view.findViewById(R.id.interests_notSelected_layout);
//        myInterests = (TextView) view.findViewById(R.id.my_interests_frag);
        noPostsSelected = (TextView) view.findViewById(R.id.no_post_message);

//        myInterests.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchFragment(new MyInterestFragment());
//            }
//        });

        catList.add("Dance");
        catList.add("Dubsmash");
        catList.add("Images");
        catList.add("Music");
        catList.add("Short Movies");
        catList.add("Standup Comedy");

        getNotifications();
        searchBtn = (ImageView) mToolbar.findViewById(R.id.search_btn_toolbar_home);
        friendsIV = (ImageView) mToolbar.findViewById(R.id.friends_btn_toolbar_home);
        notoificationIV = (ImageView) mToolbar.findViewById(R.id.notification_btn_toolbar_home);
        homeSegment = (TextView) mToolbar.findViewById(R.id.home_segment_toolbar_home);
        createPostSegment = (TextView) mToolbar.findViewById(R.id.create_post_segment_toolbar_home);
        followingSegment = (TextView) mToolbar.findViewById(R.id.following_segment_toolbar_home);

        followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
        followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));

        homeSegment.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
        homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));

        filterOptions = (LinearLayout)view.findViewById(R.id.filter_option);
        filterOptions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filterPosts(v);
            }
        });

        // getHomeScreenValues(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        Log.e("Home USERID ", new SessionManager(getActivity()).getUserDetails().get("userid").toString());
        createPostSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new CreatePostHomeFragment());
            }
        });

        followingSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new FollowingFragments());
            }
        });

        notoificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switchFragment(new NotificationFragment());
                updateNotifications();
            }
        });

        friendsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                friendsIV.setImageResource(R.drawable.fans_selected);
                homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
                homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
                followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));

                switchFragment(new FansFollowFragment());
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
                homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
                followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));

                switchFragment(new SearchUserFragment());
            }
        });


        mRecyclerView = (RecyclerView) view.findViewById(R.id.home_items_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        getHomeScreenValues(String.valueOf(new SessionManager(getActivity()).getUserDetails().get("userid")));



        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    public void switchFragment(Fragment fragment) {

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();


    }

    public void setScreenValues(List<HomeResults> results) {
        mAdapter = new HomeItemsRVAdapter(getActivity(), results,
                new HomeItemsRVAdapter.onRateChangeListener() {
                    @Override
                    public void onRateChanged(RatingBar mRatingBar,HomeResults results, float ratingCount) {
                        if (results.getUserrating() != null)

                            apiCallForLikes(mRatingBar,results.getPostid(), ratingCount);

                    }
                },
                new HomeItemsRVAdapter.onShareClickListener() {
                    @Override
                    public void onItemClick(HomeResults results) {
                        apiCallForShare(results.getPath(), results.getPostid(), results.getCategory());
                    }
                },
                new HomeItemsRVAdapter.onCommentClickListener() {
                    @Override
                    public void onItemClick(HomeResults results, View view) {
                        onShowPopup(view, results.getPostid());
                    }
                },
                new HomeItemsRVAdapter.onFrameClickListener() {
                    @Override
                    public void onItemClick(HomeResults results) {
                        Bundle bundle = new Bundle();
                        if (results.getPath().contains("youtube")) {
                            bundle.putString("category", "youtube");
                        } else if (results.getCategory().equalsIgnoreCase("images")) {
                            bundle.putString("category", "images");
                        } else {
                            bundle.putString("category", "video");
                        }
                        Intent intent = new Intent(getActivity(), FullScreenActivity.class);
                        bundle.putString("path", results.getPath());
                        intent.putExtras(bundle);
                        startActivity(intent);
                    }
                }, new HomeItemsRVAdapter.onDotMenuListener() {
            @Override
            public void onItemClick(HomeResults results, View view) {
                showDotmenu(view, results.getPostid());
            }
        });
        mRecyclerView.setAdapter(mAdapter);
        hideProgressBar();

    }

    private void showDotmenu(View view, final int postid) {

        PopupMenu popupMenu = new PopupMenu(getActivity(), view);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                apiCallToSavePost(postid);
                return true;
            }
        });
        popupMenu.inflate(R.menu.menu_save);
        popupMenu.show();

    }

//    public void getHomeScreenValues(String userid) {
//        showProgressBar();
////        UseridModel object = new UseridModel();
//
//
//        UseridModel useridModel = new UseridModel();
//        useridModel.setUserid(userid);
//
//        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
//        Call<HomeResponse> call = apiInterface.getHomeValues(useridModel);
//        call.enqueue(new Callback<HomeResponse>() {
//            @Override
//            public void onResponse(Call<HomeResponse> call, retrofit2.Response<HomeResponse> response) {
//
//                HomeResponse myResp;
//
//                if (response.isSuccessful()) {
//                    hideProgressBar();
//
////                    myResp = response.body();
//                    if (response.body() != null) {
//                        if (response.body().getResults() != null) {
//
//                            if (response.body().getResults().toString().contains("no intrest u have selected")) {
//                                Log.d("Response", response.body().getResults().toString());
//                                interestsNotSelected.setVisibility(View.VISIBLE);
//
//                            } else if (response.body().getResults().toString().contains("no posts")) {
//                                Log.d("Response", response.body().getResults().toString());
//                                interestsNotSelected.setVisibility(View.VISIBLE);
//                            } else {
////                                ArrayList<HomeResults> values = (ArrayList<HomeResults>) myResp.getResults();
//
////                                Gson gson = new Gson();
////                                Log.d("Response", response.body().getResults().toString());
////                                myResp = gson.fromJson(response.body().toString(), HomeResponse.class);
//                                //  myResp = mapper.readValue(response.toString(), HomeResponse.class);
//                                myResp = response.body();
//                                interestsNotSelected.setVisibility(View.GONE);
////                                Log.d("Response", myResp.getResults().toString());
//                                setScreenValues(myResp.getResults());
//                            }
//
//                        }
//
//
//                    } else {
//                        Log.e("Birthdays Null", response.message() + "");
//                    }
//                } else {
//                    Log.e("Birthdays api failed", response.message() + "");
//                }
//            }
//
//            @Override
//            public void onFailure(Call<HomeResponse> call, Throwable t) {
//                Log.e("OnFailure Bday ", t.getMessage() + "");
//                hideProgressBar();
//
//            }
//        });
//
//    }


    public void setSimpleList(RecyclerView mListView, int postid) {
        apiCallForComments(mListView, postid);

    }

    public void apiCallForComments(final RecyclerView mListView, int postid) {

        CommentsPostBody body = new CommentsPostBody();
        body.setPostid(postid);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<CommentsResultModel> call = apiInterface.getComments(body);
        call.enqueue(new Callback<CommentsResultModel>() {
            @Override
            public void onResponse(Call<CommentsResultModel> call, Response<CommentsResultModel> response) {

                List<CommentResults> commentList;
                CommentsResultModel responseBody;

                if (response.isSuccessful()) {

                    mProgressBar.setVisibility(View.GONE);
                    if (response.body() != null) {
                        if (response.body().getCode() == 201) {
                            if (!response.body().getResults().toString().contains("No comment exists for this particular users")) {
                                noCommentsMessage.setVisibility(View.VISIBLE);
                            }
                        } else {
                            responseBody = response.body();
                            commentList = responseBody.getResults();
                            commentsAdapter = new CommentsAdapter(getActivity(), commentList);
                            mListView.setAdapter(commentsAdapter);
                        }
                    }

                } else {
                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
                    Log.e("Comments", "APi Call Fail");

                }
            }

            @Override
            public void onFailure(Call<CommentsResultModel> call, Throwable t) {
                Log.e("Comments", "APi Call Failed " + t.getMessage());
                hideProgressBar();
                noCommentsMessage.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(),"Something went wrong",Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void apiCallForLikes(final RatingBar mRatingBar, int postid, final float likes) {

        RatingPostModel body = new RatingPostModel();
        body.setPostid(postid);
        body.setLikes(likes + "");
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateRating(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        //  mProgressBarVideo.setVisibility(View.GONE);

                        if (responseBody.getSuccess().equalsIgnoreCase("Like Count updated successfully")) {
//                            mAdapter.swap();
                            mRatingBar.setRating(likes);
                            getHomeScreenValues(String.valueOf(new SessionManager(getActivity()).getUserDetails().get("userid")));
                            Log.e("Like" ,"Rated Post Successful");
                            Toast.makeText(getActivity(), "You rated this post", Toast.LENGTH_SHORT).show();
                        } else {
                            Log.e("Like" ," " +responseBody.getSuccess());
                            Toast.makeText(getActivity(), responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
                    Log.e("Comments", "APi Call Fail");
//                    mProgressBarVideo.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Comments", "APi Call Failed");
//                mProgressBarVideo.setVisibility(View.GONE);

            }
        });
    }


    //
    public void apiCallForShare(String postpath, int postid, String sharecount) {

        SharePostBody body = new SharePostBody();
        body.setPostid(postid);
        body.setCategory(sharecount);
        body.setPostpath(postpath);
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.sharePost(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        //   mProgressBarVideo.setVisibility(View.GONE);

                        if (responseBody.getSuccess().equalsIgnoreCase("")) {
                            Toast.makeText(getActivity(), responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(getActivity(), "You shared this post", Toast.LENGTH_SHORT).show();

                        }

                    }

                } else {
                    Log.e("Share", "APi Call Fail");
//                    mProgressBarVideo.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Share", "APi Call Failed " );
//                mProgressBarVideo.setVisibility(View.GONE);

            }
        });

    }

    public void getHomeScreenValues(String userid) {
        showProgressBar();
        JSONObject object = new JSONObject();
        try {
            object.put("userid", userid);
            final ObjectMapper mapper = new ObjectMapper();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    HOME_URL, object, new com.android.volley.Response.Listener<JSONObject>() {
                HomeResponse myResp;

                @Override
                public void onResponse(JSONObject response) {

                    try {
                        hideProgressBar();
                        if (response.get("Results") != null) {
                            if (response.get("Results").toString().equalsIgnoreCase(
                                    "There is no intrest u have selected please select the intrest First")) {
                                interestsNotSelected.setVisibility(View.VISIBLE);
                                noPostsSelected.setVisibility(View.VISIBLE);
                                noPostsSelected.setText(response.get("Results").toString());
                                Log.d("Response", response.get("Results").toString());

                            } else {
                                noPostsSelected.setVisibility(View.GONE);
                                interestsNotSelected.setVisibility(View.GONE);
                                Log.d("Response==>", response.get("Results").toString());
                                myResp = mapper.readValue(response.toString(), HomeResponse.class);
                                displayValues = myResp.getResults();
                                setScreenValues(displayValues);
                            }
                        }

                    } catch (JsonParseException e) {
                        e.printStackTrace();
                        Log.d("Response Parse", e.getMessage());

                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                        Log.d("Response Mapping", e.getMessage());

                    } catch (IOException e) {
                        Log.d("Response IO", e.getMessage());
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Response Parse", error.getMessage() + "");
                }
            }) {
                @Override
                protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return com.android.volley.Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return com.android.volley.Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return com.android.volley.Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }

            };

            // Adding request to request queue
            OSTOApplication.getInstance().addToRequestQueue(jsonObjReq);
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("Response JSONExce", e.getMessage());
            hideProgressBar();
        }

    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }


    public void onShowPopup(View v, final int postid) {

        Button submitCommentBtn;
        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        View inflatedView = layoutInflater.inflate(R.layout.comments_section, null, false);
        // find the ListView in the popup layout
        RecyclerView listView = (RecyclerView) inflatedView.findViewById(R.id.comments_listview);
        listView.setLayoutManager(new LinearLayoutManager(getActivity()));
        LinearLayout headerView = (LinearLayout) inflatedView.findViewById(R.id.headerLayout);

        mProgressBar = (ProgressBar) inflatedView.findViewById(R.id.progressbar_comments);
        noCommentsMessage = (TextView) inflatedView.findViewById(R.id.no_comments_message);
        noCommentsMessage.setVisibility(View.GONE);

        myComment = (EditText) inflatedView.findViewById(R.id.write_comment);
        submitCommentBtn = (Button) inflatedView.findViewById(R.id.send_comment);
        mProgressBar.setVisibility(View.VISIBLE);

        submitCommentBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (myComment.getText()
                        .toString().contentEquals("")) {
                } else {
                    sendComment(postid, myComment.getText().toString());
                }
            }
        });


        // get device size
        Display display;
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;


        // fill the data to the list items
        setSimpleList(listView, postid);

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, width, height - 50, true);
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.rounded_blue_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);
    }

    public void sendComment(int postid, String comment) {

        WriteCommentsPostBody body = new WriteCommentsPostBody();


        body.setPostid(postid + "");
        body.setComments(comment);
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());


        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.writeComments(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                List<CommentResults> commentList = new ArrayList<>();
                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        Log.e("Comments", "APi Call Success");

                        if (responseBody.getSuccess().equalsIgnoreCase("comments added sucessfully")) {
                            Log.e("Comments", "Success adding");
                            Toast.makeText(getActivity(), "Comment added successfully", Toast.LENGTH_SHORT).show();
                            popWindow.dismiss();
                        } else {
                            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
                            Log.e("Comments", "Failed");

                        }
                    }

                } else {
                    Log.e("LOGIN ACTIVITY", response.errorBody() + "");
                    Log.e("Comments", "APi Call Fail");

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Comments", "APi Call Failed");

            }
        });

    }


    public void apiCallToSavePost(final int path) {
        Log.e("Save ", " post Id " +path );

        SaveItemPostBody body = new SaveItemPostBody();

        body.setUserid((Integer) new SessionManager(getActivity()).getUserDetails().get("userid"));
        body.setPostId(path);


        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<SavePostResponse> call = apiInterface.savePost(body);
        call.enqueue(new Callback<SavePostResponse>() {
            @Override
            public void onResponse(Call<SavePostResponse> call, Response<SavePostResponse> response) {

                SavePostResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getResults().equalsIgnoreCase(" your post saved successfully")) {
                            Toast.makeText(getActivity(), "Saved this Post", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                        }

                    }

                } else {
                    Log.e("Share ACTIVITY", response.errorBody() + "");
                    Log.e("Share", "APi Call Fail");

                }
            }

            @Override
            public void onFailure(Call<SavePostResponse> call, Throwable t) {
                Log.e("Save Post", "APi Call Failed " +t.getMessage());

            }
        });

    }

    public void getNotifications()
    {
        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.getNotification(body);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                NotificationResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getNotification()!=null)
                        {
                            Log.e("Notifications"," "+responseBody.getNotification().size());
                            setNotificationBell(responseBody.getNotification());
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed");

            }
        });
    }

    public void setNotificationBell(List<NotificationResults> notifications)
    {

        for(int i =0;i<notifications.size();i++)
        {
            if(notifications.get(i).getIsread().equalsIgnoreCase("false"))
            {
                notificationCount++;
            }
        }



        if(notificationCount>0)
        {
            notificatioCountView.setVisibility(View.VISIBLE);
            notificatioCountView.setText(notificationCount+"");
        }
        else
        {
            notificatioCountView.setVisibility(View.GONE);
        }
    }

    public void updateNotifications()
    {


        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateNotification(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getSuccess()!=null)
                        {
                            notificatioCountView.setVisibility(View.GONE);
                            Log.e("Notification", "Succesfully Cleared Notifications" + "");
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");
                }
            }
            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed");

            }
        });
    }


    public void setSpinnerValues(Spinner mSpinner) {


        ArrayAdapter<String> mAdapter = new ArrayAdapter(getActivity(), R.layout.spinner_values_textview, R.id.spinner_item_textview, catList);
//        categoryListView.setAdapter(mAdapter);
        mSpinner.setAdapter(mAdapter);
    }

    EditText addressOption;

    public void filterPosts(View v) {

        final Spinner mSpinner;
        Button applyBtn;


        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // inflate the custom popup layout
        View inflatedView = layoutInflater.inflate(R.layout.filter_posts, null, false);
        // find the ListView in the popup layout
        LinearLayout headerView = (LinearLayout) inflatedView.findViewById(R.id.headerLayout);
        mSpinner = (Spinner) inflatedView.findViewById(R.id.custom_spinner_filters);
        applyBtn = (Button) inflatedView.findViewById(R.id.create_btn_filters);
        addressOption = (EditText) inflatedView.findViewById(R.id.location_filters);


        setSpinnerValues(mSpinner);

//        mProgressBar = (ProgressBar) inflatedView.findViewById(R.id.progressbar_comments);
//        noCommentsMessage = (TextView) inflatedView.findViewById(R.id.no_comments_message);
//        noCommentsMessage.setVisibility(View.GONE);

        mProgressBar.setVisibility(View.GONE);

        applyBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (addressOption.getText()
                        .toString().contentEquals("")) {
                    addressOption.setError("Enter Location to filter");
                } else {
//                    sendComment(postid, myComment.getText().toString());
                    showFilteredOptions(addressOption.getText().toString(),mSpinner.getSelectedItem());
                    if(popWindow!=null)
                        popWindow.dismiss();
                }
            }
        });



        addressOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlacesSelection();

            }
        });
//
//        addressOption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if (hasFocus)
//                    setPlacesSelection();
//            }
//        });



        // get device size
        Display display;
        WindowManager windowManager = (WindowManager) getActivity().getSystemService(Context.WINDOW_SERVICE);
        display = windowManager.getDefaultDisplay();
        final Point size = new Point();
        display.getSize(size);
//        mDeviceHeight = size.y;
        DisplayMetrics displayMetrics = getActivity().getResources().getDisplayMetrics();
        int width = displayMetrics.widthPixels;
        int height = displayMetrics.heightPixels;


        // fill the data to the list items
//        setSimpleList(listView, postid);

        // set height depends on the device size
        popWindow = new PopupWindow(inflatedView, width, height - 50, true);
        // set a background drawable with rounders corners
        popWindow.setBackgroundDrawable(getActivity().getResources().getDrawable(R.drawable.rounded_blue_bg));

        popWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NEEDED);
        popWindow.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        popWindow.setAnimationStyle(R.style.PopupAnimation);

        // show the popup at bottom of the screen and set some margin at bottom ie,
        popWindow.showAtLocation(v, Gravity.BOTTOM, 0, 100);
    }


    private void setPlacesSelection() {

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();

        try {
            Intent mIntent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .build(getActivity());
            startActivityForResult(mIntent, 100);


        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        switch (requestCode) {

            case 100:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                    Log.e("Place", place.toString());
                    addressOption.setText(place.getAddress());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                    Log.e("Place", status.toString());
                    addressOption.setError("Please try again");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

List<HomeResults> filteredValues = new ArrayList<>();
    public void showFilteredOptions(String location, Object object)
    {
        if(displayValues!=null)
        {
            for(int i =0; i< displayValues.size();i++)
            {
                if(displayValues.get(i).getCategory()!=null && (displayValues.get(i).getCategory()!=null))
                {
                    if(String.valueOf(object).equalsIgnoreCase(displayValues.get(i).getCategory()))
                    {
                            if(location.equalsIgnoreCase(displayValues.get(i).getLocation()))
                        {
                            filteredValues.add(displayValues.get(i));
                        }
                    }
                }
            }

            setScreenValues(filteredValues);
            filteredValues = new ArrayList<>();

        }

    }

}
