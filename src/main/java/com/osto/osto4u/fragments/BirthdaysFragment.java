package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.BirthdaysResponse;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BirthdaysFragment extends Fragment {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    public final String BIRTHDAYS_URL = "http://osto.herokuapp.com:80/birthday";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());
//
        View view = inflater.inflate(R.layout.fragment_birthday, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_birthday);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        toolBarTitle.setText("Birthdays");
        apiCall(new SessionManager(getActivity()).getUserDetails().get("userid").toString());
        return view;
    }

    public void apiCall(String userId) {

        UseridModel useridModel = new UseridModel();
        useridModel.setUserid("1001");

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<BirthdaysResponse> call = apiInterface.getBirthdays(useridModel);
        call.enqueue(new Callback<BirthdaysResponse>() {
            @Override
            public void onResponse(Call<BirthdaysResponse> call, Response<BirthdaysResponse> response) {

                BirthdaysResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        Log.e("Birthdays", responseBody.getResults() + "");

                    } else {
                        Log.e("Birthdays Null", response.message() + "");
                    }
                } else {
                    Log.e("Birthdays api failed", response.message() + "");
                }
            }

            @Override
            public void onFailure(Call<BirthdaysResponse> call, Throwable t) {
                Log.e("OnFailure Bday ", t.getMessage() + "");
            }
        });

    }

    public void getBirthdays(String userid) {
        JSONObject object = new JSONObject();
        try {
            object.put("userid", userid);


            final ObjectMapper mapper = new ObjectMapper();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    BIRTHDAYS_URL, object, new com.android.volley.Response.Listener<JSONObject>() {
                BirthdaysResponse myResp;

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response.toString());

                    try {

                        myResp = mapper.readValue(response.toString(), BirthdaysResponse.class);

                        if (myResp != null) {
                            if (myResp.getCode().contentEquals("200")) {
                                Log.e("User Interests", myResp.getResults().toString());

                            }
                        }

                    } catch (JsonParseException e) {
                        e.printStackTrace();
                        Log.d("Response Parse", e.getMessage());

                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                        Log.d("Response Mapping", e.getMessage());

                    } catch (IOException e) {
                        Log.d("Response IO", e.getMessage());

                        e.printStackTrace();
                    }
                }
            }, new com.android.volley.Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Response Parse Search", error.getMessage());

                }
            }) {
                @Override
                protected com.android.volley.Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return com.android.volley.Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return com.android.volley.Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return com.android.volley.Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }

            };
            // Adding request to request queue
            OSTOApplication.getInstance().addToRequestQueue(jsonObjReq);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}