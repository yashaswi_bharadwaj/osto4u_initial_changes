package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.activities.CreateClubsActivity;
import com.osto.osto4u.adapters.ClubsRVAdapter;
import com.osto.osto4u.models.ClubsModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;

public class JoinedClubsFragment extends Fragment {

    RecyclerView mRecyclerView;
    List<ClubsModel> searchClubsItems;
    List<Integer> clubMembersList;


    RelativeLayout mToolBarLayout;
    TextView toolBarTitle,createClubs,clubsOption,clubsJoinedOption;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_clubs, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_clubs);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        createClubs = (TextView) mToolBarLayout.findViewById(R.id.toolbar_menu);
        toolBarTitle.setText("Clubs");

        createClubs.setVisibility(View.VISIBLE);
        clubsOption = (TextView) mToolBarLayout.findViewById(R.id.clubs_option);
        clubsJoinedOption = (TextView) mToolBarLayout.findViewById(R.id.clubs_joined_option);

        setBGToolbar();

        createClubs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                switchFragment(new CreateClubsFragment());
                startActivity(new Intent(getActivity(), CreateClubsActivity.class));
            }
        });

        clubsJoinedOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createClubs.setVisibility(View.VISIBLE);
                switchFragment(new JoinedClubsFragment());
            }
        });

        clubsOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createClubs.setVisibility(View.VISIBLE);
                switchFragment(new ClubsFragment());

            }
        });



        clubMembersList = new ArrayList<>();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_general);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        searchClubsItems = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            clubMembersList.add(R.drawable.prof_pic_placeholder);
        }
        ClubsModel model;

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);

        model.setOwnerName("Owner 1");
        model.setClubName("Club 1");
        model.setMembersCount(255);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);
        searchClubsItems.add(model);

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 2");
        model.setClubName("Club 2");
        model.setMembersCount(251);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);

        searchClubsItems.add(model);

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 3");
        model.setClubName("Club 3");
        model.setMembersCount(205);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);

        searchClubsItems.add(model);


        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 4");
        model.setClubName("Club 4");
        model.setMembersCount(155);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);
        searchClubsItems.add(model);

        mRecyclerView.setAdapter(new ClubsRVAdapter(getActivity(), searchClubsItems,"joinedClubs"));


        return view;
    }

    public void switchFragment(Fragment mFragment)
    {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, mFragment);
        fragmentTransaction.commit();
    }


    public void setBGToolbar() {
        clubsOption.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
        clubsJoinedOption.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));
        clubsOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        clubsJoinedOption.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
    }
    }