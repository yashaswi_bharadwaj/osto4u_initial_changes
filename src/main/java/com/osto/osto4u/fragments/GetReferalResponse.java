package com.osto.osto4u.fragments;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by VEDAVYASA GUTTAL on 4/19/2018.
 */


public class GetReferalResponse {

    @JsonProperty("Refferal code")
    String Refferalcode;

    String code,Results;

    public String getResults() {
        return Results;
    }

    public void setResults(String results) {
        Results = results;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRefferalcode() {
        return Refferalcode;
    }

    public void setRefferalcode(String refferalcode) {
        Refferalcode = refferalcode;
    }
}
