package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.adapters.FansFollowRVAdapter;
import com.osto.osto4u.models.FansFollowModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;

public class FansFollowFragment extends Fragment {


    RecyclerView mRecyclerView;
    FansFollowModel fansModel;
    List<FansFollowModel> mList;
    RelativeLayout mToolbar;
    ImageView searchBtn, notificationIV;
    TextView homeSegment, createPostSegment, followingSegment;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_fans_following, container, false);

        mToolbar = (RelativeLayout) view.findViewById(R.id.layout_toolbar_fans_follow);

        searchBtn = (ImageView) mToolbar.findViewById(R.id.search_btn_toolbar_home);
        notificationIV = (ImageView) mToolbar.findViewById(R.id.notification_btn_toolbar_home);
        homeSegment = (TextView) mToolbar.findViewById(R.id.home_segment_toolbar_home);
        createPostSegment = (TextView) mToolbar.findViewById(R.id.create_post_segment_toolbar_home);
        followingSegment = (TextView) mToolbar.findViewById(R.id.following_segment_toolbar_home);
        ((ImageView)mToolbar.findViewById(R.id.friends_btn_toolbar_home)).setImageResource(R.drawable.fans_selected);

//        homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
//        homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
//
//        followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
//        followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
//
//        createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
//        createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));
//
//
//        createPostSegment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchFragment(new CreatePostHomeFragment());
//            }
//        });
//
//        homeSegment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchFragment(new HomeFragment());
//
//            }
//        });
//        followingSegment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                switchFragment(new FollowingFragments());
//            }
//        });

        notificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new SearchUserFragment());
            }
        });


        mRecyclerView = (RecyclerView) view.findViewById(R.id.fans_follow_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mList = new ArrayList<>();

        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Meghana    Send Friend Request");
        mList.add(fansModel);
        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Ramesh    Send Friend Request");
        mList.add(fansModel);
        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Suresh    Send Friend Request");
        mList.add(fansModel);
        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Guru    Send Friend Request");
        mList.add(fansModel);
        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Vishnu    Send Friend Request");
        mList.add(fansModel);
        fansModel = new FansFollowModel(R.drawable.prof_pic_placeholder, "Deepak    Send Friend Request");
        mList.add(fansModel);

        mRecyclerView.setAdapter(new FansFollowRVAdapter(getActivity(),mList));


        return view;
    }

    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }
}
