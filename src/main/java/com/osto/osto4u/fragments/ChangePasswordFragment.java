package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.ChangePasswordModel;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordFragment extends Fragment {
    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    EditText oldPassword, newPassword, confirmPassword;
    Button createPwdBtn;
    ProgressBar mProgressBar;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_change_password, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_change_password);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        toolBarTitle.setText("Change Password");

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_change_password);
        mProgressBar.setVisibility(View.GONE);

        oldPassword = (EditText) view.findViewById(R.id.change_pwd_old_password);
        newPassword = (EditText) view.findViewById(R.id.change_pwd_new_password);
        confirmPassword = (EditText) view.findViewById(R.id.change_pwd_confirm_password);
        createPwdBtn = (Button) view.findViewById(R.id.create_pwd_create_btn);

        oldPassword.setTransformationMethod(new PasswordTransformationMethod());
        confirmPassword.setTransformationMethod(new PasswordTransformationMethod());
        newPassword.setTransformationMethod(new PasswordTransformationMethod());


        createPwdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (newPassword.getText().toString().contentEquals(confirmPassword.getText().toString())) {
                    showProgressBar();
                    apiCall(new SessionManager(getActivity()).getUserDetails().get("userid").toString());
                } else {
                    Toast.makeText(getActivity(), "Please check new passwords you entered", Toast.LENGTH_SHORT).show();
                }
            }
        });


        return view;
    }

    public void apiCall(String userId) {

        ChangePasswordModel body = new ChangePasswordModel();
        body.setUserid(userId);
        body.setOldpassword(oldPassword.getText().toString());
        body.setPassword(newPassword.getText().toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.changePassword(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                hideProgressBar();
                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getCode() == 200) {
                            Toast.makeText(getActivity(), responseBody.getSucess(), Toast.LENGTH_SHORT).show();
                            switchFragment(new HomeFragment());
                        } else {
                            Toast.makeText(getActivity(), responseBody.getSuccess(), Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Log.e("ChangePassword", response.message() + "");
                    }
                } else {
                    Log.e("ChangePassword", response.message() + "");
                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("ChangePassword", t.getMessage() + "");
                hideProgressBar();
            }
        });

    }

    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }


    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if(getActivity()!=null)

        getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if(getActivity()!=null)

            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
}