package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.adapters.MyExpandableListAdapter;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HelpAndSupportFragment extends Fragment {
    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    ImageView goBack;


    ExpandableListView expandableListView;
    MyExpandableListAdapter mExpandableListAdapter;
    List<Integer> expandableListTitle;
    HashMap<Integer, Integer> expandableListDetail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_help_and_support, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_help_support);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        toolBarTitle.setText("Help and Support");


        expandableListView = (ExpandableListView) view.findViewById(R.id.expandable_list_view_faq);


        expandableListTitle = new ArrayList<>();
        expandableListDetail = new HashMap<>();
        setExpandableListValues();

        mExpandableListAdapter = new MyExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(mExpandableListAdapter);

        return view;

    }

    public void setExpandableListValues() {
        expandableListTitle.add(R.string.question_1);
        expandableListTitle.add(R.string.question_2);
        expandableListTitle.add(R.string.question_3);
        expandableListTitle.add(R.string.question_4);
        expandableListTitle.add(R.string.question_5);
        expandableListTitle.add(R.string.question_6);
        expandableListTitle.add(R.string.question_7);
        expandableListTitle.add(R.string.question_8);
        expandableListTitle.add(R.string.question_9);
        expandableListTitle.add(R.string.question_10);
        expandableListTitle.add(R.string.question_11);
        expandableListTitle.add(R.string.question_12);
        expandableListTitle.add(R.string.question_13);
        expandableListTitle.add(R.string.question_14);
        expandableListTitle.add(R.string.question_15);
        expandableListTitle.add(R.string.question_16);
        expandableListTitle.add(R.string.question_17);
        expandableListTitle.add(R.string.question_18);
        expandableListTitle.add(R.string.question_19);
        expandableListTitle.add(R.string.question_20);
        expandableListTitle.add(R.string.question_21);
        expandableListTitle.add(R.string.question_22);
        expandableListTitle.add(R.string.question_23);
        expandableListTitle.add(R.string.question_24);
        expandableListTitle.add(R.string.question_25);
        expandableListTitle.add(R.string.question_26);

        expandableListDetail.put(R.string.question_1, R.string.answer1);
        expandableListDetail.put(R.string.question_2, R.string.answer2);
        expandableListDetail.put(R.string.question_3, R.string.answer3);
        expandableListDetail.put(R.string.question_4, R.string.answer4);
        expandableListDetail.put(R.string.question_5, R.string.answer5);
        expandableListDetail.put(R.string.question_6, R.string.answer6);
        expandableListDetail.put(R.string.question_7, R.string.answer7);
        expandableListDetail.put(R.string.question_8, R.string.answer8);
        expandableListDetail.put(R.string.question_9, R.string.answer9);
        expandableListDetail.put(R.string.question_10, R.string.answer10);
        expandableListDetail.put(R.string.question_11, R.string.answer11);
        expandableListDetail.put(R.string.question_12, R.string.answer12);
        expandableListDetail.put(R.string.question_13, R.string.answer13);
        expandableListDetail.put(R.string.question_14, R.string.answer14);
        expandableListDetail.put(R.string.question_15, R.string.answer15);
        expandableListDetail.put(R.string.question_16, R.string.answer16);
        expandableListDetail.put(R.string.question_17, R.string.answer17);
        expandableListDetail.put(R.string.question_18, R.string.answer18);
        expandableListDetail.put(R.string.question_19, R.string.answer19);
        expandableListDetail.put(R.string.question_20, R.string.answer20);

        expandableListDetail.put(R.string.question_21, R.string.answer21);
        expandableListDetail.put(R.string.question_22, R.string.answer22);
        expandableListDetail.put(R.string.question_23, R.string.answer23);
        expandableListDetail.put(R.string.question_24, R.string.answer24);
        expandableListDetail.put(R.string.question_25, R.string.answer25);
        expandableListDetail.put(R.string.question_26, R.string.answer26);

    }

}