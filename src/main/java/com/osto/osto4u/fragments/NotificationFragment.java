package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.adapters.NotificationsRVAdapter;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.NotificationResponse;
import com.osto.osto4u.models.NotificationResults;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationFragment extends Fragment {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    RecyclerView notificationRecyclerView;
    LinearLayout noNotifications;
    ImageView searchBtn;
    ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_notification, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.layout_toolbar_notification);
        searchBtn = (ImageView) mToolBarLayout.findViewById(R.id.search_btn_toolbar_home);


        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new SearchUserFragment());
            }
        });
        noNotifications =( LinearLayout)view.findViewById(R.id.no_notification_view) ;
        noNotifications.setVisibility(View.GONE);
        notificationRecyclerView = (RecyclerView)view.findViewById(R.id.notification_recycler_view);
        notificationRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mProgressBar =(ProgressBar)view.findViewById(R.id.progressbar_notification);
        apiCallToGetNotifications();

        return view;
    }

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    public void switchFragment(Fragment fragment) {

        fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();


    }
    public void apiCallToGetNotifications()
    {
        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.getNotification(body);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {
                mProgressBar.setVisibility(View.GONE);
                NotificationResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getNotification()!=null)
                        {
                            setScreenData(responseBody.getNotification());
                        }
                    }

                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e("Comments", "APi Call Failed");
                mProgressBar.setVisibility(View.GONE);

            }
        });

    }


    private void setScreenData(List<NotificationResults> notifications) {

        if(notifications.size()> 0)
        {
            noNotifications.setVisibility(View.GONE);
            notificationRecyclerView.setAdapter(new NotificationsRVAdapter(getActivity(),notifications));
        }
        else
        {
            noNotifications.setVisibility(View.VISIBLE);
        }


    }
}
