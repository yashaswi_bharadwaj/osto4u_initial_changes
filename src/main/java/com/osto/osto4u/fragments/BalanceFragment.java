package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.R;

public class BalanceFragment extends Fragment {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_balance, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.layout_toolbar_balance);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        toolBarTitle.setText("Balance");

        return view;
    }
}