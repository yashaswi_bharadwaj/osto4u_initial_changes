package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.osto.osto4u.R;

public class FullScreenView  extends Fragment {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;

    String type,path;
    ImageView mImageView;
    VideoView mVideoView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_full_screen, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_fullscreen);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
//        toolBarTitle.setText("Balance");

        type = getArguments().getString("category");
        path = getArguments().getString("path");

        mImageView = (ImageView)view.findViewById(R.id.imageview_fullscreen);
        mVideoView = (VideoView)view.findViewById(R.id.videoview_fullscreen);


        if(type.equalsIgnoreCase("images"))
        {
            mVideoView.setVisibility(View.GONE);
            mImageView.setVisibility(View.VISIBLE);
            Glide.with(getActivity()).load(path).placeholder(R.drawable.home_placeholder).into(mImageView);
        }
        else if (type.equalsIgnoreCase("video"))
        {
            mVideoView.setVisibility(View.VISIBLE);
            mImageView.setVisibility(View.GONE);

            final Uri uri = Uri.parse(path);
            mVideoView.setVideoURI(uri);

            mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mVideoView.start();
                }
            });
            MediaController mediaController = new MediaController(getActivity());
            mVideoView.setMediaController(mediaController);
        }



        return view;
    }
}

