package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.activities.FullScreenActivity;
import com.osto.osto4u.activities.ProfileDetailsActivity;
import com.osto.osto4u.adapters.GridLayoutAdapter;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.MyProfResults;
import com.osto.osto4u.models.MyProfilePostResults;
import com.osto.osto4u.models.MyProfileResp;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class MyProfileFragment extends Fragment implements View.OnClickListener {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle, toolbarEmail, toolbarMobile, toolbarFans, toolBarFollowing,
            toolbarDesc, imagesTimeline, fullTimeline, videoTimeline;
    LinearLayout noPostsTimeline;
    ProgressBar mProgressBar;
    List<MyProfilePostResults> myProfResults;
    GridView timelineGridView;
    ImageView toolbarMenu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_my_profile, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_my_profile);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        toolbarEmail = (TextView) mToolBarLayout.findViewById(R.id.toolbar_mail_id_profile);
        toolbarMobile = (TextView) mToolBarLayout.findViewById(R.id.toolbar_mobile_profile);
        toolbarFans = (TextView) mToolBarLayout.findViewById(R.id.toolbar_fans_profile);
        toolBarFollowing = (TextView) mToolBarLayout.findViewById(R.id.toolbar_following_profile);
        toolbarDesc = (TextView) mToolBarLayout.findViewById(R.id.about_user_profile);
        timelineGridView = (GridView) view.findViewById(R.id.gridview_timeline);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_my_prof);
        toolbarMenu = (ImageView) mToolBarLayout.findViewById(R.id.toolbar_menu);

        toolbarMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent mIntent =new Intent(getActivity(), ProfileDetailsActivity.class);

                mIntent.putExtra("username",toolBarTitle.getText()+"");
                mIntent.putExtra("email",toolbarEmail.getText()+"");
                mIntent.putExtra("mobile",toolbarMobile.getText()+"");
                mIntent.putExtra("description",toolbarDesc.getText()+"");

                startActivity(mIntent);


            }
        });


        fullTimeline = (TextView) view.findViewById(R.id.timeline_profile);
        imagesTimeline = (TextView) view.findViewById(R.id.images_profile);
        videoTimeline = (TextView) view.findViewById(R.id.video_profile);


        fullTimeline.setOnClickListener(this);
        videoTimeline.setOnClickListener(this);
        imagesTimeline.setOnClickListener(this);

        noPostsTimeline = (LinearLayout) view.findViewById(R.id.no_posts_timeline);
        fullTimeline.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));

        toolBarTitle.setText("My Profile");

        apiCall(new SessionManager(getActivity()).getUserDetails().get("userid").toString());
        return view;
    }


    public void apiCall(String userid) {

        showProgressBar();
        UseridModel object = new UseridModel();
        object.setUserid(userid);
        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<MyProfileResp> call = apiInterface.getProfile(object);
        call.enqueue(new Callback<MyProfileResp>() {
            @Override
            public void onResponse(Call<MyProfileResp> call, retrofit2.Response<MyProfileResp> response) {
                hideProgressBar();

                if (response.isSuccessful()) {
                    MyProfileResp myProf = response.body();

                    if (myProf != null) {
                        if (myProf.getCode() != null) {
                            if (myProf.getSucess().equalsIgnoreCase("You have shared & created, a posts")) {
                                Log.e("Profile ", " response " + myProf.getPostResults().toString());
                                Log.e("Profile ", " response " + myProf.getResults().toString());
                            } else {
                                Log.e("Profile ", " " + myProf.getCode());
                            }
                            setProfileValues(myProf);

                            if (myProf.getPostResults() != null) {
                                noPostsTimeline.setVisibility(View.GONE);
                            } else {
                                noPostsTimeline.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                } else {
                    Log.e("", " response Failed " + response.message());
                }

            }

            @Override
            public void onFailure(Call<MyProfileResp> call, Throwable t) {
                Log.e("", " API FAILURE " + t.getMessage());
                hideProgressBar();

            }
        });


    }

//
//    public void getProfile(String userid) {
//        showProgressBar();
//        JsonObject object = new JsonObject();
//        try {
//            object.addProperty("userid", userid);
//            final ObjectMapper mapper = new ObjectMapper();
//
//            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
//                    MY_PROFILE, object, new Response.Listener<JSONObject>() {
//                MyProfileResp myResp;
//
//                @Override
//                public void onResponse(JSONObject response) {
//                    Log.d("Response", response.toString());
//
//                    try {
//
//                        myResp = mapper.readValue(response.toString(), MyProfileResp.class);
//                        if (myResp.getResults().size() != 0) {
//                            setProfileValues(myResp.getResults());
//                        } else {
//
//                        }
//                        hideProgressBar();
//
//
//                    } catch (JsonParseException e) {
//                        e.printStackTrace();
//                        Log.d("Response Parse", e.getMessage());
//
//                    } catch (JsonMappingException e) {
//                        e.printStackTrace();
//                        Log.d("Response Mapping", e.getMessage());
//
//                    } catch (IOException e) {
//                        Log.d("Response IO", e.getMessage());
//                        e.printStackTrace();
//                    }
//                }
//            }, new Response.ErrorListener() {
//
//                @Override
//                public void onErrorResponse(VolleyError error) {
//                    Log.d("Response Parse", error.getMessage() + "");
//                }
//            }) {
//                @Override
//                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//                    try {
//                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
//                        if (cacheEntry == null) {
//                            cacheEntry = new Cache.Entry();
//                        }
//                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
//                        long now = System.currentTimeMillis();
//                        final long softExpire = now + cacheHitButRefreshed;
//                        final long ttl = now + cacheExpired;
//                        cacheEntry.data = response.data;
//                        cacheEntry.softTtl = softExpire;
//                        cacheEntry.ttl = ttl;
//                        String headerValue;
//                        headerValue = response.headers.get("Date");
//                        if (headerValue != null) {
//                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                        }
//                        headerValue = response.headers.get("Last-Modified");
//                        if (headerValue != null) {
//                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                        }
//                        cacheEntry.responseHeaders = response.headers;
//                        final String jsonString = new String(response.data,
//                                HttpHeaderParser.parseCharset(response.headers));
//                        return Response.success(new JSONObject(jsonString), cacheEntry);
//                    } catch (UnsupportedEncodingException e) {
//                        return Response.error(new ParseError(e));
//                    } catch (JSONException e) {
//                        return Response.error(new ParseError(e));
//                    }
//                }
//
//                @Override
//                protected void deliverResponse(JSONObject response) {
//                    super.deliverResponse(response);
//                }
//
//                @Override
//                public void deliverError(VolleyError error) {
//                    super.deliverError(error);
//                }
//
//                @Override
//                protected VolleyError parseNetworkError(VolleyError volleyError) {
//                    return super.parseNetworkError(volleyError);
//                }
//
//            };
//            // Adding request to request queue
//            OSTOApplication.getInstance().addToRequestQueue(jsonObjReq);
//        } catch (JSONException e) {
//            e.printStackTrace();
//            hideProgressBar();
//            Log.d("Response JSONExce", e.getMessage());
//        }
//
//    }

    private void setProfileValues(MyProfileResp profResults) {

        List<MyProfResults> results = profResults.getResults();

        HashSet<MyProfResults> temp = new HashSet<>();
        for (int i = 0; i < results.size(); i++) {
            temp.add(results.get(i));
        }

        results = new ArrayList<>();
        for (MyProfResults value : temp)
            results.add(value);


        if (profResults.getPostResults() != null) {
            myProfResults = profResults.getPostResults();
        }

        toolBarTitle.setText(results.get(0).getUsername());

        if (results.get(0).getDescription() == null)
            toolbarDesc.setText("Write something about you.");
        else
            toolbarDesc.setText(results.get(0).getDescription());

        toolbarEmail.setText(results.get(0).getEmail());
        toolbarMobile.setText(results.get(0).getPhone());

        if (results.get(0).getFan() == null)
            toolbarFans.setText("0");
        else
            toolbarFans.setText(results.get(0).getFan());

        if (results.get(0).getFollowing() == null)
            toolBarFollowing.setText("0");
        else
            toolBarFollowing.setText(results.get(0).getFollowing());


        if (myProfResults != null) {
            setGridValues(myProfResults);
            for(MyProfilePostResults val : myProfResults)
            {
                Log.e("Val Path", val.getPath()+"");
            }
        } else {

        }

    }


    public List<MyProfilePostResults> removeDuplicate(List<MyProfilePostResults> toBeFiltered) {
        HashSet<MyProfilePostResults> tempRes = new HashSet<>();
        tempRes.addAll(toBeFiltered);
        toBeFiltered = new ArrayList<>();
        toBeFiltered.addAll(tempRes);
        return toBeFiltered;
    }

    public void setGridValues(List<MyProfilePostResults> myProfResults) {
        myProfResults = removeDuplicate(myProfResults);
        timelineGridView.setAdapter(new GridLayoutAdapter(getActivity(), myProfResults, new GridLayoutAdapter.itemClickListener() {
            @Override
            public void onItemClick(MyProfilePostResults result) {

                Bundle bundle = new Bundle();

                if (result.getPath() != null) {
                    if (result.getPath().endsWith("jpg")
                            || result.getPath().endsWith("jpeg")
                            || result.getPath().endsWith("png")
                            || result.getPath().endsWith("PNG")
                            || result.getPath().endsWith("JPEG")
                            || result.getPath().endsWith("JPG")
                            ) {
                        bundle.putString("category", "images");
                    } else if (result.getPath().contains("youtube")) {
                        bundle.putString("category", "youtube");
                    } else {
                        bundle.putString("category", "video");
                    }
                    bundle.putString("path", result.getPath());

                    Intent mIntent = new Intent(getActivity(), FullScreenActivity.class);
                    mIntent.putExtras(bundle);
                    startActivity(new Intent(mIntent));

                }

            }
        }));
        hideProgressBar();

    }


    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    @Override
    public void onClick(View v) {
        ArrayList<MyProfilePostResults> myResults;
        showProgressBar();

        switch (v.getId()) {

            case R.id.images_profile:
                imagesTimeline.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                fullTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));
                videoTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));

                myResults = new ArrayList<>();
                if (myProfResults != null) {
                    if (myProfResults.size() > 0) {
                        for (int i = 0; i < myProfResults.size(); i++) {


                            if (myProfResults.get(i).getPath() != null) {
                                if (myProfResults.get(i).getPath().endsWith(".jpg")
                                        || myProfResults.get(i).getPath().endsWith(".jpeg")
                                        || myProfResults.get(i).getPath().endsWith(".png")
                                        || myProfResults.get(i).getPath().endsWith(".PNG")
                                        || myProfResults.get(i).getPath().endsWith(".JPEG")
                                        || myProfResults.get(i).getPath().endsWith(".JPG")
                                        ) {
                                    myResults.add(myProfResults.get(i));

                                }
                            }

//                            if (myProfResults.get(i).getCategory().equalsIgnoreCase("images")) {
//                                myResults.add(myProfResults.get(i));
//                            }
                        }
                        if (myProfResults != null)
                            setGridValues(myResults);
                    }
                }
                break;

            case R.id.video_profile:

                videoTimeline.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                fullTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));
                imagesTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));

                myResults = new ArrayList<>();
                if (myProfResults != null) {
                    if (myProfResults.size() > 0) {
                        for (int i = 0; i < myProfResults.size(); i++) {
                            if (myProfResults.get(i).getPath() != null) {
                                if (!myProfResults.get(i).getPath().contains("images")) {
                                    myResults.add(myProfResults.get(i));
                                }
                            }

//                            if (!myProfResults.get(i).getCategory().equalsIgnoreCase("images")
//                                    || !myProfResults.get(i).getCategory().equalsIgnoreCase("music")) {
//                                myResults.add(myProfResults.get(i));
//                            }
                        }
                        if (myProfResults != null)
                            setGridValues(myResults);
                    }
                }
                break;

            case R.id.timeline_profile:
                fullTimeline.setTextColor(getActivity().getResources().getColor(R.color.colorPrimaryDark));
                videoTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));
                imagesTimeline.setTextColor(getActivity().getResources().getColor(R.color.textColorPrimary));

                if (myProfResults != null)
                    setGridValues(myProfResults);
                break;

        }


    }
}