package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.adapters.MyExpandableListAdapter;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PrivacyPolicyFragment extends Fragment {
    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    ImageView goBack;

    ExpandableListView expandableListView;
    MyExpandableListAdapter mExpandableListAdapter;
    List<Integer> expandableListTitle;
    HashMap<Integer, Integer> expandableListDetail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_privacy_policy, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_privacy_policy);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        goBack = (ImageView) mToolBarLayout.findViewById(R.id.go_back_toolbar);
        toolBarTitle.setText("Privacy Policy");
        expandableListView = (ExpandableListView) view.findViewById(R.id.expandable_list_view_privacy);


        expandableListTitle = new ArrayList<>();
        expandableListDetail = new HashMap<>();
        setExpandableListValues();

        mExpandableListAdapter = new MyExpandableListAdapter(getActivity(), expandableListTitle, expandableListDetail);
        expandableListView.setAdapter(mExpandableListAdapter);

        return view;
    }

    public void setExpandableListValues() {
        expandableListTitle.add(R.string.general);
        expandableListTitle.add(R.string.personal_information);
        expandableListTitle.add(R.string.use_of_personal_information);
        expandableListTitle.add(R.string.security);


        expandableListDetail.put(R.string.general , R.string.general_answer);
        expandableListDetail.put(R.string.personal_information, R.string.personal_information_answer);
        expandableListDetail.put(R.string.use_of_personal_information, R.string.use_of_personal_information_answer);
        expandableListDetail.put(R.string.security, R.string.security_answer);

    }


}