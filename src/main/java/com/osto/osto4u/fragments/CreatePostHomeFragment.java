package com.osto.osto4u.fragments;


import android.Manifest;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.PathUtil;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.NotificationResponse;
import com.osto.osto4u.models.NotificationResults;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.services.ConvertService;
import com.osto.osto4u.R;

import java.io.File;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;

public class CreatePostHomeFragment extends Fragment {

    EditText location, youTubeLink, description;
    public static final int PICKFILE_RESULT_CODE = 100;
    public static final int SELECT_PLACE_CODE = 101;
    Spinner categorySpinner;
    RelativeLayout mToolbar;
    ImageView searchBtn, friendsIV, notoificationIV, upArrow, downArrow;
    TextView homeSegment, createPostSegment, followingSegment, selectedFileTV;
    Button createButton, browseButton;
    PlaceAutocompleteFragment places;
    String fileAbsolutePath;
    public ProgressBar mProgressBar;
    ListView categoryListView;
    String[] spinnerItems = new String[]{"Dance", "Dubsmash", "Images", "Music", "Short Movies", "Standup Comedy"};
    ArrayList<String> catList = new ArrayList<>();

    TextView notificatioCountView;
    int notificationCount;

    public boolean storagePermissionGranted = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_create_post, container, false);

        location = (EditText) view.findViewById(R.id.location_create_post);
        youTubeLink = (EditText) view.findViewById(R.id.youtube_link_create_post);
        description = (EditText) view.findViewById(R.id.post_desc_create_post);
        browseButton = (Button) view.findViewById(R.id.browse_create_post);
        createButton = (Button) view.findViewById(R.id.create_btn_create_post);
        categorySpinner = (Spinner) view.findViewById(R.id.custom_spinner_create_post);
        selectedFileTV = (TextView) view.findViewById(R.id.selected_file_create_post);
        upArrow = (ImageView) view.findViewById(R.id.up_arrow_create_post);
        downArrow = (ImageView) view.findViewById(R.id.down_arrow_create_post);
        categoryListView = (ListView) view.findViewById(R.id.category_list_view);

        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_createpost);
        mProgressBar.setVisibility(View.GONE);

//        Dexter.withActivity(getActivity())
//                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                .withListener(new PermissionListener() {
//                    @Override
//                    public void onPermissionGranted(PermissionGrantedResponse response) {
//                        // permission is granted, open the camera
//
//                    }
//
//                    @Override
//                    public void onPermissionDenied(PermissionDeniedResponse response) {
//                        // check for permanent denial of permission
//                        if (response.isPermanentlyDenied()) {
//                            // navigate user to app settings
//                        }
//                    }
//
//                    @Override
//                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
//                        token.continuePermissionRequest();
//                    }
//                }).check();
//

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showProgressBar();

                if (!location.getText().toString().contentEquals("")) {
                    if (!selectedFileTV.getText().toString().contentEquals("")
                            || !youTubeLink.getText().toString().contentEquals("")) {

                        if (selectedFileTV.getText().toString().contentEquals("")) {
                            mProgressBar.setVisibility(View.VISIBLE);
//                            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            Log.e("Spinner 1", String.valueOf(categorySpinner.getSelectedItem()));

//                            new UploadHelperS3().createPostAPICall(getActivity(),
//                                    String.valueOf(categorySpinner.getSelectedItem()),
//                                    location.getText().toString(),
//                                    "",
//                                    description.getText().toString(),
//                                    youTubeLink.getText().toString(), mProgressBar);

                            Intent intentService = new Intent(getActivity(), ConvertService.class);
                            intentService.putExtra("filePath", fileAbsolutePath);
                            intentService.putExtra("location", location.getText().toString());
                            intentService.putExtra("description", description.getText().toString());
                            intentService.putExtra("spinnerSelected", categorySpinner.getSelectedItem().toString());
                            intentService.putExtra("youtube", youTubeLink.getText() + "");

                            getActivity().startService(intentService);


                        } else {
//                            mProgressBar.setVisibility(View.VISIBLE);
                            Log.e("Spinner 2", String.valueOf(categorySpinner.getSelectedItem()));

                            Intent intentService = new Intent(getActivity(), ConvertService.class);
                            intentService.putExtra("filePath", fileAbsolutePath);
                            intentService.putExtra("location", location.getText().toString());
                            intentService.putExtra("description", description.getText().toString());
                            intentService.putExtra("spinnerSelected", categorySpinner.getSelectedItem().toString());
                            intentService.putExtra("youtube", "");

                            getActivity().startService(intentService);

//                            new ConverterHelpers().startProcess(getActivity(),
//                                    String.valueOf(categorySpinner.getSelectedItem()),
//                                    fileAbsolutePath,
//                                    location.getText().toString(),
//                                    description.getText().toString()
//                                    , mProgressBar
//                            );
                        }

                    } else {
                        Toast.makeText(getActivity(), "Please select the file or provide youtube link", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getActivity(), "Please select the location", Toast.LENGTH_SHORT).show();
                }

                Toast.makeText(getActivity(), "Your post is updating..!", Toast.LENGTH_SHORT).show();
                switchFragment(new HomeFragment());
                hideProgressBar();
            }
        });

        catList.add("Dance");
        catList.add("Dubsmash");
        catList.add("Images");
        catList.add("Music");
        catList.add("Short Movies");
        catList.add("Standup Comedy");

        setSpinnerValues();
        upArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(categoryListView.setSelection(catList))
            }
        });

        downArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        mToolbar = (RelativeLayout) view.findViewById(R.id.layout_toolbar);
        notificatioCountView= (TextView)mToolbar.findViewById(R.id.notification_count_toolbar_home);

        searchBtn = (ImageView) mToolbar.findViewById(R.id.search_btn_toolbar_home);
        friendsIV = (ImageView) mToolbar.findViewById(R.id.friends_btn_toolbar_home);
        notoificationIV = (ImageView) mToolbar.findViewById(R.id.notification_btn_toolbar_home);
        homeSegment = (TextView) mToolbar.findViewById(R.id.home_segment_toolbar_home);
        createPostSegment = (TextView) mToolbar.findViewById(R.id.create_post_segment_toolbar_home);
        followingSegment = (TextView) mToolbar.findViewById(R.id.following_segment_toolbar_home);

        createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
        createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));
        homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
        homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
        followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

        checkPermission();
//        places.setFilter(typeFilter);
        location.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setPlacesSelection();
            }
        });

        location.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    setPlacesSelection();
            }
        });

        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (storagePermissionGranted)

                {
                    Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(intent, PICKFILE_RESULT_CODE);

                } else {
                    checkPermission();
                }


            }
        });

        homeSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new HomeFragment());

            }
        });

        followingSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new FollowingFragments());
                homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
                homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
                followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));

            }
        });

        notoificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new NotificationFragment());
                updateNotifications();
            }
        });

        friendsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new FansFollowFragment());
                friendsIV.setImageResource(R.drawable.fans_selected);
                homeSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
                homeSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                followingSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
                followingSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));

                createPostSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                createPostSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));

            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new SearchUserFragment());
            }
        });
        getNotifications();

        return view;
    }

    private void setPlacesSelection() {

        AutocompleteFilter typeFilter = new AutocompleteFilter.Builder()
                .setTypeFilter(AutocompleteFilter.TYPE_FILTER_CITIES)
                .build();

        try {
            Intent mIntent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_FULLSCREEN)
                    .setFilter(typeFilter)
                    .build(getActivity());
            startActivityForResult(mIntent, SELECT_PLACE_CODE);


        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        }
    }


    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    public void setSpinnerValues() {
        ArrayAdapter<String> mAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_values_textview, R.id.spinner_item_textview, catList);
//        categoryListView.setAdapter(mAdapter);
        categorySpinner.setAdapter(mAdapter);
    }

    Uri mUri;
    File myFile;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        switch (requestCode) {
            case PICKFILE_RESULT_CODE:
                if (resultCode == RESULT_OK) {
                    String filePath = data.getData().getPath();
                    selectedFileTV.setText(filePath);
                    try {
                        fileAbsolutePath = PathUtil.getPath(getActivity(), data.getData());
                        Log.e("FilePath ", " " + fileAbsolutePath);
                    } catch (URISyntaxException e) {
                        e.printStackTrace();
                    }
                }
                break;
            case SELECT_PLACE_CODE:
                if (resultCode == RESULT_OK) {
                    Place place = PlaceAutocomplete.getPlace(getActivity(), data);
                    Log.e("Place", place.toString());
                    location.setText(place.getAddress());
                } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                    Status status = PlaceAutocomplete.getStatus(getActivity(), data);
                    Log.e("Place", status.toString());
                    location.setError("Please try again");
                } else if (resultCode == RESULT_CANCELED) {
                }
                break;

        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public String getFilePath(Context cntx, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getApplicationContext().getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            Log.e("Path cursor", " " + cursor.getString(column_index));
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public void checkPermission() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        // permission is granted, open the camera
                        storagePermissionGranted = true;
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        // check for permanent denial of permission
                        if (response.isPermanentlyDenied()) {
                            // navigate user to app settings
                            storagePermissionGranted = false;
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)

            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void getNotifications()
    {
        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.getNotification(body);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, Response<NotificationResponse> response) {

                NotificationResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getNotification()!=null)
                        {
                            Log.e("Notifications"," "+responseBody.getNotification().size());

                            setNotificationBell(responseBody.getNotification());
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed");

            }
        });
    }

    public void setNotificationBell(List<NotificationResults> notifications)
    {

        for(int i =0;i<notifications.size();i++)
        {
            if(notifications.get(i).getIsread().equalsIgnoreCase("false"))
            {
                notificationCount++;
            }
        }



        if(notificationCount>0)
        {
            notificatioCountView.setVisibility(View.VISIBLE);
            notificatioCountView.setText(notificationCount+"");
        }
        else
        {
            notificatioCountView.setVisibility(View.GONE);
        }
    }

    public void updateNotifications()
    {


        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateNotification(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getSuccess()!=null)
                        {
                            notificatioCountView.setVisibility(View.GONE);
                            Log.e("Notification", "Succesfully Cleared Notifications" + "");

                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed");

            }
        });
    }
}