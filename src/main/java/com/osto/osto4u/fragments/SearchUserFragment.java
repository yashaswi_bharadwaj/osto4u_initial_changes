package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Cache;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonObjectRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.osto.osto4u.adapters.SearchClubsRVAdapter;
import com.osto.osto4u.adapters.SearchUserRVAdapter;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.models.NotificationResponse;
import com.osto.osto4u.models.NotificationResults;
import com.osto.osto4u.models.SearchClubResultsSection;
import com.osto.osto4u.models.SearchClubsModel;
import com.osto.osto4u.models.SearchUserModel;
import com.osto.osto4u.models.SearchUserModelResponse;
import com.osto.osto4u.models.SearchUserResultsSection;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class SearchUserFragment extends Fragment {

    RecyclerView mRecyclerView;
    List<SearchUserModel> userList;
    List<SearchClubsModel> clusbsList;

    RelativeLayout mToolbar;
    TextView searchUserOption, searchClubOption;
    EditText searchEditText;
    public SearchUserRVAdapter adapter;
    public SearchClubsRVAdapter clubsAdapter;
    ImageView searchBtn, friendsIV, notificationIV;
    TextView homeSegment, createPostSegment, followingSegment;
    public static final String SEARCH_USER_URL = "http://osto.herokuapp.com/search";
    public static final String SEARCH_CLUB_URL = "http://osto.herokuapp.com/clubsearch";
    TextView notificatioCountView;
    int notificationCount;
    public ProgressBar mProgressBar;
    String whereIAm = "user";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_search_user, container, false);


        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_search_user_frag);
        mToolbar = (RelativeLayout) view.findViewById(R.id.layout_toolbar_user);
        searchBtn = (ImageView) mToolbar.findViewById(R.id.search_btn_search_user_fragment);
        searchClubOption = (TextView) mToolbar.findViewById(R.id.search_club_option);
        searchUserOption = (TextView) mToolbar.findViewById(R.id.search_user_option);
        notificationIV = (ImageView) mToolbar.findViewById(R.id.notifications_search_user_fragment);
        notificatioCountView= (TextView)mToolbar.findViewById(R.id.notification_count_toolbar_home);

        searchEditText = (EditText) view.findViewById(R.id.search_bar_et);
        getNotifications();

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (whereIAm.contentEquals("user")) {
                    adapter.filter(s.toString());

                } else {
                    clubsAdapter.filter(s.toString());

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        friendsIV = (ImageView) mToolbar.findViewById(R.id.fans_search_user_fragment);
        notificationIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new NotificationFragment());
                updateNotifications();
            }
        });

        friendsIV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchFragment(new FansFollowFragment());

            }
        });

        searchClubOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                searchEditText.setHint("Search By Clubs");
//                searchUserOption.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
//                searchClubOption.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));
//                searchUserOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
//                searchClubOption.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
//                getClubsList();
//                whereIAm = "club";
                Toast.makeText(getActivity(),"Coming soon...",Toast.LENGTH_SHORT).show();
            }
        });

        searchUserOption.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setHint("Search By Users");

                searchClubOption.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_right));
                searchUserOption.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));
                searchClubOption.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
                searchUserOption.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
                getClubsList();
                whereIAm = "user";

            }
        });

        mRecyclerView = (RecyclerView) view.findViewById(R.id.search_user_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


//        searchBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                FragmentManager fragmentManager = getFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.container_body, mFragment);
//                fragmentTransaction.commit();
//            }
//        });

//        setSearchAdapter();
        getClubsList();
        return view;
    }

    public void setSearchAdapter(List<SearchUserResultsSection> userSearchRes) {

        userList = new ArrayList<>();
        SearchUserModel userModel;

        for (int i = 0; i < userSearchRes.size(); i++) {
            userModel = new SearchUserModel(userSearchRes.get(i).getUsername(), R.drawable.prof_pic_placeholder);
            userList.add(userModel);
        }

        adapter = new SearchUserRVAdapter(getActivity(), userSearchRes);
        mRecyclerView.setAdapter(adapter);

    }

    public void setClubSearchAdapter(List<SearchClubResultsSection> userSearchRes) {

        clusbsList = new ArrayList<>();
        SearchClubsModel userModel;

        for (int i = 0; i < userSearchRes.size(); i++) {
            userModel = new SearchClubsModel(userSearchRes.get(i).getClubname(), userSearchRes.get(i).getClubdppath());
            clusbsList.add(userModel);
        }

        clubsAdapter = new SearchClubsRVAdapter(getActivity(), clusbsList);
        mRecyclerView.setAdapter(clubsAdapter);

    }

    public void switchFragment(Fragment mFragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, mFragment);
        fragmentTransaction.commit();
    }

    public void getUserList() {

        showProgressBar();
        final ObjectMapper mapper = new ObjectMapper();

        JSONObject object = new JSONObject();

        try {
            object.put("userid", new SessionManager(getActivity()).getUserDetails().get("userid").toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    SEARCH_USER_URL, null, new Response.Listener<JSONObject>() {
                SearchUserModelResponse myResp;

                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response.toString());

                    try {

                        myResp = mapper.readValue(response.toString(), SearchUserModelResponse.class);

                        if (myResp != null) {
                            if (myResp.getCode().contentEquals("200")) {
                                Log.e("User Search", myResp.getResults().toString());
                                setSearchAdapter(myResp.getResults());
                                hideProgressBar();
                            }
                        }

                    } catch (JsonParseException e) {
                        e.printStackTrace();
                        Log.d("Response Parse",""+ e.getMessage());
                        hideProgressBar();
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                        Log.d("Response Mapping", ""+e.getMessage());
                        hideProgressBar();
                    } catch (IOException e) {
                        Log.d("Response IO", ""+e.getMessage());
                        hideProgressBar();
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Response Parse Search", error.getMessage()+"");
                    hideProgressBar();

                }
            }) {
                @Override
                protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
                    try {
                        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
                        if (cacheEntry == null) {
                            cacheEntry = new Cache.Entry();
                        }
                        final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
                        final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
                        long now = System.currentTimeMillis();
                        final long softExpire = now + cacheHitButRefreshed;
                        final long ttl = now + cacheExpired;
                        cacheEntry.data = response.data;
                        cacheEntry.softTtl = softExpire;
                        cacheEntry.ttl = ttl;
                        String headerValue;
                        headerValue = response.headers.get("Date");
                        if (headerValue != null) {
                            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        headerValue = response.headers.get("Last-Modified");
                        if (headerValue != null) {
                            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
                        }
                        cacheEntry.responseHeaders = response.headers;
                        final String jsonString = new String(response.data,
                                HttpHeaderParser.parseCharset(response.headers));
                        return Response.success(new JSONObject(jsonString), cacheEntry);
                    } catch (UnsupportedEncodingException e) {
                        return Response.error(new ParseError(e));
                    } catch (JSONException e) {
                        return Response.error(new ParseError(e));
                    }
                }

                @Override
                protected void deliverResponse(JSONObject response) {
                    super.deliverResponse(response);
                }

                @Override
                public void deliverError(VolleyError error) {
                    super.deliverError(error);
                }

                @Override
                protected VolleyError parseNetworkError(VolleyError volleyError) {
                    return super.parseNetworkError(volleyError);
                }

            };
            OSTOApplication.getInstance().addToRequestQueue(jsonObjReq);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        // Adding request to request queue

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }


    public void getClubsList() {

        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<SearchUserModelResponse> call = apiInterface.seacrhUser(body);
        call.enqueue(new Callback<SearchUserModelResponse>() {
            @Override
            public void onResponse(Call<SearchUserModelResponse> call, retrofit2.Response<SearchUserModelResponse> response) {

                SearchUserModelResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if (responseBody.getCode().contentEquals("200")) {
                            Log.e("User Search", responseBody.getResults().toString());
                            setSearchAdapter(responseBody.getResults());
                            hideProgressBar();
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<SearchUserModelResponse> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed "+t.getMessage());

            }
        });




//        showProgressBar();
//        final ObjectMapper mapper = new ObjectMapper();
//
//        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
//                SEARCH_CLUB_URL, null, new Response.Listener<JSONObject>() {
//            SearchClubModelResponse myResp;
//
//            @Override
//            public void onResponse(JSONObject response) {
//                Log.d("Response", response.toString());
//
//                try {
//
//                    myResp = mapper.readValue(response.toString(), SearchClubModelResponse.class);
//
//                    if (myResp != null) {
//                        if (myResp.getCode().contentEquals("200")) {
//                            Log.e("User Search", myResp.getResults().toString());
//                            setClubSearchAdapter(myResp.getResults());
//                            hideProgressBar();
//                        }
//                    }
//
//                } catch (JsonParseException e) {
//                    e.printStackTrace();
//                    Log.d("Response Parse", e.getMessage());
//                    hideProgressBar();
//                } catch (JsonMappingException e) {
//                    e.printStackTrace();
//                    Log.d("Response Mapping", e.getMessage());
//                    hideProgressBar();
//                } catch (IOException e) {
//                    Log.d("Response IO", e.getMessage());
//                    hideProgressBar();
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("Response Parse Search", error.getMessage());
//                hideProgressBar();
//
//            }
//        }) {
//            @Override
//            protected Response<JSONObject> parseNetworkResponse(NetworkResponse response) {
//                try {
//                    Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
//                    if (cacheEntry == null) {
//                        cacheEntry = new Cache.Entry();
//                    }
//                    final long cacheHitButRefreshed = 3 * 60 * 1000; // in 3 minutes cache will be hit, but also refreshed on background
//                    final long cacheExpired = 24 * 60 * 60 * 1000; // in 24 hours this cache entry expires completely
//                    long now = System.currentTimeMillis();
//                    final long softExpire = now + cacheHitButRefreshed;
//                    final long ttl = now + cacheExpired;
//                    cacheEntry.data = response.data;
//                    cacheEntry.softTtl = softExpire;
//                    cacheEntry.ttl = ttl;
//                    String headerValue;
//                    headerValue = response.headers.get("Date");
//                    if (headerValue != null) {
//                        cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                    }
//                    headerValue = response.headers.get("Last-Modified");
//                    if (headerValue != null) {
//                        cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
//                    }
//                    cacheEntry.responseHeaders = response.headers;
//                    final String jsonString = new String(response.data,
//                            HttpHeaderParser.parseCharset(response.headers));
//                    return Response.success(new JSONObject(jsonString), cacheEntry);
//                } catch (UnsupportedEncodingException e) {
//                    return Response.error(new ParseError(e));
//                } catch (JSONException e) {
//                    return Response.error(new ParseError(e));
//                }
//            }
//
//            @Override
//            protected void deliverResponse(JSONObject response) {
//                super.deliverResponse(response);
//            }
//
//            @Override
//            public void deliverError(VolleyError error) {
//                super.deliverError(error);
//            }
//
//            @Override
//            protected VolleyError parseNetworkError(VolleyError volleyError) {
//                return super.parseNetworkError(volleyError);
//            }
//
//        };
//        // Adding request to request queue
//        OSTOApplication.getInstance().addToRequestQueue(jsonObjReq);

    }


    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void getNotifications()
    {
        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<NotificationResponse> call = apiInterface.getNotification(body);
        call.enqueue(new Callback<NotificationResponse>() {
            @Override
            public void onResponse(Call<NotificationResponse> call, retrofit2.Response<NotificationResponse> response) {

                NotificationResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getNotification()!=null)
                        {
                            Log.e("Notifications"," "+responseBody.getNotification().size());

                            setNotificationBell(responseBody.getNotification());
                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<NotificationResponse> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed "+t.getMessage());

            }
        });
    }

    public void setNotificationBell(List<NotificationResults> notifications)
    {

        for(int i =0;i<notifications.size();i++)
        {
            if(notifications.get(i).getIsread().equalsIgnoreCase("false"))
            {
                notificationCount++;
            }
        }



        if(notificationCount>0)
        {
            notificatioCountView.setVisibility(View.VISIBLE);
            notificatioCountView.setText(notificationCount+"");
        }
        else
        {
            notificatioCountView.setVisibility(View.GONE);
        }
    }

    public void updateNotifications()
    {


        UseridModel body = new UseridModel();
        body.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.updateNotification(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, retrofit2.Response<GeneralRespModel> response) {

                GeneralRespModel responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        if(responseBody.getSuccess()!=null)
                        {
                            notificatioCountView.setVisibility(View.GONE);
                            Log.e("Notification", "Succesfully Cleared Notifications" + "");

                        }
                    }
                } else {
                    Log.e("Notification", response.errorBody() + "");

                }
            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Notifications", "Api Call Failed");

            }
        });


    }


}