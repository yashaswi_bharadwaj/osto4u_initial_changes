package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.OSTOApplication;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.InterestResponse;
import com.osto.osto4u.models.InterestsBody;
import com.osto.osto4u.models.MyInterestRespModel;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;

public class MyInterestFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    Button updateInterestButton;
    CheckBox shortMoviesCB, musicCB, danceCB, dubsmashCB, imagesCB, standupComedyCB;
    public String INTERESTS_URL = "http://osto.herokuapp.com:80/getintrest";
    ArrayList<String> selectedValues;
    LinearLayout shortMoviesLayout, musicsLayout, dubsmashLayout, danceLayouts, imagesLayout, standupComedyLayout;
    public ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_my_interest, container, false);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.toolbar_activity_my_interest);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_my_interests);

        updateInterestButton = (Button) view.findViewById(R.id.update_interests);
        shortMoviesCB = (CheckBox) view.findViewById(R.id.short_movies_checkbox);
        musicCB = (CheckBox) view.findViewById(R.id.music_checkbox);
        dubsmashCB = (CheckBox) view.findViewById(R.id.dubsmash_checkbox);
        imagesCB = (CheckBox) view.findViewById(R.id.images_checkbox);
        danceCB = (CheckBox) view.findViewById(R.id.dance_checkbox);
        standupComedyCB = (CheckBox) view.findViewById(R.id.standup_comedy_checkbox);

        shortMoviesLayout = (LinearLayout) view.findViewById(R.id.short_movies_ent);
        musicsLayout = (LinearLayout) view.findViewById(R.id.music_ent);
        dubsmashLayout = (LinearLayout) view.findViewById(R.id.dubsmash_ent);
        danceLayouts = (LinearLayout) view.findViewById(R.id.dance_ent);
        imagesLayout = (LinearLayout) view.findViewById(R.id.images_ent);
        standupComedyLayout = (LinearLayout) view.findViewById(R.id.standup_comedy_ent);

        shortMoviesLayout.setOnClickListener(this);
        musicsLayout.setOnClickListener(this);
        dubsmashLayout.setOnClickListener(this);
        danceLayouts.setOnClickListener(this);
        imagesLayout.setOnClickListener(this);
        standupComedyLayout.setOnClickListener(this);
        updateInterestButton.setOnClickListener(this);

        shortMoviesCB.setOnCheckedChangeListener(this);
        musicCB.setOnCheckedChangeListener(this);
        dubsmashCB.setOnCheckedChangeListener(this);
        imagesCB.setOnCheckedChangeListener(this);
        danceCB.setOnCheckedChangeListener(this);
        standupComedyCB.setOnCheckedChangeListener(this);

        toolBarTitle.setText("My Interest");
        selectedValues = new ArrayList<>();
        showProgressBar();

        getUserInterests(new SessionManager(getActivity()).getUserDetails().get("userid").toString());
        Log.e("USERID VERIFY", " " + new SessionManager(getActivity()).getUserDetails().get("userid").toString());
//        getUserInterests("1002");
        return view;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.images_checkbox:
                if (isChecked) {
                    selectedValues.add("Images");
                } else {
                    selectedValues.remove("Images");
                }
                break;
            case R.id.dubsmash_checkbox:
                if (isChecked) {
                    selectedValues.add("Dubsmash");
                } else {
                    selectedValues.remove("Dubsmash");
                }
                break;
            case R.id.short_movies_checkbox:
                if (isChecked) {
                    selectedValues.add("ShortMovies");
                } else {
                    selectedValues.remove("ShortMovies");
                }
                break;
            case R.id.dance_checkbox:
                if (isChecked) {
                    selectedValues.add("Dance");
                } else {
                    selectedValues.remove("Dance");
                }
                break;
            case R.id.standup_comedy_checkbox:
                if (isChecked) {
                    selectedValues.add("StandupComedy");
                } else {
                    selectedValues.remove("StandupComedy");
                }
                break;
            case R.id.music_checkbox:
                if (isChecked) {
                    selectedValues.add("Music");
                } else {
                    selectedValues.remove("Music");
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.update_interests:

                if (selectedValues.size() == 0) {
                    Toast.makeText(getActivity(), "Select atleast one Interest", Toast.LENGTH_SHORT).show();

                } else {
                    submitInterestsApiCall();

                }
                break;
            case R.id.short_movies_ent:
                if (shortMoviesCB.isChecked()) {
                    shortMoviesCB.setChecked(false);
                } else {
                    shortMoviesCB.setChecked(true);

                }

                break;
            case R.id.dubsmash_ent:
                if (dubsmashCB.isChecked()) {
                    dubsmashCB.setChecked(false);
                } else {
                    dubsmashCB.setChecked(true);

                }

                break;
            case R.id.standup_comedy_ent:

                if (standupComedyCB.isChecked()) {
                    standupComedyCB.setChecked(false);
                } else {
                    standupComedyCB.setChecked(true);

                }

                break;
            case R.id.images_ent:
                if (imagesCB.isChecked()) {
                    imagesCB.setChecked(false);
                } else {
                    imagesCB.setChecked(true);

                }

                break;

            case R.id.dance_ent:
                if (danceCB.isChecked()) {
                    danceCB.setChecked(false);
                } else {
                    danceCB.setChecked(true);

                }

                break;
            case R.id.music_ent:
                if (musicCB.isChecked()) {
                    musicCB.setChecked(false);
                } else {
                    musicCB.setChecked(true);

                }
//                }
                break;
        }
    }

    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }

    public void getUserInterests(String userid) {

        UseridModel object = new UseridModel();
        object.setUserid(userid);
        Log.e("USERID VERIFY", " " + object.getUserid());
//
        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<MyInterestRespModel> call = apiInterface.getUserInterests(object);
        call.enqueue(new Callback<MyInterestRespModel>() {
            @Override
            public void onResponse(Call<MyInterestRespModel> call, retrofit2.Response<MyInterestRespModel> response) {

                MyInterestRespModel myResp;
                if (response.isSuccessful()) {
                    myResp = response.body();
                    if (myResp != null) {

                        if (myResp.getCode().equalsIgnoreCase("200")) {
                            if (myResp.getSuccess() == null) {
                                String val = myResp.getEvents().get(0).get("intrest");
                                Log.e("MyyInterests Values", val + "");
                                setSelectedValues(val);
                            }
                        }
                    }
                    hideProgressBar();
                } else {
                    Log.e("Entertainment Fail", response.message() + "");
                    hideProgressBar();
                    Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MyInterestRespModel> call, Throwable t) {
                Log.e("OnFailure ", t.getMessage() + "");
                hideProgressBar();
            }
        });

    }

    public void setSelectedValues(String values) {

        String intrests[] = values.split(",");
        for (int i = 0; i < intrests.length; i++) {
            Log.e("Interests For", intrests[i] + "");

            if (intrests[i].equalsIgnoreCase("dance")) {
                Log.e("Interests", intrests[i] + "");
                danceCB.setChecked(true);

            }
            if (intrests[i].equalsIgnoreCase("shortMovies")) {
                Log.e("Interests", intrests[i] + "");

                shortMoviesCB.setChecked(true);

            }
            if (intrests[i].equalsIgnoreCase("music")) {
                musicCB.setChecked(true);
                Log.e("Interests", intrests[i] + "");

            }
            if (intrests[i].equalsIgnoreCase("images")) {
                imagesCB.setChecked(true);
                Log.e("Interests", intrests[i] + "");

            }
            if (intrests[i].equalsIgnoreCase("dubsmash")) {
                dubsmashCB.setChecked(true);
                Log.e("Interests", intrests[i] + "");

            }
            if (intrests[i].equalsIgnoreCase("standupComedy")) {
                standupComedyCB.setChecked(true);
                Log.e("Interests", intrests[i] + "");

            }
            hideProgressBar();
        }
//        Log.e("Values", " " + values);

    }

    String interests = "";

    public void submitInterestsApiCall() {

        showProgressBar();
        for (String val : selectedValues) {
            interests = val + "," + interests;
        }

        Log.e("Values Selected", " " + interests);
        InterestsBody body = new InterestsBody();
//        body.setUserId("1002");
        body.setUserId(String.valueOf(new SessionManager(getActivity()).getUserDetails().get("userid")));
        body.setIntrest(interests);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<InterestResponse> call = apiInterface.updateInterestFromMyInterest(body);
        call.enqueue(new Callback<InterestResponse>() {
            @Override
            public void onResponse(Call<InterestResponse> call, retrofit2.Response<InterestResponse> response) {

                InterestResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        Log.e("Values Submitted", " " + response.body().toString());

                        if (responseBody.getCode().equalsIgnoreCase("200")) {
                            Log.e("Values Submitted", " " + response.body().getCode());
                            Log.e("Values Submitted", " " + interests);
                            hideProgressBar();
                            OSTOApplication.getInstance().getRequestQueue().getCache().remove("http://osto.herokuapp.com:80/home");
                            switchFragment(new HomeFragment());
                        }
                    }

                } else {
                    Log.e("Entertainment Fail", response.message() + "");
                    hideProgressBar();
                    Toast.makeText(getActivity(), "Please try again", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<InterestResponse> call, Throwable t) {
                Log.e("OnFailure ", t.getMessage() + "");
//                mProgressBar.setVisibility(View.GONE);
                getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            }
        });

    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)

            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
}