package com.osto.osto4u.fragments;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.osto.osto4u.adapters.ClubsRVAdapter;
import com.osto.osto4u.models.ClubsModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;

public class SearchClubsFragment extends Fragment {

    RecyclerView mRecyclerView;
    List<ClubsModel> searchClubsItems;
    List<Integer> clubMembersList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_search_clubs, container, false);
        clubMembersList = new ArrayList<>();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.search_clubs_rv);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        searchClubsItems = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            clubMembersList.add(R.drawable.prof_pic_placeholder);
        }
        ClubsModel model;

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);

        model.setOwnerName("Owner 1");
        model.setClubName("Club 1");
        model.setMembersCount(255);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);
        searchClubsItems.add(model);

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 2");
        model.setClubName("Club 2");
        model.setMembersCount(251);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);

        searchClubsItems.add(model);

        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 3");
        model.setClubName("Club 3");
        model.setMembersCount(205);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);

        searchClubsItems.add(model);


        model = new ClubsModel();
        model.setSearchByClubDesc(R.string.dummy_description);
        model.setOwnerName("Owner 4");
        model.setClubName("Club 4");
        model.setMembersCount(155);
        model.setClubProfilePic(R.drawable.prof_pic_placeholder);
        model.setClubDescPic(R.drawable.club_placeholder);
        model.setMembersProfPics(clubMembersList);
        searchClubsItems.add(model);

        mRecyclerView.setAdapter(new ClubsRVAdapter(getActivity(), searchClubsItems,""));


        return view;
    }
}