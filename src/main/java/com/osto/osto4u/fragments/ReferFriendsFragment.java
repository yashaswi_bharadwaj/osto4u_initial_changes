package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.UseridModel;
import com.osto.osto4u.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReferFriendsFragment extends Fragment {

    RelativeLayout mToolBarLayout;
    TextView toolBarTitle;
    Button referFriends;
    TextView referCode;
    String buttonPressAction = "";
    public ProgressBar mProgressBar;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_refer_friend, container, false);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar_referal);

        mToolBarLayout = (RelativeLayout) view.findViewById(R.id.layout_toolbar_refer);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);
        toolBarTitle.setText("Refer");

        referFriends = (Button) view.findViewById(R.id.refer_friends_btn);
        referFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(buttonPressAction.contentEquals("referFriends")) {
                    shareOptionsInvoke();
                }
                else
                    {
                        showProgressBar();
                        apiCallToGenerateCode();
                    }
            }
        });
        referCode = (TextView)view.findViewById(R.id.referal_code);

        showProgressBar();
        apiCallToGetResponse();

        return view;
    }

    private void apiCallToGenerateCode() {

        UseridModel useridModel = new UseridModel();
        useridModel.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GetReferalResponse> call = apiInterface.generateReferral(useridModel);
        call.enqueue(new Callback<GetReferalResponse>() {
            @Override
            public void onResponse(Call<GetReferalResponse> call, Response<GetReferalResponse> response) {

                GetReferalResponse responseBody;

                if (response.isSuccessful()) {
                    responseBody = response.body();
                    if (responseBody != null) {
                        hideProgressBar();
                        if(responseBody.getCode().contentEquals("201"))
                        {
                            Toast.makeText(getActivity(),responseBody.getRefferalcode(),Toast.LENGTH_SHORT).show();
                            referCode.setText(responseBody.getResults());
                            referFriends.setText("Generate code");
                            buttonPressAction = "generateCode";
                        }
                        else
                        {
                            referCode.setText(responseBody.getRefferalcode());
                            referFriends.setText("Refer to your Friends");
                            buttonPressAction = "referFriends";

                        }
                    } else {
                        Log.e("Birthdays Null", response.message() + "");
                    }
                } else {
                    hideProgressBar();
                    Log.e("Birthdays api failed", response.message() + "");
                }
            }

            @Override
            public void onFailure(Call<GetReferalResponse> call, Throwable t) {
             hideProgressBar();
                Log.e("OnFailure Bday ", t.getMessage() + "");
            }
        });

    }

    private void apiCallToGetResponse() {



        UseridModel useridModel = new UseridModel();
        useridModel.setUserid(new SessionManager(getActivity()).getUserDetails().get("userid").toString());

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GetReferalResponse> call = apiInterface.getReferral(useridModel);
        call.enqueue(new Callback<GetReferalResponse>() {
            @Override
            public void onResponse(Call<GetReferalResponse> call, Response<GetReferalResponse> response) {

                GetReferalResponse responseBody;

                if (response.isSuccessful()) {
                    hideProgressBar();
                    responseBody = response.body();
                    if (responseBody != null) {
                        Log.e("ReferalCode",responseBody.getResults()+" ");
                        if(responseBody.getCode().contentEquals("201"))
                        {

                            referCode.setText(responseBody.getRefferalcode());
                            referFriends.setText("Generate code");
                            buttonPressAction = "generateCode";
                        }
                        else
                        {
                            referCode.setText(responseBody.getResults());
                            referFriends.setText("Refer to your Friends");
                            buttonPressAction = "referFriends";

                        }
                    } else {
                        Log.e("Birthdays Null", response.message() + "");
                    }
                } else {
                    Log.e("Birthdays api failed", response.message() + "");
                    hideProgressBar();

                }
            }

            @Override
            public void onFailure(Call<GetReferalResponse> call, Throwable t) {
                hideProgressBar();

                Log.e("OnFailure Bday ", t.getMessage() + "");
            }
        });

    }

    public void shareOptionsInvoke() {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "Hey check out my app at: https://play.google.com/store/apps/details?id=com.osto.osto4u");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }

    public void hideProgressBar() {
        mProgressBar.setVisibility(View.GONE);
        if (getActivity() != null)
            getActivity().getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }

    public void showProgressBar() {
        mProgressBar.setVisibility(View.VISIBLE);
        if (getActivity() != null)
            getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);

    }
}