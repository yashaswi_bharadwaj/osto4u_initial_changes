package com.osto.osto4u.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.osto.osto4u.activities.CreateEventsActivity;
import com.osto.osto4u.adapters.EventsRVAdapter;
import com.osto.osto4u.models.EventsModel;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;

public class EventsParticipatingFragment extends Fragment {

    RecyclerView mRecyclerView;
    EventsModel eventsModel;
    List<EventsModel> mEventsList;


    RelativeLayout mToolBarLayout;
    TextView toolBarTitle, createEvents, eventsSegment, participatingSegment, invitationSegment;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        Crashlytics.setString("Crashed Screen", this.getFragmentManager().getClass().getName());

        View view = inflater.inflate(R.layout.fragment_events, container, false);
        mEventsList = new ArrayList<>();
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_events);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));


        mToolBarLayout = (RelativeLayout)view. findViewById(R.id.toolbar_events);
        toolBarTitle = (TextView) mToolBarLayout.findViewById(R.id.toolbar_title);

        toolBarTitle.setText("Events");

        createEvents = (TextView) mToolBarLayout.findViewById(R.id.toolbar_menu);
        createEvents.setVisibility(View.VISIBLE);

        eventsSegment = (TextView) mToolBarLayout.findViewById(R.id.events_segment);
        participatingSegment = (TextView) mToolBarLayout.findViewById(R.id.participating_segment);
        invitationSegment = (TextView) mToolBarLayout.findViewById(R.id.invitation_segment);

        eventsSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_left));
        eventsSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        invitationSegment.setTextColor(ContextCompat.getColor(getActivity(), R.color.colorPrimaryDark));
        invitationSegment.setBackground(ContextCompat.getDrawable(getActivity(), R.drawable.toolbar_bg_white_center));
        participatingSegment.setTextColor(ContextCompat.getColor(getActivity(), android.R.color.white));
        participatingSegment.setBackground(ContextCompat.getDrawable(getActivity(), android.R.color.transparent));


        createEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(),CreateEventsActivity.class));
            }
        });


        eventsSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEvents.setVisibility(View.VISIBLE);
                  switchFragment(new EventsFragment());


            }
        });
        invitationSegment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createEvents.setVisibility(View.VISIBLE);
               switchFragment(new EventsInvitationFragment());

            }
        });


        eventsModel = new EventsModel();
        eventsModel.setEventsName("Event Name1");
        eventsModel.setEventsTiming("10:00 am, 15 jun 2018");
        eventsModel.setEventsDescription(R.string.dummy_description);
        eventsModel.setEventsCoverPic(R.drawable.home_placeholder);
        mEventsList.add(eventsModel);

        eventsModel = new EventsModel();
        eventsModel.setEventsName("Event Name2");
        eventsModel.setEventsTiming("11:00 am, 22 jun 2018");
        eventsModel.setEventsDescription(R.string.dummy_description);
        eventsModel.setEventsCoverPic(R.drawable.home_placeholder);
        mEventsList.add(eventsModel);

        eventsModel = new EventsModel();
        eventsModel.setEventsName("Event Name3");
        eventsModel.setEventsTiming("07:00 pm, 25 jun 2018");
        eventsModel.setEventsDescription(R.string.dummy_description);
        eventsModel.setEventsCoverPic(R.drawable.home_placeholder);
        mEventsList.add(eventsModel);

        eventsModel = new EventsModel();
        eventsModel.setEventsName("Event Name4");
        eventsModel.setEventsTiming("02:00 pm, 25 jun 2018");
        eventsModel.setEventsDescription(R.string.dummy_description);
        eventsModel.setEventsCoverPic(R.drawable.home_placeholder);
        mEventsList.add(eventsModel);

        mRecyclerView.setAdapter(new EventsRVAdapter(getActivity(),mEventsList,"participating"));

        return view;
    }

    public void switchFragment(Fragment fragment) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.container_body, fragment);
        fragmentTransaction.commit();
    }
}
