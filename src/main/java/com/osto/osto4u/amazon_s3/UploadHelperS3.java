package com.osto.osto4u.amazon_s3;


import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.osto.osto4u.helpers.ApiClient;
import com.osto.osto4u.helpers.SessionManager;
import com.osto.osto4u.interfaces.ApiInterface;
import com.osto.osto4u.models.CreatePostBody;
import com.osto.osto4u.models.GeneralRespModel;
import com.osto.osto4u.services.ConvertService;

import java.io.File;
import java.net.URL;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UploadHelperS3 {
    AmazonS3Client s3;
    BasicAWSCredentials credentials;
    TransferUtility transferUtility;
    TransferObserver observer;
    ProgressBar mProgressbar;

    File mFile;

    private final String KEY_S3 = "AKIAIO2TGX6554S6XZBQ";
    private final String SECRET_S3 = "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";

    public void initialize() {
        credentials = new BasicAWSCredentials(KEY_S3, SECRET_S3);
    }

//    public void uploadFile(final Context mContext, String type, final File mFile, final String location,
//                           final String descripton, ProgressBar mProgressBar) {
//        this.mProgressbar = mProgressBar;
//        initialize();
//
//        if (type.contains("standup")) {
//            type = "StandupComedy";
//        }
//        if (type.contains("short")) {
//            type = "ShortMovies";
//        }
//        if (type.contains("music")) {
//            type = "Music";
//        }
//        if (type.equalsIgnoreCase("image")) {
//            type = "Images";
//        }
//
//        if (type.equalsIgnoreCase("dubsmash")) {
//            type = "Dubsmash";
//        }
//
//        if (type.contains("dance")) {
//            type = "Dance";
//        }
//
//
//        final AmazonS3 s3client = new AmazonS3Client(credentials);
//
//        s3client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
////        File mFile = new File( path);
//
//        transferUtility = new TransferUtility(s3client, mContext);
//
//        final String foldername = type.toLowerCase();
//        observer = transferUtility.upload(
//                "devmainbucket01/" + foldername,
//                mFile.getName(), mFile, CannedAccessControlList.PublicRead);
//
//        final String finalType = type;
//        observer.setTransferListener(new TransferListener() {
//            @Override
//            public void onStateChanged(int id, TransferState state) {
//                if (TransferState.COMPLETED == state) {
//                    Log.e("Upload ", "State Changed " + id + "  " + observer.getAbsoluteFilePath());
//                    createPostAPICall(mContext, finalType, location, mFile.getName(), descripton,
//                            getS3Url(s3client, mFile.getName(), foldername), mProgressbar);
//                }
//            }
//
//            @Override
//            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
//                Log.e("Upload ", "Progress Changed " + id);
//                Log.e("Upload ", "Progress Changed " + bytesTotal);
//                Log.e("Upload ", "Progress Changed " + bytesCurrent);
//
//            }
//
//            @Override
//            public void onError(int id, Exception ex) {
//                Log.e("Upload ", "Error " + ex.getMessage());
//
//            }
//        });
//
//    }


    public void uploadFile(final Context mContext, String filePath, String type, final String descripton,
                           final String location, final String youtube) {
        initialize();
        mFile = new File(filePath);

        if (type.contains("standup")) {
            type = "StandupComedy";
        }
        if (type.contains("short")) {
            type = "ShortMovies";
        }
        if (type.equalsIgnoreCase("music")) {
            type = "Music";
        }
        if (type.equalsIgnoreCase("images")) {
            type = "Images";
        }

        if (type.equalsIgnoreCase("dubsmash")) {
            type = "Dubsmash";
        }

        if (type.equalsIgnoreCase("dance")) {
            type = "Dance";
        }


        final AmazonS3 s3client = new AmazonS3Client(credentials);

        s3client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
//        File mFile = new File( path);

        transferUtility = new TransferUtility(s3client, mContext);

        final String foldername = type.toLowerCase();
        observer = transferUtility.upload(
                "devmainbucket01/" + foldername,
                mFile.getName(), mFile, CannedAccessControlList.PublicRead);

        final String finalType = type;
        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {
                if (TransferState.COMPLETED == state) {
                    Log.e("Upload ", "State Changed " + id + "  " + observer.getAbsoluteFilePath());

                    if (youtube.contains("youtube")) {
                        createPostAPICall(mContext, finalType, location, mFile.getName(), descripton,
                                youtube, mProgressbar);
                    } else {
                        createPostAPICall(mContext, finalType, location, mFile.getName(), descripton,
                                getS3Url(s3client, mFile.getName(), foldername), mProgressbar);
                    }
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                Log.e("Upload ", "Progress Changed " + id);
                Log.e("Upload ", "Progress Changed " + bytesTotal);
                Log.e("Upload ", "Progress Changed " + bytesCurrent);

            }

            @Override
            public void onError(int id, Exception ex) {
                Log.e("Upload ", "Error " + ex.getMessage());

            }
        });

    }


    public void createPostAPICall(final Context mContext, String type, String location,
                                  String fileName, String description, String youtubeLink, final ProgressBar progressbar) {

        String userId = new SessionManager(mContext).getUserDetails().get("userid").toString();

        CreatePostBody body = new CreatePostBody();
        body.setCategory(type);
        body.setDescription(description);
        body.setFilename(fileName);
        body.setUserid(userId);
        body.setLocation(location);
        body.setPath(youtubeLink);

        ApiInterface apiInterface = new ApiClient().getClient().create(ApiInterface.class);
        Call<GeneralRespModel> call = apiInterface.createPost(body);
        call.enqueue(new Callback<GeneralRespModel>() {
            @Override
            public void onResponse(Call<GeneralRespModel> call, Response<GeneralRespModel> response) {
                GeneralRespModel respModel = response.body();
                if (respModel != null) {
                    if (respModel.getCode() == 200) {
                        Log.e("Uploaded ", "Successfully ");
                        mContext.stopService(new Intent(mContext, ConvertService.class));
                        Toast.makeText(mContext, "Your post is Updated Successfully..!", Toast.LENGTH_SHORT).show();

                    }
                }


            }

            @Override
            public void onFailure(Call<GeneralRespModel> call, Throwable t) {
                Log.e("Uploaded ", t.getMessage() + "");
            }
        });
    }


    public static String getS3Url(AmazonS3 s3client, String objectKey, String folderName) {

        URL url = new AmazonS3Client().getUrl("devmainbucket01", folderName + "/" + objectKey);
        Log.e("URL", "" + String.valueOf(url));

        return url.toString();
    }

}


//
//
//    public void pauseSingleFile(int idOfTransferToBePaused) {
////To Pause a Single Transfer
//        transferUtility.pause(idOfTransferToBePaused);
//    }
//
//    public void pauseAllFile() {
////To Pause All Uploads
//        transferUtility.pauseAllWithType(TransferType.UPLOAD);
//    }
//
//    public void resumeSingleFile(int idOfTransferToBeResumed) {
////To Resuming a Single Transfer
//        transferUtility.resume(idOfTransferToBeResumed);
//
//    }
//
//    public void cancelSingleFileTransfer(int idToBeCancelled) {
//
//        //To Cancel a Single Transfer
//        transferUtility.cancel(idToBeCancelled);
//    }
//
//    public void cancelAllFileTransfer() {
////To Cancel All Transfer (Uploads)
//        transferUtility.cancelAllWithType(TransferType.UPLOAD);
//    }
