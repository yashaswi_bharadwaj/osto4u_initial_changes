package com.osto.osto4u.helpers;

/**
 * Created by VEDAVYASA GUTTAL on 4/18/2018.
 */

public class YoutubeVideo {

    String url;

    public YoutubeVideo() {
    }

    public YoutubeVideo(String url) {

        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
