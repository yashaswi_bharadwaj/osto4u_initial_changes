package com.osto.osto4u.helpers;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.annotation.NonNull;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.osto.osto4u.activities.LoginActivity;

import java.util.HashMap;

public class SessionManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Sharedpref file name
    private static final String PREF_NAME = "UserLogin";

    // All Shared Preferences Keys
    private static final String IS_LOGIN = "IsLoggedIn";


    // All Shared Preferences Keys
    private static final String LOGIN_FROM = "loginFrom";


    // User name (make variable public to access from outside)
    public static final String USERID = "userid";

    // User name (make variable public to access from outside)
    public static final String KEY_NAME = "name";

    // Email address (make variable public to access from outside)
    public static final String KEY_EMAIL = "email";

    // Email address (make variable public to access from outside)
    public static final String PHONE = "phone";

    private String INTEREST_SELECTED = "selectedInterests";

    // Constructor
    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    /**
     * Create login session
     */
    public void createLoginSession(String email,String userName, Integer userid,String from) {
        // Storing login value as TRUE
        editor.putBoolean(IS_LOGIN, true);

        // Storing ID in pref
        editor.putInt(USERID, userid);

        // Storing UserName in pref
        editor.putString(KEY_NAME, userName);

        // Storing email in pref
        editor.putString(KEY_EMAIL, email);


        // Storing loginFrom in pref
        editor.putString(LOGIN_FROM, from);
        // Storing phone in pref
//        editor.putString(PHONE, phone);

        // commit changes
        editor.commit();
    }

    /**
     * Check login method wil check user login status
     * If false it will redirect user to login page
     * Else won't do anything
     */
    public void checkLogin() {
        // Check login status
        if (!this.isLoggedIn()) {
            // user is not logged in redirect him to Login Activity
            Intent i = new Intent(_context, LoginActivity.class);
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            // Staring Login Activity
            _context.startActivity(i);
        }

    }


    /**
     * Get stored session data
     */
    public HashMap<String, Object> getUserDetails() {
        HashMap<String, Object> user = new HashMap<String, Object>();
        // user name
        user.put(USERID, pref.getInt(USERID, 0));

        // user email id
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));


        user.put(LOGIN_FROM, pref.getString(LOGIN_FROM, null));
        // return user
        return user;
    }


    public boolean isInterestsSelected() {
        return pref.getBoolean(INTEREST_SELECTED, false);
    }

    public void setInterestSelected(boolean selected) {
        editor.putBoolean(INTEREST_SELECTED, selected);
        editor.commit();
    }


    /**
     * Clear session details
     */
    public void logoutUser() {
        // Clearing all data from Shared Preferences
        editor.clear();
        editor.commit();

        OSTOApplication.getInstance().getRequestQueue().getCache().remove("http://osto.herokuapp.com:80/home");

        try{
            if (OSTOApplication.getInstance().getmApiClient().isConnected()) {
                Auth.GoogleSignInApi.signOut(OSTOApplication.getInstance().mApiClient).setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(@NonNull Status status) {
                        Log.e("Logged Out", "From GMAIL");

                    }
                });
            } else if (AccessToken.getCurrentAccessToken() != null) {
                LoginManager.getInstance().logOut();
                Log.e("Logged Out", "From FB");
            }
        }catch (Exception e)
        {
            Log.e("error", " == >" +e.getMessage() );

        }

        Log.e("Logged Out", "Osto Account");

        // After logout redirect user to Loing Activity
        Intent i = new Intent(_context, LoginActivity.class);
        // Closing all the Activities
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        // Staring Login Activity
        _context.startActivity(i);
    }

    /**
     * Quick check for login
     **/
    // Get Login State
    public boolean isLoggedIn() {
        return pref.getBoolean(IS_LOGIN, false);
    }
}