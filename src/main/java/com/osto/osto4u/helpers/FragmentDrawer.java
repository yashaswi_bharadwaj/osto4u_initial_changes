package com.osto.osto4u.helpers;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.osto.osto4u.adapters.NavDrawerAdapter;
import com.osto.osto4u.fragments.MyProfileFragment;
import com.osto.osto4u.R;

import java.util.ArrayList;
import java.util.List;


public class FragmentDrawer extends Fragment {

    private static String TAG = FragmentDrawer.class.getSimpleName();

    private RecyclerView recyclerView;
    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private NavDrawerAdapter adapter;
    private View containerView;
    private static String[] titles = null;
    private static Integer[] icons = null;
    private FragmentDrawerListener drawerListener;
    private LinearLayout myProfileLayout;

    public FragmentDrawer() {

    }

    public void setDrawerListener(FragmentDrawerListener listener) {
        this.drawerListener = listener;
    }

    public static List<NavDrawerItem> getData() {
        List<NavDrawerItem> data = new ArrayList<>();

        // preparing navigation drawer items
        for (int i = 0; i < titles.length; i++) {
            NavDrawerItem navItem = new NavDrawerItem();
            navItem.setTitle(titles[i]);
            navItem.setIcons(icons[i]);
            data.add(navItem);
        }
        return data;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // drawer labels
        titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
//        icons = new Integer[]{R.drawable.home_icon, R.drawable.saved_items_icon, R.mipmap.ic_launcher,
//                R.drawable.clubs_icon, R.drawable.events_icon,
//                R.drawable.birthdays_icon, R.drawable.balance_icon, R.drawable.change_password_icon,
//                R.mipmap.ic_launcher, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
//                R.drawable.logout_icon};
        icons = new Integer[]{R.drawable.home_icon, R.drawable.saved_items_icon, R.drawable.push_pin,
                R.drawable.birthdays_icon, R.drawable.balance_icon, R.drawable.change_password_icon,
                R.drawable.meeting, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
                R.drawable.logout_icon};

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflating view layout
        View layout = inflater.inflate(R.layout.navigation_drawer, container, false);
        recyclerView = (RecyclerView) layout.findViewById(R.id.drawerList);
        myProfileLayout = (LinearLayout) layout.findViewById(R.id.nav_header_container);


        // Write Code On Selecting My Profile Android
        myProfileLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.container_body, new MyProfileFragment());
                fragmentTransaction.commit();
                mDrawerLayout.closeDrawer(GravityCompat.START);
//                Toast.makeText(getContext(), "Selected My Profile", Toast.LENGTH_SHORT).show();
            }
        });

        if (new SessionManager(getActivity()).getUserDetails().get("loginFrom").toString().equalsIgnoreCase("app")) {
            titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels);
//            icons = new Integer[]{R.mipmap.ic_launcher, R.drawable.saved_items_icon, R.mipmap.ic_launcher, R.drawable.clubs_icon, R.drawable.events_icon,
//                    R.drawable.birthdays_icon, R.drawable.balance_icon, R.drawable.change_password_icon,
//                    R.mipmap.ic_launcher, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
//                    R.drawable.logout_icon};

            icons = new Integer[]{R.drawable.home_icon, R.drawable.saved_items_icon, R.drawable.push_pin,
                    R.drawable.balance_icon, R.drawable.change_password_icon,
                    R.drawable.meeting, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
                    R.drawable.logout_icon};

        } else {
            titles = getActivity().getResources().getStringArray(R.array.nav_drawer_labels_no_cp);
//            icons = new Integer[]{R.mipmap.ic_launcher, R.drawable.saved_items_icon, R.mipmap.ic_launcher, R.drawable.clubs_icon, R.drawable.events_icon,
//                    R.drawable.birthdays_icon, R.drawable.balance_icon,
//                    R.mipmap.ic_launcher, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
//                    R.drawable.logout_icon};

            icons = new Integer[]{R.drawable.home_icon, R.drawable.saved_items_icon, R.drawable.push_pin,
                    R.drawable.balance_icon,
                    R.drawable.meeting, R.drawable.t_and_c_icon, R.drawable.privacy_policy_icon, R.drawable.help_and_support_icon,
                    R.drawable.logout_icon};
        }

        adapter = new NavDrawerAdapter(getActivity(), titles, icons);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                drawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        return layout;
    }


    public void setUp(int fragmentId, DrawerLayout drawerLayout, final Toolbar toolbar) {
        containerView = getActivity().findViewById(fragmentId);
        mDrawerLayout = drawerLayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                toolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });

    }

    public static interface ClickListener {
        public void onClick(View view, int position);

        public void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }


    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}
