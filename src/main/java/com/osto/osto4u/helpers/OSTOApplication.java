package com.osto.osto4u.helpers;

import android.app.Application;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.api.GoogleApiClient;
import com.osto.osto4u.R;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.ILoadCallback;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


public class OSTOApplication extends Application {


    public static final String TAG = OSTOApplication.class.getSimpleName();

    private RequestQueue mRequestQueue;
    public GoogleApiClient mApiClient;
    private static OSTOApplication mInstance;
    private String firebaseTokenId="";

    public static final String MyPREFERENCES = "MyPrefs" ;
    public static final String name = "key";
    SharedPreferences sharedpreferences;

    @Override
    public void onCreate() {
        super.onCreate();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/dejavusanscondensed.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        mInstance=this;
//        Fabric.with(this, new Crashlytics());

//        if(new SessionManager(this).getUserDetails().get("userid").toString()!=null)
//        {
//            Crashlytics.setUserIdentifier(new SessionManager(this).getUserDetails().get("userid").toString());
//        }

        SharedPreferences prefs = getSharedPreferences("fb_tokenid", MODE_PRIVATE);
        String firebaseTokenId = prefs.getString("key", "");

        setFirebaseTokenId(firebaseTokenId);

        AndroidAudioConverter.load(this, new ILoadCallback() {
            @Override
            public void onSuccess() {
                // Great!
                Log.e("MyConverter"," Great");
            }
            @Override
            public void onFailure(Exception error) {
                // FFmpeg is not supported by device
                Log.e("MP3Converter"," FFmpeg is not supported by device");
            }
        });

    }
    public String getFirebaseTokenId() {
        return firebaseTokenId;
    }

    public void setFirebaseTokenId(String firebaseTokenId) {
        this.firebaseTokenId = firebaseTokenId;
    }



    public GoogleApiClient getmApiClient() {
        return mApiClient;
    }

    public void setmApiClient(GoogleApiClient mApiClient) {
        this.mApiClient = mApiClient;
    }

    public static synchronized OSTOApplication getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
