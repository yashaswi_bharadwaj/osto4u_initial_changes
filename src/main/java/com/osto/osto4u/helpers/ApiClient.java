package com.osto.osto4u.helpers;


import com.google.gson.Gson;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    Retrofit mClient;
    public String BASE_URL = "http://osto.herokuapp.com:80";

    public Retrofit getClient()
    {

        mClient = new Retrofit.Builder().
                baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        return mClient;
    }


}
