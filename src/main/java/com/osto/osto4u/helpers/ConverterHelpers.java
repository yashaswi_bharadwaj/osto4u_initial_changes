package com.osto.osto4u.helpers;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ProgressBar;

import com.osto.osto4u.amazon_s3.UploadHelperS3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;

public class ConverterHelpers {

    File fileConverted;


    public void startProcess(Context mContext, String type, String pathToFile, String location, String description,
                             ProgressBar mProgressBar) {

        if (type.equalsIgnoreCase("music")) {
//            File file = convertAudioFile(mContext, type, new File(pathToFile), location, description);
        }
        if (type.equalsIgnoreCase("images")) {
            convertImagetoJpg(mContext, type, pathToFile, location, description);
        } else {
            //new UploadHelperS3().uploadFile(mContext, type, new File(pathToFile), location, description, mProgressBar);
        }
    }

//
//    public File convertAudioFile(final Context mContext, final String type, File inputFile, final String location, final String description) {
////        File input = new File(Environment.getExternalStorageDirectory() + "/Download/parentcolors.mov");
//
////        new UploadHelperS3().uploadFile(mContext, type, inputFile, location, description);
//
//        IConvertCallback callback = new IConvertCallback() {
//            @Override
//            public void onSuccess(File convertedFile) {
//                // So fast? Love it!
////("Conversion Done");
//                Log.e("MP3Converter", " Conversion Done ");
//                fileConverted = convertedFile;
////                mContext.stopService(new Intent(mContext,ConvertService.class));
////                new UploadHelperS3().uploadFile(mContext, type, convertedFile, location, description);
//            }
//
//            @Override
//            public void onFailure(Exception error) {
//                // Oops! Something went wrong
//                Log.e("MP3Converter", " Conversion Failed");
//            }
//        };
////        AndroidAudioConverter.with(mContext)
////                // Your current audio file
////                .setFile(inputFile)
////
////                // Your desired audio format
////                .setFormat(AudioFormat.MP3)
////
////                // An callback to know when conversion is finished
////                .setCallback(callback)
////
////                // Start conversion
////                .convert();
//        return fileConverted;
//    }

    public FileOutputStream convertImagetoJpg(Context mContext, String type, String path, String location,
                                              String description) {

        Bitmap bmp = BitmapFactory.decodeFile(path);// here you need to pass your existing file path


        File image = new File(path);
        FileOutputStream outStream = null;
        try {
            outStream = new FileOutputStream(image);
// here you need to pass the format as I have passed as PNG so you can create the file with specific format
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
    /* 100 to keep full quality of the image */
//            new UploadHelperS3().uploadFile(mContext, type, new File(path), location, description);
            outStream.flush();
            outStream.close();


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outStream;

//        Bitmap bmp = null;
//        try {
//            FileOutputStream out = new FileOutputStream(path);
//            bmp.compress(Bitmap.CompressFormat.JPEG, 100, out); //100-best quality
//            out.close();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

//    private void addLog(final String log) {
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//                Log.e("MP3Converter"," " +log);
//            }
//        });
//    }


    public void convertAudioFile(final Context mContext, final String filePath
            , final String type, final String description, final String location,final String youtube) {

        IConvertCallback callback = new IConvertCallback() {
            @Override
            public void onSuccess(File convertedFile) {

                Log.e("MP3Converter", " Conversion Done ");
                fileConverted = convertedFile;

                new UploadHelperS3().uploadFile(mContext, filePath, type, description, location,youtube);
//                mContext.stopService(new Intent(mContext, ConvertService.class));

            }

            @Override
            public void onFailure(Exception error) {
                Log.e("MP3Converter", " Conversion Failed");
            }
        };
        AndroidAudioConverter.with(mContext)
                // Your current audio file
                .setFile(new File(filePath))

                // Your desired audio format
                .setFormat(AudioFormat.MP3)

                // An callback to know when conversion is finished
                .setCallback(callback)

                // Start conversion
                .convert();

    }


    public void convertImageToJPG(Context mContext, String filePath,
                                  String type, final String description,
                                  final String location,final String youtube) {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);

        try {
            FileOutputStream out = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //100-best quality
            out.close();
            new UploadHelperS3().uploadFile(mContext, filePath, type, description, location,youtube);

        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Convert Service", "Compress Failed " + e.getMessage());
        }


    }

//    private Bitmap scaleBitmap(Bitmap bm) {
//        int width = bm.getWidth();
//        int height = bm.getHeight();
//
//        Log.v("Pictures", "Width and height are " + width + "--" + height);
//
//        if (width > height) {
//            // landscape
//            float ratio = (float) width / maxWidth;
//            width = maxWidth;
//            height = (int)(height / ratio);
//        } else if (height > width) {
//            // portrait
//            float ratio = (float) height / maxHeight;
//            height = maxHeight;
//            width = (int)(width / ratio);
//        } else {
//            // square
//            height = maxHeight;
//            width = maxWidth;
//        }
//
//        Log.v("Pictures", "after scaling Width and height are " + width + "--" + height);
//
//        bm = Bitmap.createScaledBitmap(bm, width, height, true);
//        return bm;
//    }
//

}
