package com.osto.osto4u.helpers;


public class OstoObjects {
    private static OstoObjects mInstance= null;

    private boolean interestsSelected =false;



    public static OstoObjects getmInstance() {
        return mInstance;
    }

    public static void setmInstance(OstoObjects mInstance) {
        OstoObjects.mInstance = mInstance;
    }


    public boolean isInterestsSelected() {
        return interestsSelected;
    }

    public void setInterestsSelected(boolean interestsSelected) {
        this.interestsSelected = interestsSelected;
    }

    protected OstoObjects(){}

    public static synchronized OstoObjects getInstance(){
        if(null == mInstance){
            mInstance = new OstoObjects();
        }
        return mInstance;
    }
}
