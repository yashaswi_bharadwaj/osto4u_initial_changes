package com.osto.osto4u.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.osto.osto4u.amazon_s3.UploadHelperS3;
import com.osto.osto4u.helpers.ConverterHelpers;

import java.io.File;
import java.io.FileOutputStream;

import cafe.adriel.androidaudioconverter.AndroidAudioConverter;
import cafe.adriel.androidaudioconverter.callback.IConvertCallback;
import cafe.adriel.androidaudioconverter.model.AudioFormat;


public class ConvertService extends Service {
    /**
     * indicates how to behave if the service is killed
     */
    int mStartMode;
    File fileConverted;

    String category, filePath, location, description,youtube;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Called when the service is being created.
     */
    @Override
    public void onCreate() {
//        Crashlytics.setString("Crashed Screen", this.getClass().getName());

        Log.e("Service", "Created Service");


    }

    /**
     * The service is starting, due to a call to startService()
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        /*Code to convert Files*/
        Log.e("Service", "Start Command");

        category = intent.getStringExtra("spinnerSelected");
        description = intent.getStringExtra("description");
        location = intent.getStringExtra("location");
        filePath = intent.getStringExtra("filePath");
        youtube= intent.getStringExtra("youtube");
        if (category.equalsIgnoreCase("images")) {
            new ConverterHelpers().convertImageToJPG(getApplicationContext(),filePath,category,description,location,youtube);
        }

        else if(category.equalsIgnoreCase("music"))
        {
            Log.e("Service",category+" ");
            new ConverterHelpers().convertAudioFile(getApplicationContext(),filePath,category,description,location,youtube);
        }
        else
        {
            new UploadHelperS3().uploadFile(getApplicationContext(), filePath, category, description, location,youtube);

        }

        return mStartMode;
    }

    @Override
    public boolean stopService(Intent name) {

        Log.e("Service","Stopped");
        return super.stopService(name);
    }

    public File convertAudioFile() {
//        File input = new File(Environment.getExternalStorageDirectory() + "/Download/parentcolors.mov");
//        new UploadHelperS3().uploadFile(mContext, type, inputFile,location,description,mProgressBar);

        IConvertCallback callback = new IConvertCallback() {
            @Override
            public void onSuccess(File convertedFile) {
                // So fast? Love it!
//("Conversion Done");
                Log.e("MP3Converter", " Conversion Done ");
                fileConverted = convertedFile;

//                stopService(new Intent(this,ConvertService.class));
//                new UploadHelperS3().uploadFile(getApplicationContext(), category, convertedFile, location, description, mProgressBar);
            }

            @Override
            public void onFailure(Exception error) {
                // Oops! Something went wrong
                Log.e("MP3Converter", " Conversion Failed");
            }
        };
        AndroidAudioConverter.with(getApplicationContext())
                // Your current audio file
                .setFile(new File(filePath))

                // Your desired audio format
                .setFormat(AudioFormat.MP3)

                // An callback to know when conversion is finished
                .setCallback(callback)

                // Start conversion
                .convert();
        return fileConverted;
    }

    public Bitmap convertImageToJPG() {
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(filePath, bmOptions);

        try {
            FileOutputStream out = new FileOutputStream(filePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); //100-best quality
            out.close();


        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Convert Service", "Compress Failed " + e.getMessage());
        }

        return bitmap;
    }


}
