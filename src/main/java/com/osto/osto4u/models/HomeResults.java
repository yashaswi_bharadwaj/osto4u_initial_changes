package com.osto.osto4u.models;


import java.util.HashMap;
import java.util.Map;

public class HomeResults {
        private String category,location,path,username,description,shares,eng,userrating;
        private int postid,rating;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        public HomeResults() {
        }

        public HomeResults(String category, String location, String path, String username, String description, String comment, String report, String shares, String eng, int postid, int rating,String userrating) {

            this.userrating = userrating;
            this.category = category;
            this.location = location;
            this.path = path;
            this.username = username;
            this.description = description;
//        this.comment = comment;
//            this.report = report;
            this.shares = shares;
            this.eng = eng;
            this.postid = postid;
            this.rating = rating;
        }

//    public String getComment() {
//        return comment;
//    }


    public String getUserrating() {
        return userrating;
    }

//    public String getReport() {
//            return report;
//        }

        public String getShares() {
            return shares;
        }

        public String getEng() {
            return eng;
        }

        public int getRating() {
            return rating;
        }

        public String getDescription() {
            return description;
        }

        public int getPostid() {
            return postid;
        }

        public String getCategory() {
            return category;
        }

        public String getLocation() {
            return location;
        }

        public String getPath() {
            return path;
        }

        public String getUsername() {
            return username;
        }


}
