package com.osto.osto4u.models;

import java.util.List;


public class BirthdaysResponse {

    String code;
    List<BirthdayResults> Results;

    public BirthdaysResponse() {
    }

    public BirthdaysResponse(String code, List<BirthdayResults> results) {
        this.code = code;
        Results = results;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<BirthdayResults> getResults() {
        return Results;
    }

    public void setResults(List<BirthdayResults> results) {
        Results = results;
    }

    public class BirthdayResults {
        String username, dob, dppath;

        public String getUsernamw() {
            return username;
        }

        public void setUsernamw(String username) {
            this.username = username;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getDppath() {
            return dppath;
        }

        public void setDppath(String dppath) {
            this.dppath = dppath;
        }
    }
}
