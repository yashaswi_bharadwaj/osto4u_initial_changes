package com.osto.osto4u.models;


public class ForgotPasswordBody {
    String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
