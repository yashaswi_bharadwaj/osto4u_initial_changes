package com.osto.osto4u.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeResponse {
    Integer code;
    @JsonProperty("Results")
    private List<HomeResults> results = null;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    //    ArrayList<LikeResults> Likeresults;

//    public ArrayList<LikeResults> getLikeresults() {
//        return Likeresults;
//    }


    public Map<String, Object> getAdditionalProperties() {
        return additionalProperties;
    }

    public HomeResponse(int code, ArrayList<HomeResults> results) {
        this.code = code;

        this.results = results;
    }



    public HomeResponse() {
    }

    public Integer getCode() {
        return code;
    }

    public List<HomeResults> getResults() {
        return results;
    }
}
