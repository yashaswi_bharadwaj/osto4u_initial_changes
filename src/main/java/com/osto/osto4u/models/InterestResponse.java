package com.osto.osto4u.models;


public class InterestResponse {

    private String code, success;

    public String getCode() {
        return code;
    }

    public String getSuccess() {
        return success;
    }
}
