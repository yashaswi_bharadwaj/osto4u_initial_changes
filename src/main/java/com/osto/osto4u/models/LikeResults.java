package com.osto.osto4u.models;



public class LikeResults {

    int postid,Likes;

    public LikeResults() {
    }

    public int getPostid() {
        return postid;
    }

    public int getLikes() {
        return Likes;
    }

    public LikeResults(int postid, int Likes) {
        this.postid = postid;
        this.Likes = Likes;
    }
}
