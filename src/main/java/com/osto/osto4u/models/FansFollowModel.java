package com.osto.osto4u.models;


public class FansFollowModel {

    private int profPicFans;
    private String fanName;

    public FansFollowModel(int profPicFans, String fanName) {
        this.profPicFans = profPicFans;
        this.fanName = fanName;
    }

    public int getProfPicFans() {
        return profPicFans;
    }

    public String getFanName() {
        return fanName;
    }

}
