package com.osto.osto4u.models;


public class CommentResults
{

    String comments, username, dppath;

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDppath() {
        return dppath;
    }

    public void setDppath(String dppath) {
        this.dppath = dppath;
    }
}
