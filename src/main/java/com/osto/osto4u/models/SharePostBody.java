package com.osto.osto4u.models;

public class SharePostBody {

    String userid,category,path;
int postid;
    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getPostpath() {
        return path;
    }

    public void setPostpath(String postpath) {
        this.path = postpath;
    }
}
