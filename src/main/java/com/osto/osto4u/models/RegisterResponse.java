package com.osto.osto4u.models;

public class RegisterResponse {
    private String code,success;
    private int userid;

    public String getCode() {
        return code;
    }

    public String getSuccess() {
        return success;
    }

    public Integer getUserid() {
        return userid;
    }
}
