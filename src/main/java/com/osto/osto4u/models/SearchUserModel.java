package com.osto.osto4u.models;


public class SearchUserModel {
    private String userName;
    private int profilePic;

    public SearchUserModel(String userName, int profilePic) {
        this.userName = userName;
        this.profilePic = profilePic;
    }

    public SearchUserModel() {
        this.userName = userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public int getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(int profilePic) {
        this.profilePic = profilePic;
    }
}
