package com.osto.osto4u.models;


public class GeneralRespModel {
    private int code,userid;
    private String success,sucess,Results;

    public String getResults() {
        return Results;
    }

    public void setResults(String results) {
        Results = results;
    }

    public void setSucess(String sucess) {
        this.sucess = sucess;
    }

    public String getSucess() {
        return sucess;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getResponseCode() {
        return code;
    }

    public void setResponseCode(int code) {
        this.code = code;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
