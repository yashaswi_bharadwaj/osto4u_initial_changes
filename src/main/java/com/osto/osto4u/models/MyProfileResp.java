package com.osto.osto4u.models;


import java.util.List;

public class MyProfileResp {
    String code,sucess;
    List<MyProfResults> Result;
    List<MyProfilePostResults> postResult;


    public MyProfileResp() {
    }

    public MyProfileResp(String code, String sucess, List<MyProfResults> results) {
        this.code = code;
        this.sucess = sucess;
        Result = results;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setSucess(String sucess) {
        this.sucess = sucess;
    }

    public void setResults(List<MyProfResults> results) {
        Result = results;
    }

    public List<MyProfilePostResults> getPostResults() {
        return postResult;
    }

    public void setPostResult(List<MyProfilePostResults> postResult) {
        this.postResult = postResult;
    }

    public String getCode() {
        return code;
    }

    public String getSucess() {
        return sucess;
    }

    public List<MyProfResults> getResults() {
        return Result;
    }
}
