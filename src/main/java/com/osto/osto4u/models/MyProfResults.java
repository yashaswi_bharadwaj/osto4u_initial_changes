package com.osto.osto4u.models;


public class MyProfResults {
    String username,dob,dppath,email,phone,description,Fan,following;

    public MyProfResults() {

    }

    public MyProfResults(String username, String dob, String dppath, String email, String phone, String description, String fan, String following) {
        this.username = username;
        this.dob = dob;
        this.dppath = dppath;
        this.email = email;
        this.phone = phone;
        this.description = description;
        Fan = fan;
        this.following = following;
    }

    public String getUsername() {
        return username;
    }

    public String getDob() {
        return dob;
    }

    public String getDppath() {
        return dppath;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getDescription() {
        return description;
    }

    public String getFan() {
        return Fan;
    }

    public String getFollowing() {
        return following;
    }
}
