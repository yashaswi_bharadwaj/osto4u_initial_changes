package com.osto.osto4u.models;

public class SearchClubResultsSection {

    private String clubname,clubdppath;
    private int clubid;

    public String getClubname() {
        return clubname;
    }

    public String  getClubdppath() {
        return clubdppath;
    }

    public int getClubid() {
        return clubid;
    }
}
