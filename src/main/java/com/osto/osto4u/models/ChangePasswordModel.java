package com.osto.osto4u.models;


public class ChangePasswordModel {

    String userid,
            password,
            oldpassword;

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setOldpassword(String oldpassword) {
        this.oldpassword = oldpassword;
    }

    public String getUserid() {
        return userid;
    }

    public String getPassword() {
        return password;
    }

    public String getOldpassword() {
        return oldpassword;
    }
}
