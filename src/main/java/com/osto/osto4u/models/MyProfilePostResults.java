package com.osto.osto4u.models;

/**
 * Created by VEDAVYASA GUTTAL on 4/21/2018.
 */

public class MyProfilePostResults {
    String path,category;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
