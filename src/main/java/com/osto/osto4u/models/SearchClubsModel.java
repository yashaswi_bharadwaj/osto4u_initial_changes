package com.osto.osto4u.models;

public class SearchClubsModel {
    private int clubid;
    private String clubName, clubdppath;

    public SearchClubsModel( String clubName, String clubdppath) {
        this.clubName = clubName;
        this.clubdppath = clubdppath;
    }

    public int getClubid() {
        return clubid;
    }

    public String getClubName() {
        return clubName;
    }

    public String getClubdppath() {
        return clubdppath;
    }
}
