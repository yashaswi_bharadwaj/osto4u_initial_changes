package com.osto.osto4u.models;


import java.util.List;
import java.util.Map;

public class MyInterestRespModel {

    String code;
    List<Map<String,String>> Events;
    String success;

    public MyInterestRespModel(String code, List<Map<String, String>> events, String success) {
        this.code = code;
        Events = events;
        this.success = success;
    }

    public MyInterestRespModel() {
    }

    public String getSuccess() {
        return success;
    }

    public String getCode() {
        return code;
    }

    public List<Map<String,String>> getEvents() {
        return Events;
    }
}
