package com.osto.osto4u.models;


public class QuesAnsModel {
    int question,answer;

    String commenterName,comments;

    public String getCommenterName() {
        return commenterName;
    }

    public void setCommenterName(String commenterName) {
        this.commenterName = commenterName;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public QuesAnsModel(String commenterName, String comments) {
        this.commenterName = commenterName;
        this.comments = comments;
    }

    public QuesAnsModel(int question, int answer) {
        this.question = question;
        this.answer = answer;
    }

    public int getQuestion() {
        return question;
    }

    public void setQuestion(int question) {
        this.question = question;
    }

    public int getAnswer() {
        return answer;
    }

    public void setAnswer(int answer) {
        this.answer = answer;
    }
}
