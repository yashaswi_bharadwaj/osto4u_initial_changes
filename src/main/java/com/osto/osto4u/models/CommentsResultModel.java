package com.osto.osto4u.models;

import java.util.List;

public class CommentsResultModel {


    int code;
    List<CommentResults> Results;

    public int getCode() {
        return code;
    }

    public List<CommentResults> getResults() {
        return Results;
    }

}