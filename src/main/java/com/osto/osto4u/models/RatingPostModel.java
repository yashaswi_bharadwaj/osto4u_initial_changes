package com.osto.osto4u.models;

/**
 * Created by VEDAVYASA GUTTAL on 4/17/2018.
 */

public class RatingPostModel {

    String userid,likes;
    int postid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Integer getPostid() {
        return postid;
    }

    public void setPostid(int postid) {
        this.postid = postid;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }
}
