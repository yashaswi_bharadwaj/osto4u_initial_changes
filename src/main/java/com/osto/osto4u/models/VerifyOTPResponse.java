package com.osto.osto4u.models;


public class VerifyOTPResponse {
    private String  success;
    private int code;

    public int getCode() {
        return code;
    }

    public String getSuccess() {
        return success;
    }

}
