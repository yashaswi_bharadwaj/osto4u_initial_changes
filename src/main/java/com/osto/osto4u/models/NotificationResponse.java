package com.osto.osto4u.models;


import java.util.List;

public class NotificationResponse {

    int code;
    List<NotificationResults> notification;

    public int getCode() {
        return code;
    }

    public List<NotificationResults> getNotification() {
        return notification;
    }
}
