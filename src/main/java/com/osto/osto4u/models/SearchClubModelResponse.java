package com.osto.osto4u.models;

import java.util.List;

public class SearchClubModelResponse {

    String code;
    List<SearchClubResultsSection> Results;


    public SearchClubModelResponse(String code, List<SearchClubResultsSection> results) {
        this.code = code;
        Results = results;
    }

    public SearchClubModelResponse() {
    }

    public String getCode() {
        return code;
    }

    public List<SearchClubResultsSection> getResults() {
        return Results;
    }
}
