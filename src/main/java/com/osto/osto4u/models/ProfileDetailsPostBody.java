package com.osto.osto4u.models;

/**
 * Created by VEDAVYASA GUTTAL on 4/22/2018.
 */

public class ProfileDetailsPostBody {

    String userid,dob,dppath,description,gender;

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public void setDppath(String dppath) {
        this.dppath = dppath;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
