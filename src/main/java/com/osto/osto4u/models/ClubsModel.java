package com.osto.osto4u.models;


import java.util.List;

public class ClubsModel {
    private int clubProfilePic, clubDescPic, membersCount,searchByClubDesc;
    private String clubName, ownerName;
    private List<Integer> membersProfPics;

    public List<Integer> getMembersProfPics() {
        return membersProfPics;
    }

    public void setMembersProfPics(List<Integer> membersProfPics) {
        this.membersProfPics = membersProfPics;
    }

    public int getClubProfilePic() {
        return clubProfilePic;
    }

    public void setClubProfilePic(int clubProfilePic) {
        this.clubProfilePic = clubProfilePic;
    }

    public int getClubDescPic() {
        return clubDescPic;
    }

    public void setClubDescPic(int clubDescPic) {
        this.clubDescPic = clubDescPic;
    }

    public int getMembersCount() {
        return membersCount;
    }

    public void setMembersCount(int membersCount) {
        this.membersCount = membersCount;
    }

    public String getClubName() {
        return clubName;
    }

    public void setClubName(String clubName) {
        this.clubName = clubName;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public Integer getSearchByClubDesc() {
        return searchByClubDesc;
    }

    public void setSearchByClubDesc(int searchByClubDesc) {
        this.searchByClubDesc = searchByClubDesc;
    }
}
