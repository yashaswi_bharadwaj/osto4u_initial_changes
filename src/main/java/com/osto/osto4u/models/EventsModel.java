package com.osto.osto4u.models;



public class EventsModel {
    int eventsCoverPic,eventsDescription;
    String eventsName,eventsTiming;

    public int getEventsCoverPic() {
        return eventsCoverPic;
    }

    public void setEventsCoverPic(int eventsCoverPic) {
        this.eventsCoverPic = eventsCoverPic;
    }

    public String getEventsName() {
        return eventsName;
    }

    public void setEventsName(String eventsName) {
        this.eventsName = eventsName;
    }

    public String getEventsTiming() {
        return eventsTiming;
    }

    public void setEventsTiming(String eventsTiming) {
        this.eventsTiming = eventsTiming;
    }

    public Integer getEventsDescription() {
        return eventsDescription;
    }

    public void setEventsDescription(int eventsDescription) {
        this.eventsDescription = eventsDescription;
    }
}
