package com.osto.osto4u.models;

public class NotificationResults {

    public String postid,userid,message,isread,createdAt,updatedAt;

    public String getPostid() {
        return postid;
    }

    public String getUserid() {
        return userid;
    }

    public String getMessage() {
        return message;
    }

    public String getIsread() {
        return isread;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }
}
