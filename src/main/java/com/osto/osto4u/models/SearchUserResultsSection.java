package com.osto.osto4u.models;

public class SearchUserResultsSection {

    private String username,dppath;
    private int userid;
    boolean isfollow;

    public boolean isIsfollow() {
        return isfollow;
    }

    public String getUsername() {
        return username;
    }

    public String getDppath() {
        return dppath;
    }

    public int getUserid() {
        return userid;
    }
}
