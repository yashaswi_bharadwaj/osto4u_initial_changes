package com.osto.osto4u.models;

/**
 * Created by VEDAVYASA GUTTAL on 4/19/2018.
 */

public class SaveItemPostBody {

    private int postid;
    private int userid;

    public SaveItemPostBody() {
    }

    public SaveItemPostBody(int postid, int userid) {
        this.postid = postid;
        this.userid = userid;
    }

    public int getPostId() {
        return postid;
    }

    public void setPostId(int postid) {
        this.postid = postid;
    }

    public int getUserid() {
        return userid;
    }

    public void setUserid(int userid) {
        this.userid = userid;
    }
}
