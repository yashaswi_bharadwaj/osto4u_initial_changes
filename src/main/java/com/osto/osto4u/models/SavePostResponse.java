package com.osto.osto4u.models;

/**
 * Created by VEDAVYASA GUTTAL on 4/19/2018.
 */

public class SavePostResponse {

    String success;
    int code;



    public String getResults() {
        return success;
    }

    public void setResults(String results) {
        success = results;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
