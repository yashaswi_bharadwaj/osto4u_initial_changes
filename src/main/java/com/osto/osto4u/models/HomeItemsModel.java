package com.osto.osto4u.models;

public class HomeItemsModel {

    private String userName, userLocation ;
    private int viewersWatched, contentImage,contentDesc;
    private float reviews;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLocation() {
        return userLocation;
    }

    public void setUserLocation(String userLocation) {
        this.userLocation = userLocation;
    }

    public Integer getContentDesc() {
        return contentDesc;
    }

    public void setContentDesc(int contentDesc) {
        this.contentDesc = contentDesc;
    }

    public int getViewersWatched() {
        return viewersWatched;
    }

    public void setViewersWatched(int viewersWatched) {
        this.viewersWatched = viewersWatched;
    }

    public int getContentImage() {
        return contentImage;
    }

    public void setContentImage(int contentImage) {
        this.contentImage = contentImage;
    }

    public float getReviews() {
        return reviews;
    }

    public void setReviews(float reviews) {
        this.reviews = reviews;
    }
}
