package com.osto.osto4u.utils;

/**
 * Created by yashaswi on 4/27/18.
 */

public class AppConstants {
    // This TAG is to be commonly used in all classes, to identify app logs easily.
    public static final String TAG = "OSTO4U-";

    // Duration until which splash screen should last
    public static final long SPLASH_SCREEN_DURATION = 250;
}
